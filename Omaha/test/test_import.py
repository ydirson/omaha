# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# Simple "import module" tests for those modules not otherwise covered
# by unit tests.

import Overlord
import Omaha.Core
import sys, os

from importlib import resources
import unittest

import logging
logger = logging.getLogger(__name__)

data_module = resources.files("Omaha.plugins")
gamesdir = data_module.joinpath("Games")

class NullToolkit(Omaha.Core.UI.Toolkit):
    def open_params_dialog(self, title, params, app_ui, accept_cb=None, refuse_cb=None,
                           parent_plugin=None, parent_window=None,
                           destroy=True, synchronous=False):
        return True # == run the game

class GameAppStub:
    def __init__(self, game):
         self.game = game

class _playerDriverTemplate:
    gameplugin: str
    def setUp(self):
        assert isinstance(self.gameplugin,str)
        gp = Overlord.plugin.Plugin(gamesdir.joinpath(self.gameplugin),
                                    "X-Omaha-Game")
        tk = NullToolkit()
        plugin_manager = Overlord.PluginManager("Omaha.plugins")
        game = gp.Class(parentcontext=Omaha.Core.Context(plugin_manager))
        self.context = game.context.clone()
        self.context.game_plugin = gp
        self.app = GameAppStub(game)
    def contruct_player_driver(self, cls):
        return cls(gui=self.app, player=None, parentcontext=self.context)
    def contruct_move_composer(self, cls):
        return cls(gui=self.app, parentcontext=self.context)

    # universal drivers
    def test_local(self):
        import Omaha.Core.player_driver
        self.contruct_player_driver(Omaha.Core.player_driver.LocalPlayerDriver)

    # universal MoveComposers
    def test_ManualEntry(self):
        import Omaha.MoveComposers.ManualEntry
        self.contruct_move_composer(Omaha.MoveComposers.ManualEntry.ManualEntryMoveComposer)
    def test_Stdin(self):
        import Omaha.MoveComposers.Stdin
        self.contruct_move_composer(Omaha.MoveComposers.Stdin.StdinMoveComposer)
    def test_Multiplexer(self):
        import Omaha.MoveComposers.Multiplexer
        self.contruct_move_composer(Omaha.MoveComposers.Multiplexer.MultiplexerMoveComposer)
    def test_MouseRendererBased(self) -> None:
        import Omaha.MoveComposers.MouseRendererBased
        self.contruct_move_composer(
            Omaha.MoveComposers.MouseRendererBased.MouseRendererBasedMoveComposer)

class test_GoPlayerDrivers(_playerDriverTemplate, unittest.TestCase):
    gameplugin = "Go19.desktop"
    def test_GTP2Program(self):
        import Omaha.PlayerDrivers.GTP2Program
        self.contruct_player_driver(Omaha.PlayerDrivers.GTP2Program.GTP2Program)
    def test_MouseSingleClick(self):
        import Omaha.MoveComposers.MouseSingleClick
        self.contruct_move_composer(Omaha.MoveComposers.MouseSingleClick.MouseSingleClickMoveComposer)

class test_ChessPlayerDrivers(_playerDriverTemplate, unittest.TestCase):
    gameplugin = "Chess.desktop"
    def test_XBoard(self):
        import Omaha.PlayerDrivers.XBoard
        self.contruct_player_driver(Omaha.PlayerDrivers.XBoard.XBoardPlayerDriver)
    def test_MouseChesslike(self):
        import Omaha.MoveComposers.MouseChesslike
        self.contruct_move_composer(Omaha.MoveComposers.MouseChesslike.MouseChesslikeMoveComposer)

class test_ShogiPlayerDrivers(_playerDriverTemplate, unittest.TestCase):
    gameplugin = "Shogi.desktop"
    def test_XShogi(self):
        import Omaha.PlayerDrivers.XShogi
        self.contruct_player_driver(Omaha.PlayerDrivers.XShogi.XShogiPlayerDriver)
    def test_CSA(self):
        import Omaha.PlayerDrivers.CSA
        self.contruct_player_driver(Omaha.PlayerDrivers.CSA.CSAPlayerDriver)
    def test_MouseChesslike(self):
        import Omaha.MoveComposers.MouseChesslike
        self.contruct_move_composer(Omaha.MoveComposers.MouseChesslike.MouseChesslikeMoveComposer)

class test_CheckersPlayerDrivers(_playerDriverTemplate, unittest.TestCase):
    gameplugin = "Checkers.desktop"
    def test_MouseWaypoint(self):
        import Omaha.MoveComposers.MouseWaypoint
        self.contruct_move_composer(Omaha.MoveComposers.MouseWaypoint.MouseWaypointMoveComposer)

class test_CantStop2PlayerDrivers(_playerDriverTemplate, unittest.TestCase):
    gameplugin = "CantStop2.desktop"

class test_CantStop3PlayerDrivers(_playerDriverTemplate, unittest.TestCase):
    gameplugin = "CantStop3.desktop"

class test_Renderers(unittest.TestCase):
    def test_Null(self):
        import Omaha.Renderers.Null
    def test_AsciiChess(self):
        import Omaha.Renderers.AsciiChess
    def test_AsciiShogi(self):
        import Omaha.Renderers.AsciiShogi
    def test_FullscreenPieceDelegating(self):
        import Omaha.Renderers.FullscreenPieceDelegating
    def test_FullscreenPoolPieceDelegating(self):
        import Omaha.Renderers.FullscreenPoolPieceDelegating
    def test_CantStop(self) -> None:
        import Omaha.Renderers.CantStop
    def test_TextOnSVGShogi(self):
        import Omaha.Renderers.TextOnSVGShogi

class test_PieceRenderers(unittest.TestCase):
    def test_Null(self):
        import Omaha.Renderers.Pieces.Null
    def test_MarkableDrawnStones(self):
        import Omaha.Renderers.Pieces.MarkableDrawnStones
    def test_SimpleDrawnStones(self):
        import Omaha.Renderers.Pieces.SimpleDrawnStones
    def test_SVGChess(self):
        import Omaha.Renderers.Pieces.SVGChess
    def test_TextOnSVGShogi(self):
        import Omaha.Renderers.Pieces.TextOnSVGShogi

class test_HolderRenderers(unittest.TestCase):
    def test_CheckeredHexBoard(self):
        import Omaha.Renderers.Holders.CheckeredHexBoard
    def test_CheckeredRectBoard(self):
        import Omaha.Renderers.Holders.CheckeredRectBoard
    def test_GoBan(self):
        import Omaha.Renderers.Holders.GoBan
    def test_HexDotLocation(self):
        import Omaha.Renderers.Holders.HexDotLocation
    def test_OutlinedHexes(self):
        import Omaha.Renderers.Holders.OutlinedHexes
    def test_SimplePool(self):
        import Omaha.Renderers.Holders.SimplePool
    def test_SquareDotLocation(self):
        import Omaha.Renderers.Holders.SquareDotLocation
    def test_WireframeRectBoard(self):
        import Omaha.Renderers.Holders.WireframeRectBoard

class test_LocationHighlighters(unittest.TestCase):
    def test_CircledLocationHighlighter(self):
        import Omaha.Renderers.LocationHighlighters.CircledLocationHighlighter
    def test_ColorSquareHighlighter(self):
        import Omaha.Renderers.LocationHighlighters.ColorSquareHighlighter
    def test_CrossHairHighlighter(self):
        import Omaha.Renderers.LocationHighlighters.CrossHairHighlighter
    def test_SimpleSquareHighlighter(self):
        import Omaha.Renderers.LocationHighlighters.SimpleSquareHighlighter

class test_MoveHighlighters(unittest.TestCase):
    def test_MoveTargetHighlighter(self):
        import Omaha.Renderers.MoveHighlighters.MoveTargetHighlighter

class test_Apps(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.skip_gtk = False
        try:
            import gi
            gi.require_version('Gtk', '3.0')
            from gi.repository import Gtk
        except ImportError:
            cls.skip_gtk = True

        found_qt = False
        if not found_qt:
            try:
                import PySide6
                found_qt = True
            except ImportError as ex:
                logger.info("skipping PySide6: %s", ex)
        if not found_qt:
            try:
                import PySide2  # type: ignore
                found_qt = True
            except ImportError:
                logger.info("skipping PySide2: %s", ex)
        if not found_qt:
            try:
                import PyQt5
                found_qt = True
            except ImportError:
                logger.info("skipping PyQt5: %s", ex)

        cls.skip_qt = not found_qt

    def test_GtkLauncher(self):
        if self.skip_gtk:
            self.SkipTest("suitable Gtk not found")
        import Omaha.Apps.Gtk.Launcher
    def test_GtkPlay(self):
        if self.skip_gtk:
            self.SkipTest("suitable Gtk not found")
        import Omaha.Apps.Gtk.Play
    def test_QtLauncher(self):
        if self.skip_qt:
            self.SkipTest("suitable Qt not found")
        import Omaha.Apps.Qt.Launcher
    def test_QtPlay(self):
        if self.skip_qt:
            self.SkipTest("suitable Qt not found")
        import Omaha.Apps.Qt.Play
