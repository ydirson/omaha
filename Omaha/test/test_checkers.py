# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.Checkers import Checkers
from Omaha import Core
import Overlord

import unittest
from . import lib

class CheckersTestCase(unittest.TestCase):
    def test_checkers(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = Checkers(parentcontext=app.context,
                     paramvalues=dict(boardsize=4, rowcount=1))
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.assertEqual(g.phase, Core.GamePhase.Playing)
