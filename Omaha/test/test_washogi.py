# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games import WaShogi
from Omaha.Games.piecetypes.shogi import ShogiFacetype
from Omaha import Core
import Overlord

import unittest
from . import lib

class test00(unittest.TestCase):
    def setUp(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = WaShogi.Game(parentcontext=app.context)
        app.game = g # emulates GameApp
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.game = g

    def test00_washogi(self):
        g = self.game
        self.assertEqual(g.phase, Core.GamePhase.Playing)

        # replay a game
        moves = ("RR4i-5h RR8c-7d CK6k-7j SP5c-5d CK7j-6k RR7dx7i+ SP4h-4g TF4c-4e "
                 "SP4g-4f TF4e-3f SP8h-8g BD2a-3b SP8g-8f BD3b-4c SP1i-1h TF3f-3e "
                 "SP1h-1g +RR7i-7g CK6k-7j +RR7gx8f CK7j-6k +RR8f-7e CK6k-7j CM10a-9b "
                 "CK7j-6k SC3a-3b CK6k-7j SP3c-3d CK7j-6k SP1c-1d CK6k-7j SP11c-11d "
                 "CK7j-6k SP10c-10d CK6k-7j SP9c-9d CK7j-6k CM9b-9c CK6k-7j SO9a-9b "
                 "CK7j-6k SP7c-7d CK6k-7j SP6c-6d CK7j-6k SP2c-2d CK6k-7j FC8a-7b "
                 "CK7j-6k FC7b-6c CK6k-7j FF2b-3a CK7j-6k SP6d-6e CK6k-7j SP6e-6f "
                 "CK7j-6k SP11d-11e CK6k-7j SP11e-11f CK7j-6k SP10d-10e CK6k-7j "
                 "SP10e-10f CK7j-6k CE10b-10d CK6k-7j SO9b-8a CK7j-6k CE10d-7g SP2i-2h "
                 "CM9c-10d SP2h-2g CM10d-9e SP3i-3h CM9e-8f SP3h-3g SP9d-9e SP3g-3f "
                 "CE7g-7i SP3fx3e CE7ix8i CK6k-5j CE8ix10k CK5j-6k SP3dx3e CK6k-7j "
                 "FF3a-6d CK7j-6k SC3b-3c CK6k-7j CE10kx10j CK7j-6k CE10jx9i CK6k-7j "
                 "CE9i-9h CK7j-8j CE9hx10i SP6i-6h CE10i-10h CK8j-9j FF6dx4f SP6h-6g "
                 "SP6fx6g SP11i-11h CE10h-9h CK9j-10k FF4fx9k+").split(' ')

        notation = WaShogi.EnglishWaNotation(g)
        for move_str in moves:
            lib.make_move(g, move_str, notation=notation)
