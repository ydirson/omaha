# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.Go import Go
from Omaha.GameIO.SGFReader import SGFGameReader
from Omaha import Core
import Overlord

import unittest
from . import lib

class test(unittest.TestCase):
    def setUp(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = Go(parentcontext=app.context)
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.game = g

    def test_basics(self):
        g = self.game
        self.assertEqual(g.phase, Core.GamePhase.Playing)
        # replay a small partial game
        moves = "r3 d16 q17 d3 r15 c5".split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)

        # check undo
        g.undo_move()
        lib.make_move(g, "c4")
        g.undo_move()
        lib.make_move(g, "c5")

        # compare effective moves with original list
        self.assertEqual([g.default_notation.move_serialization(m)
                          for m in g.moves.played], moves)

    def test_undo_capture(self):
        g = self.game
        self.assertEqual(g.phase, Core.GamePhase.Playing)
        # replay a small partial game
        moves = "k12 k13 l13 l12 j13 j12 k14 k11 k13".split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)
        g.undo_move()
        g.undo_move()
        g.undo_move()

class test_go9x9(unittest.TestCase):
    def setUp(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        self.game = Go(parentcontext=app.context, paramvalues=dict(boardsize=9))
        self.game.setup()
        self.game.change_phase(Core.GamePhase.Playing) # FIXME!
        self.assertEqual(self.game.phase, Core.GamePhase.Playing)

    def test_largest_location(self):
        lib.make_move(self.game, "j9")

    def test_outofboard_horizontal(self):
        with self.assertRaises(IndexError):
            lib.make_move(self.game, "k9")

    def test_outofboard_vertical(self):
        with self.assertRaises(IndexError):
            lib.make_move(self.game, "j10")

class NonStandardGoTestCase(unittest.TestCase):
    def test_nonstandardgo(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = Go(parentcontext=app.context, paramvalues=dict(boardsize=28,
                                                           handicap=3))
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.assertEqual(g.phase, Core.GamePhase.Playing)

        # replay a small partial game
        moves = "aa6 x3 bb9".split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)
        # compare effective moves with original list
        self.assertEqual([g.default_notation.move_serialization(m)
                          for m in g.moves.played], moves)


def test_loadsgf():
    app = Core.UI.App(plugin_manager=None,
                      toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
    plugin_manager = Overlord.PluginManager("Omaha.plugins")
    for filename in ['Omaha/test/data/go19a.sgf']:
        with open(filename) as f:
            reader = SGFGameReader(plugin_manager,
                                   parentcontext=app.context)
            reader.parse(f)
            g = reader.game()
