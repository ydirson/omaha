# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.ToriShogi import ToriShogi
from Omaha.Games.piecetypes.shogi import ShogiFacetype
from Omaha import Core
import Overlord

import unittest
from . import lib

class test00(unittest.TestCase):
    def setUp(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = ToriShogi(parentcontext=app.context)
        app.game = g # emulates GameApp
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.game = g

    def test00_torishogi(self):
        g = self.game
        self.assertEqual(g.phase, Core.GamePhase.Playing)

        # replay a small partial game
        moves = "Sw3dx3c Sw5dx5e Sw3c-3b+ Sw5c-5d +Sw3b-3d Fa4b-5c Sw4e-4d Sw4cx4d Fa4f-3f Sw4d-4e Fa3fx4e Sw*5b +Sw3dx5b Cr5ax5b".split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)

        # checking move
        lib.make_move(g, "Sw*4b")

        # check refusal of a move that does not protect the king
        with self.assertRaises(Core.InvalidMove) as cm:
            lib.make_move(g, "Pt2a-2b")
        ex = cm.exception

        # FIXME we get "cannot move there" - not critical but surprising
        #self.assertTrue(ex.message.startswith("player's king would be in check"),
        #    "Non-protecting move should be forbidden while in check '%s'" % ex.message)

        lib.make_move(g, "Ph4a-5a")

class test20_loadpgn_fullgame(lib.PGNLoad, unittest.TestCase):
    filename = 'Omaha/test/data/tori.pgn'

class test30_frompgn(lib.FromPGN, unittest.TestCase):
    filename = 'Omaha/test/data/tori.pgn'
