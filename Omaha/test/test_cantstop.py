# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

import logging
import random
import unittest
from typing import Optional, Sequence

import Overlord
from Omaha import Core
from Omaha.Games import CantStop

from . import lib

class test(unittest.TestCase):
    def setUp(self) -> None:
        self.next_dice: list[int] = []
        # note: we fake only a subset of randrange
        random.randrange = self.fudged_randrange # type: ignore[assignment]
        # create a game
        app: Core.GameApp = Core.UI.App( # type: ignore[assignment]
            plugin_manager=None,         # type: ignore[arg-type]
            toolkit=lib.StubToolkit(),
            parentcontext=Overlord.Context(None)) # type: ignore[arg-type]
        g = CantStop.Game2Players(parentcontext=app.context)
        app.game = g # emulates GameApp
        g.setup()
        self.game = g

    def fudge(self, dice: Sequence[int]) -> None:
        "Fudge dice values for randrange"
        assert not self.next_dice
        logging.debug("fudging %s", dice)
        self.next_dice.extend(dice)

    def fudged_randrange(self, stop: int) -> int:
        "Stub randrange implementation from fudged values"
        assert self.next_dice[0] < stop
        return self.next_dice.pop(0)

    def make_move(self, xmove_str: str, notation: Optional[Core.MoveNotation]=None) -> None:
        if not notation:
            notation = self.game.default_notation
        xmove = xmove_str.split('|')
        if len(xmove) == 1:
            move_str, = xmove
            player = self.game.scratch_state.whose_turn
        elif len(xmove) == 2:
            playerid, move_str = xmove
            playeridx = "12".index(playerid)
            player = self.game.players[playeridx]
        else:
            self.assertIsNone("unparseable xmove %r" % xmove_str)
        logging.debug("making move %r: %r as %s", xmove_str, move_str, player.name)
        self.game.make_move(player,
                            notation.move_deserialization(self.game.last_state,
                                                          move_str, player))

    def test_00basic(self) -> None:
        g = self.game

        self.fudge((1,2,3,4))
        g.change_phase(Core.GamePhase.Playing)

        # replay a small partial game
        for dice, moves_str in (((), "1|cols=1+2,3+4"),
                                ((0,1,2,3), "1|roll 1|cols=1+2,0+3"),
                                ((1,2,3,4), "1|stop 2|cols=1+2,3+4"),
                                ((0,1,2,3), "2|roll 2|cols=0+1"),
                                ((5,5,5,5, 1,2,3,4), "2|roll"), # fall
                                ):
            self.fudge(dice)
            for move_str in moves_str.split(' '):
                self.make_move(move_str)

    def test_01locking(self) -> None:
        g = self.game

        self.fudge((0,0,2,3))
        g.change_phase(Core.GamePhase.Playing)

        for dice, moves_str in (((), "1|cols=0+0,2+3"),
                                ((0,0,2,3), "1|roll 1|cols=0+0,2+3"),
                                ((0,0,2,3), "1|roll 1|cols=0+0,2+3"),
                                ((0,0,1,1), "1|stop"),
                                ):
            self.fudge(dice)
            for move_str in moves_str.split(' '):
                self.make_move(move_str)

        # check locking
        with self.assertRaises(Core.InvalidMove) as cm:
            self.make_move("cols=0+0")
        ex = cm.exception
        self.assertEqual(ex.args[0], "column 0 is already locked")

        self.make_move("cols=1+1")

        for dice, moves_str in (((2,3,2,3), "2|stop 1|cols=2+3,2+3"),
                                ((2,3,2,3), "1|roll 1|cols=2+3,2+3"),
                                ((2,3,5,5), "1|roll 1|cols=2+3,5+5"),
                                ((5,5,5,5), "1|roll 1|cols=5+5,5+5"),
                                ((2,3,5,5), "1|stop"),
                                ):
            self.fudge(dice)
            for move_str in moves_str.split(' '):
                self.make_move(move_str)

        # 3 locked columns for player 1
        self.assertEqual(g.phase, Core.GamePhase.Over)

class test_pos_notation(unittest.TestCase):
    def setUp(self) -> None:
        # create a game
        app: Core.GameApp = Core.UI.App( # type: ignore[assignment]
            plugin_manager=None,         # type: ignore[arg-type]
            toolkit=lib.StubToolkit(),
            parentcontext=Overlord.Context(None)) # type: ignore[arg-type]
        g = CantStop.Game2Players(parentcontext=app.context)
        self.game = g
        self.pos_notation = CantStop.PositionNotation(g)
    def test_pos1(self) -> None:
        "test deserialization with pieces in locked columns"
        pos = """P: 0
0: 0:3 5:3 9:2
1: 2:2 9:4 0:1
M: 5:8 10:3"""
        gamestate = self.pos_notation.position_deserialization(pos)
        self.assertIsNone(gamestate.is_locked(1))
        self.assertIsNotNone(gamestate.is_locked(0))
        self.assertIsNotNone(gamestate.is_locked(9))
