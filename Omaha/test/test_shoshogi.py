# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.ShoShogi import ShoShogi
from Omaha import Core
import Overlord

import unittest
from . import lib

class test_shoshogi(unittest.TestCase):
    def setUp(self):
        # create a game
        app = Core.UI.App(plugin_manager=None,
                          toolkit=lib.StubToolkit(), parentcontext=Core.Context(None))
        g = ShoShogi(parentcontext=app.context)
        app.game = g # emulates GameApp
        g.setup()
        g.change_phase(Core.GamePhase.Playing) # FIXME!
        self.assertEqual(g.phase, Core.GamePhase.Playing)

        self.game = g

    def test_shoshogi(self):
        g = self.game
        # replay a small partial game
        moves = ("P5g-5f P5c-5d P5f-5e P5dx5e DE5h-5g P5e-5f DE5gx5f P6c-6d DE5f-5e "
                 "P6d-6e DE5e-6d P6e-6f DE6d-6c+ DE5b-4b P4g-4f R8b-5b P4f-4e").split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)

        # compare effective moves with original list
        self.assertEqual([g.default_notation.move_serialization(m)
                          for m in g.moves.played], moves)

    def test_prince(self):
        g = self.game
        # replay a small partial game
        moves = ("P5g-5f P7c-7d DE5h-5g P7d-7e DE5g-6f P7e-7f DE6f-7e P7fx7g+ "
                 "DE7e-7d +P7g-6h").split(' ')
        for move_str in moves:
            lib.make_move(g, move_str)

        # king now in check, promoting elephant to compensate
        lib.make_move(g, "DE7d-7c+")
        # capture king
        lib.make_move(g, "+P6hx5i")

        with self.assertRaises(Core.InvalidMove) as cm:
            lib.make_move(g, "P5f-5e")
        ex = cm.exception
        self.assertTrue(ex.message.startswith("player's king would be in check"),
                        "prince cannot be in check when the king is dead")

        lib.make_move(g, "+DE7c-7d")
