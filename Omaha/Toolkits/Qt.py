# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Core import UnsatisfiableConstraint
import Overlord
from Overlord.misc import pretty

import sys, os, logging, math

logger = logging.getLogger(__name__)

# in order, try PySide6, PySide2, then PyQt5
__backend_found = False
if not __backend_found:
    try:
        #raise ImportError("Skipping PySide6")
        import PySide6 as PyQt
        from PySide6 import QtCore, QtGui, QtWidgets, QtSvg
        QAction: type = QtGui.QAction
        logger.info("using PySide6")
        __backend_found = True
    except ImportError as ex:
        logger.debug("skipping PySide6: %s", ex)
if not __backend_found:
    try:
        #raise ImportError("Skipping PySide2")
        import PySide2 as PyQt  # type: ignore
        from PySide2 import QtCore, QtGui, QtWidgets, QtSvg # type: ignore[no-redef]
        QAction: type = QtWidgets.QAction # type: ignore[attr-defined,no-redef]
        logger.info("using PySide2")
        __backend_found = True
    except ImportError as ex:
        logger.debug("skipping PySide2: %s", ex)
if not __backend_found:
    try:
        import sip              # type: ignore
        sip.setapi('QString', 2)
        sip.setapi('QVariant', 2)
        import PyQt5 as PyQt    # type: ignore[attr-defined,no-redef]
        from PyQt5 import QtCore, QtGui, QtWidgets, QtSvg # type: ignore[no-redef]
        QAction: type = QtWidgets.QAction # type: ignore[attr-defined,no-redef]
        logger.info("using PyQt5")
        __backend_found = True
    except ImportError as ex:
        logger.debug("skipping PyQt5: %s", ex)
if not __backend_found:
    logger.critical("could not load a Qt backend, see debug logs or -vv for details")
    exit(1)

class BitmapImage(Core.UI.Toolkit.BitmapImage):
    def __init__(self, width, height):
        assert width > 0 and height > 0
        self.__img = QtGui.QImage(width, height, QtGui.QImage.Format_ARGB32)
        assert self.__img.paintEngine()
        self.__img.fill(QtGui.QColor(0, 0, 0, 0))
    @property
    def _img(self):
        return self.__img
    @property
    def width(self):
        return self.__img.width()
    @property
    def height(self):
        return self.__img.height()

    def draw_sized_text(self, x, y, width, height, text, **attrs):
        _draw_sized_text(QtGui.QPainter(self.__img),
                         x, y, width, height, text, **attrs)

    def fill_circle(self, center_x, center_y, radius, **attrs):
        _fill_circle(QtGui.QPainter(self.__img),
                     center_x, center_y, radius, **attrs)

class VectorImage(Core.UI.VectorImage):
    def __init__(self, svgfilename):
        if not os.path.exists(svgfilename):
            raise RuntimeError("File not found '%s'" % svgfilename)
        self.__svg = QtSvg.QSvgRenderer(svgfilename)
    @property
    def width(self):
        return self.__svg.defaultSize().width()
    @property
    def height(self):
        return self.__svg.defaultSize().height()
    def render_to_bitmap(self, bitmap, x, y, width, height,
                         rotation=None):
        """
        Render vector graphics in bbox, after possible rotation.
        """
        painter = QtGui.QPainter(bitmap._img)
        if rotation:
            rx, ry, angle = rotation
            painter.translate(rx, ry)
            painter.rotate(angle * 180 / math.pi)
            painter.translate(-rx, -ry)
        self.__svg.render(painter, QtCore.QRectF(x, y, width, height))

class SolidFillPattern(Core.UI.SolidFillPattern):
    def __init__(self, color):
        self.brush = QtGui.QBrush(color)

class Canvas(QtWidgets.QWidget):
    def __init__(self, app_ui, default_size, *args, **kwargs):
        super(Canvas, self).__init__(*args, **kwargs)
        self.app_ui = app_ui
        assert isinstance(default_size, QtCore.QSize)
        self.__default_size = default_size

        self.__mouse_press_callbacks = []
        self.__mouse_release_callbacks = []
        self.__mouse_move_callbacks = []

        self.__tooltip_func = None

    def paintEvent(self, event):
        # FIXME this is app stuff
        drawing_context = QtGui.QPainter(self)
        self.app_ui.game_renderer.draw(drawing_context,
                                       self.app_ui.game_renderer.game.scratch_state)

    def mousePressEvent(self, event):
        for cb in self.__mouse_press_callbacks:
            if cb(event.x(), event.y()):
                self.update()
                break
    def mouseReleaseEvent(self, event):
        for cb in self.__mouse_release_callbacks:
            if cb(event.x(), event.y()):
                self.update()
                break
    def mouseMoveEvent(self, event):
        pressed = (event.buttons() & QtCore.Qt.LeftButton)
        for cb in self.__mouse_move_callbacks:
            if cb(event.x(), event.y(), pressed):
                self.update()
                break
    def sizeHint(self):
        return self.__default_size

    def event(self, event):
        if event.type() is QtCore.QEvent.ToolTip:
            tooltip = self.__tooltip_func(event.pos()) if self.__tooltip_func else None
            if tooltip == None:
                QtWidgets.QToolTip.hideText()
            else:
                QtWidgets.QToolTip.showText(event.globalPos(), tooltip)
            return True
        return super(Canvas, self).event(event)

    def set_tooltip_func(self, f):
        "Install a tooltip handler, deinstall if None"
        self.__tooltip_func = f
        if f == None:
            self.setMouseTracking(False)
        else:
            self.setMouseTracking(True)

    def set_mouse_press_callback(self, callback):
        self.__mouse_press_callbacks.append(callback)
    def unset_mouse_press_callback(self, callback):
        self.__mouse_press_callbacks.remove(callback)

    def set_mouse_release_callback(self, callback):
        self.__mouse_release_callbacks.append(callback)
    def unset_mouse_release_callback(self, callback):
        self.__mouse_release_callbacks.remove(callback)

    def set_mouse_move_callback(self, callback):
        self.__mouse_move_callbacks.append(callback)
    def unset_mouse_move_callback(self, callback):
        self.__mouse_move_callbacks.remove(callback)

class QtStringWidget(Core.UI.ParamWidget):
    def widget(self):
        self.__line_edit = QtWidgets.QLineEdit(self.param.value)
        self.__line_edit.editingFinished.connect(self.__set_value)
        return self.__line_edit
    def __set_value(self):
        self.param.value = self.__line_edit.text()
    def retrieve_value(self, entry=None):
        # already there
        pass

class QtIntWidget(Core.UI.ParamWidget):
    def widget(self):
        self.__spinbox = QtWidgets.QSpinBox()
        self.__spinbox.valueChanged.connect(self.__set_value)
        self.update()
        return self.__spinbox
    def update(self):
        self.__spinbox.setValue(self.param.value)
        if self.param.decl.has_min:
            self.__spinbox.setMinimum(self.param.decl.min)
        if self.param.decl.has_max:
            self.__spinbox.setMaximum(self.param.decl.max)
    def __set_value(self, value):
        self.param.value = value
    def retrieve_value(self, entry=None):
        # already there
        pass

class QtFloatWidget(Core.UI.ParamWidget):
    def widget(self):
        self.__spinbox = QtWidgets.QDoubleSpinBox()
        self.__spinbox.valueChanged.connect(self.__set_value)
        self.update()
        return self.__spinbox
    def update(self):
        if self.param.decl.has_min:
            self.__spinbox.setMinimum(self.param.decl.min)
        else:
            self.__spinbox.setMinimum(-sys.float_info.max)
        if self.param.decl.has_max:
            self.__spinbox.setMaximum(self.param.decl.max)
        else:
            self.__spinbox.setMaximum(sys.float_info.max)
        self.__spinbox.setValue(self.param.value)
    def __set_value(self, value):
        self.param.value = value
    def retrieve_value(self, entry=None):
        # already there
        pass

class QtChoiceWidget(Core.UI.ChoiceWidget):
    def widget(self):
        # tell early when we have nothing to choose from
        if len(self.param.decl.alternatives) == 0:
            raise UnsatisfiableConstraint('No choice for "%s".' %
                                          self.param.decl.label)

        self.__combo = QtWidgets.QComboBox()

        vbox = QtWidgets.QWidget()
        layout = QtWidgets.QVBoxLayout(vbox)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__combo)

        # insert items while detecting the index of the current one
        current_value = self.param.value
        for i, (choice, value) in enumerate(self.param.decl.alternatives.items()):
            item = self.__combo.addItem(choice, self.param)
            if value is None:
                item = self.__combo.model().item(self.__combo.count() - 1)
                item.setFlags(item.flags() & ~(QtCore.Qt.ItemIsSelectable |
                                               QtCore.Qt.ItemIsEnabled))
            if current_value == value:
                # found current, make it active
                self.__combo.setCurrentIndex(i)
        self.__combo.currentTextChanged.connect(self.__set_value)

        self._add_subparams(self.__combo, layout)

        return vbox

    def _update_one_subparam_visibility(self, combo, parent_choice, subparam_box, subparam):
        if parent_choice == combo.currentText():
            subparam_box.show()
        else:
            subparam_box.hide()

    def __set_value(self, choice):
        self.param.value = self.param.decl.alternatives[choice]
    def retrieve_value(self, entry=None):
        # already there
        pass

    def connect_activated(self, handler):
        self.__combo.activated.connect(handler)

class QtPluginChoiceWidget(Core.UI.PluginChoiceWidget):
    def widget(self):
        w = self._choice_widget.widget()
        self._choice_widget.connect_activated(self.retrieve_value)
        return w
    def retrieve_value(self, combo=None):
        self._choice_widget.retrieve_value() # FIXME: now useless ?
        self.param.value = self._choice_widget.param.value


_app = None

class Qt(Core.UI.Toolkit):
    """
    Omaha abstraction of the Qt toolkit.
    """

    BitmapImage = BitmapImage
    VectorImage = VectorImage
    SolidFillPattern = SolidFillPattern

    # parameter handling
    RawLabelWidget = QtWidgets.QLabel
    StringWidget = QtStringWidget
    IntWidget = QtIntWidget
    FloatWidget = QtFloatWidget
    ChoiceWidget = QtChoiceWidget
    # MultiChoiceWidget = FIXME
    PluginChoiceWidget = QtPluginChoiceWidget

    def __init__(self, cli_args):
        super(Qt, self).__init__()
        global _app
        if not _app:
            _app = QtWidgets.QApplication([sys.argv[0]] + cli_args)
        self.__event_sources = {}
        self.__streams = {}
        self.dialogs = set()

    # generic GUI elements

    def notify(self, title, text):
        QtWidgets.QMessageBox.information(None, "omaha - " + title, text)

    def askuser_yesno(self, text):
        btn = QtWidgets.QMessageBox.question(None, "omaha - error", text)
        return btn == QtWidgets.QMessageBox.StandardButton.Yes

    def select_load_file(self, callback):
        dlg = QtWidgets.QFileDialog(None, "omaha - load game")
        dlg.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        dlg.setAcceptMode(QtWidgets.QFileDialog.AcceptOpen)
        dlg.setFileMode(QtWidgets.QFileDialog.ExistingFile)
        dlg.fileSelected.connect(callback)
        # FIXME add/remove is racy ?
        dlg.finished.connect(lambda ret: self.dialogs.remove(dlg))
        self.dialogs.add(dlg)
        dlg.show()

    def select_save_file(self, callback):
        dlg = QtWidgets.QFileDialog(None, "omaha - save game")
        dlg.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        dlg.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
        dlg.fileSelected.connect(callback)
        # FIXME add/remove is racy ?
        dlg.finished.connect(lambda ret: self.dialogs.remove(dlg))
        self.dialogs.add(dlg)
        dlg.show()

    def open_params_dialog(self, title, params, app_ui, accept_cb=None, refuse_cb=None,
                           parent_plugin=None, parent_window=None,
                           destroy=True, synchronous=False):
        dlg = QtWidgets.QDialog(parent_window)
        dlg.setWindowTitle(title)
        dlg.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        layout = QtWidgets.QVBoxLayout(dlg)

        scroll = QtWidgets.QScrollArea()
        scroll_contents = QtWidgets.QWidget()
        scroll.setWidget(scroll_contents)
        scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        scroll.setWidgetResizable(True)
        layout.addWidget(scroll)
        prefs_layout = QtWidgets.QVBoxLayout(scroll_contents)

        butbox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok |
                                            QtWidgets.QDialogButtonBox.Cancel)
        butbox.accepted.connect(dlg.accept)
        butbox.rejected.connect(dlg.reject)

        paramwidgets = {}
        def walk_action(param):
            hbox, paramwidgets[param] = self.create_param_widget(param, app_ui, params)
            prefs_layout.addWidget(hbox)
        Overlord.walk_params(params, parent_plugin, walk_action)
        requires_interaction = (len(paramwidgets) > 0)

        def __response_handler(accepted):
            self.dialogs.remove(dlg)

            if not accepted:
                logger.debug("cancelled")
                if refuse_cb:
                    refuse_cb()
                return

            # Retrieve values from Widget objects, for params which
            # were allocated a Widget.
            for param in params.values():
                if param in paramwidgets:
                    paramwidgets[param].retrieve_value()

            if accept_cb:
                try:
                    accept_cb(params)
                except Overlord.ParameterError as ex:
                    # FIXME: should notify user
                    logger.error("Bad parameter value: {0}".format(ex))
                    return

        if requires_interaction:
            dlg.finished.connect(__response_handler)
            self.dialogs.add(dlg)
            layout.addWidget(butbox)
            if synchronous:
                response = dlg.exec_()
                return { 1: True,
                         0: False }[response]
            else:
                dlg.show()
        else:
            __response_handler(True)

        return dlg

    def create_param_widget(self, param, app_ui, params):
        """Create widget for editing param in the context of app_ui.

        Both are included in a frame, which is returned so caller can place
        it where it wants.  Returns a tuple of frame and ParamWidget instance.
        """
        assert isinstance(param, Overlord.Param)
        box = QtWidgets.QGroupBox(param.label)
        w = self._create_widget(param, app_ui, params)
        layout = QtWidgets.QVBoxLayout(box)
        layout.addWidget(w.widget())

        return box, w

    # FIXME: hackish things
    @staticmethod
    def _vbox_add(vbox, widget):
        vbox.addWidget(widget)
    @staticmethod
    def _widget_hide(widget: object) -> None:
        assert isinstance(widget, QtWidgets.QWidget)
        widget.hide()
    @staticmethod
    def _combo_setchanged_hook(combo, callback, param):
        def wrapped_callback(index):
            callback(combo, combo.itemData(index, QtCore.Qt.UserRole))
        combo.activated.connect(wrapped_callback)

    ## main event loop

    def run(self):
        sys.exit(_app.exec_())

    def set_stream_callback(self, cbtype, stream, callback, arg=None):
        key = (cbtype, stream, callback)
        assert key not in self.__event_sources
        assert stream.fileno() not in self.__streams or self.__streams[stream.fileno()] is stream
        event = {Core.UI.Toolkit.CallbackType.READ: QtCore.QSocketNotifier.Read,
                 Core.UI.Toolkit.CallbackType.ERROR: QtCore.QSocketNotifier.Exception,
        }[cbtype]
        self.__streams[stream.fileno()] = stream
        def wrapped_callback(fd):
            callback(self.__streams[fd], arg)
        notif = QtCore.QSocketNotifier(stream.fileno(),#gasp
                                       event)
        notif.activated[int].connect(wrapped_callback)
        self.__event_sources[key] = (notif, wrapped_callback)
    def unset_stream_callback(self, cbtype, stream, callback):
        key = (cbtype, stream, callback)
        notif, wrapped_callback = self.__event_sources[key]
        notif.activated[int].disconnect(wrapped_callback)
        del self.__event_sources[key]
        for _, fd, _ in self.__event_sources.keys():
            if fd == stream:
                # same stream also recorded for another cbtype
                break
        else:
            logger.info("del %s", stream.fileno())
            del self.__streams[stream.fileno()]

    def set_timer_callback(self, timeout, callback):
        timer = QtCore.QTimer()
        def wrapped_callback():
            logger.debug("timer expired: %r sec, %r", timeout, callback)
            callback()
        timer.timeout.connect(wrapped_callback)
        self.__event_sources[callback] = timer
        timer.start(timeout * 1000)
    def unset_timer_callback(self, callback):
        self.__event_sources[callback].stop()
        del self.__event_sources[callback]

    ## canvas interaction

    def set_mouse_press_callback(self, canvas, callback):
        canvas.set_mouse_press_callback(callback)
    def unset_mouse_press_callback(self, canvas, callback):
        canvas.unset_mouse_press_callback(callback)

    def set_mouse_release_callback(self, canvas, callback):
        canvas.set_mouse_release_callback(callback)
    def unset_mouse_release_callback(self, canvas, callback):
        canvas.unset_mouse_release_callback(callback)

    def set_mouse_move_callback(self, canvas, callback):
        canvas.set_mouse_move_callback(callback)
    def unset_mouse_move_callback(self, canvas, callback):
        canvas.unset_mouse_move_callback(callback)

    ## canvas drawing

    def clear(self, drawing_context):
        drawing_context.eraseRect(drawing_context.window())

    def invalidate_canvas(self, canvas):
        canvas.update()

    def color(self, canvas, colorname):
        return QtGui.QColor(colorname)

    def color_transparent(self, color, alpha):
        ret = QtGui.QColor(color)
        ret.setAlphaF(ret.alphaF() * alpha)
        return ret

    def draw_centered_text(self, drawing_context,
                           x, y, text, bold=False, height=None, **attrs):
        font =  QtGui.QFont(_app.font())
        if bold:
            font.setWeight(QtGui.QFont.Black)
        if height:
            font.setPixelSize(height)
        fontMetrics = QtGui.QFontMetrics(font);
        drawing_context.setFont(font)
        _painter_set_attributes(drawing_context, attrs)
        drawing_context.drawText(x - fontMetrics.horizontalAdvance(text) / 2,
                                 y + fontMetrics.ascent() / 2,
                                 text)

    def draw_sized_text(self, drawing_context, x, y, width, height, text, **attrs):
        _draw_sized_text(drawing_context, x, y, width, height, text, **attrs)

    def draw_image(self, drawing_context, image, x, y):
        drawing_context.drawImage(QtCore.QPointF(x, y), image._img)

    def draw_line(self, drawing_context, x0, y0, x1, y1, **attrs):
        _draw_line(drawing_context, x0, y0, x1, y1, **attrs)
    def draw_rectangle(self, drawing_context, x, y, w, h, **attrs):
        _draw_rectangle(drawing_context, x, y, w, h, **attrs)
    def fill_rectangle(self, drawing_context, x, y, w, h, **attrs):
        _draw_rectangle(drawing_context, x, y, w, h,
                        color=QtGui.QColor(0, 0, 0, 0), **attrs)
    def draw_rounded_rectangle(self, drawing_context, x, y, w, h, radius, **attrs):
        _draw_rounded_rectangle(drawing_context, x, y, w, h, float(radius), **attrs)
    def fill_rounded_rectangle(self, drawing_context, x, y, w, h, radius, **attrs):
        _draw_rounded_rectangle(drawing_context, x, y, w, h, float(radius),
                                color=QtGui.QColor(0, 0, 0, 0), **attrs)
    def draw_circle(self, drawing_context, center_x, center_y, radius, **attrs):
        _draw_circle(drawing_context, center_x, center_y, radius, **attrs)
    def fill_circle(self, drawing_context, center_x, center_y, radius, **attrs):
        _draw_circle(drawing_context, center_x, center_y, radius,
                     color=QtGui.QColor(0, 0, 0, 0), **attrs)
    def draw_polygon(self, drawing_context, pointlist, **attrs):
        _draw_polygon(drawing_context, pointlist, **attrs)
    def fill_polygon(self, drawing_context, pointlist, **attrs):
        _draw_polygon(drawing_context, pointlist, color=QtGui.QColor(0, 0, 0, 0), **attrs)

def _painter_set_attributes(painter, attrs):
    pen = QtGui.QPen()
    pen.setJoinStyle(QtCore.Qt.PenJoinStyle.MiterJoin)
    for aname, avalue in attrs.items():
        if aname == 'color':
            pen.setColor(avalue)
        elif aname == 'width':
            pen.setWidth(avalue)
        elif aname == 'rotation':
            if avalue != None:
                rx, ry, angle = avalue
                painter.translate(rx, ry)
                painter.rotate(angle * 180 / math.pi)
                painter.translate(-rx, -ry)
        elif aname == 'fillpattern':
            painter.setBrush(avalue.brush)
        elif aname in {'bold'}:
             pass # not handled at this level, but not rejected
        else:
            raise KeyError("drawing attribute '%s' not supported" % (aname,))
    painter.setPen(pen)

def _draw_sized_text(painter, x, y, width, height, text, **attrs):
    if not text:
        return
    assert width > 0 and height > 0

    bold = attrs['bold'] if 'bold' in attrs else False
    assert isinstance(bold, bool)

    painter.save()
    font = QtGui.QFont(_app.font())
    font.setBold(bold)
    painter.setFont(font)

    # get size of default-size rendering of text
    alignment = QtCore.Qt.AlignCenter | QtCore.Qt.AlignHCenter
    markup_rect = painter.boundingRect(x, y, width, height, alignment, text)

    # determine scaling factor to get into requested bounding-box
    scale = min(width / markup_rect.width(),
                height / markup_rect.height())

    _painter_set_attributes(painter, attrs)

    # render using computed scale
    center = QtCore.QPointF(x + width / 2, y + height / 2)
    painter.translate(center)
    painter.scale(scale, scale)
    painter.translate(-center)

    painter.drawText(markup_rect, alignment, text)
    painter.restore()

def _draw_line(painter, x0, y0, x1, y1, **attrs):
    painter.save()
    _painter_set_attributes(painter, attrs)
    painter.drawLine(x0, y0, x1, y1)
    painter.restore()
def _draw_rectangle(painter, x, y, w, h, **attrs):
    painter.save()
    _painter_set_attributes(painter, attrs)
    painter.drawRect(x, y, w, h)
    painter.restore()
def _draw_rounded_rectangle(painter, x, y, w, h, radius, **attrs):
    painter.save()
    _painter_set_attributes(painter, attrs)
    painter.drawRoundedRect(x, y, w, h, radius, radius)
    painter.restore()
def _draw_circle(painter, center_x, center_y, radius, **attrs):
    painter.save()
    _painter_set_attributes(painter, attrs)
    painter.drawEllipse(QtCore.QPointF(center_x, center_y), radius, radius)
    painter.restore()
def _draw_polygon(painter, pointlist, **attrs):
    painter.save()
    _painter_set_attributes(painter, attrs)
    painter.drawPolygon(QtGui.QPolygonF([QtCore.QPointF(x, y) for x, y in pointlist]))
    painter.restore()
