# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import sys
if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

from .abstract.Euclidian2D import Euclidian2D
from Omaha import Core
import Overlord

class HolderState(Euclidian2D.State):
    def __init__(self, holder):
        assert isinstance(holder, RectBoard), "%s is not a RectBoard" % (holder,)
        super().__init__(holder)

    def __str__(self):
        gamestate = self.holder.game.scratch_state
        piece_notation = self.holder.game.default_notation.piece_notation
        return "[ " + (" / ".join(" ".join((piece_notation.piece_name(gamestate.piece_state(piece))
                                         if piece else ".")
                                        for piece in row)
                               for row in self.__squares)) + " ]"

class RectBoard(Euclidian2D):
    """Rectangle board made of squares."""
    State: TypeAlias = HolderState

    def __init__(self, *, ncolumns: int, nrows: int, **kwargs):
        super().__init__(**kwargs)
        self.__ncolumns, self.__nrows = ncolumns, nrows

    # Euclidian2D properties
    @property
    def matrix_width(self) -> int:
        return self.__ncolumns
    @property
    def matrix_height(self) -> int:
        return self.__nrows

class OnePiecePerLocRectBoard(RectBoard, Core.OnePiecePerLocHolder):
    class State(RectBoard.State, Core.OnePiecePerLocHolder.State):
        pass
