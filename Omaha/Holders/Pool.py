# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from frozendict import frozendict

class PoolLocation(Core.PieceHolder.Location):
    def __init__(self, holder, piecegroup):
        assert isinstance(holder, PiecePool), "%s is not a PiecePool" % holder
        assert not isinstance(piecegroup, int)
        super().__init__(holder)
        self.__piecegroup = piecegroup
    @property
    def piecegroup(self):
        return self.__piecegroup
    def __str__(self):
        return "(%s)-in-pool" % (self.__piecegroup,)
    def __eq__(self, other):
        return self.holder is other.holder and self.__piecegroup == other.__piecegroup

class HolderState(Core.OnePieceTypePerLocHolderState):
    def __init__(self, holder):
        super().__init__(holder)
        self.__pieces_by_group = {}

    @property
    def piece_groups(self):
        return self.__pieces_by_group.keys()
    def num_pieces_of_group(self, piecegroup):
        return len(self.__pieces_by_group[piecegroup])

    def piece_location_by_group(self, piecegroup):
        """Return location for 1st piece of given group, or None."""
        location = self.holder.Location(self.holder, piecegroup)
        assert self.holder.piecegroup(self.piece_at(location)) is piecegroup
        return location

    def clone(self):
        c = super().clone()
        c.__pieces_by_group = {typ: contents.clone()
                               for typ, contents in self.__pieces_by_group.items()}
        return c
    def freeze(self):
        super().freeze()
        self.__pieces_by_group = frozendict({typ: contents.frozen()
                                             for typ, contents in self.__pieces_by_group.items()})

    @property
    def pieces(self):
        for pieces in self.__pieces_by_group.values():
            # yield from pieces # python3 only!
            for piece in pieces:
                yield piece

    def put_piece(self, location, piece):
        super().put_piece(location, piece)
        group = self.holder.piecegroup(piece)
        if location.piecegroup != None:
            assert location.piecegroup is group, "%r is not %r" % (location.piecegroup, group)
        if group not in self.__pieces_by_group:
            self.__pieces_by_group[group] = self.LocationContents()
        self.__pieces_by_group[group].add_piece(piece)
    def take_piece(self, piece):
        super().take_piece(piece)
        group = self.holder.piecegroup(piece)
        self.__pieces_by_group[group].remove_piece(piece)
        if len(self.__pieces_by_group[group]) == 0:
            del self.__pieces_by_group[group]

    def contents_at(self, location):
        return self.__pieces_by_group[location.piecegroup]

    def piece_at(self, location):
        assert location.holder is self.holder
        try:
            # return a "random" element from the set
            for piece in self.__pieces_by_group[location.piecegroup].pieces:
                return piece
        except KeyError:
            return None

    def piece_location(self, piece):
        """
        The location of the given piece.

        We need that because we do not store the index in the
        piece's location (see put_piece).
        """
        group = self.holder.piecegroup(piece)
        assert group in self.__pieces_by_group, "%s not in %s" % (group, self.__pieces_by_group)
        if piece not in self.__pieces_by_group[group]: # FIXME: do we really care ?
            return None
        # FIXME that's fishy, can return location for another piece of same group
        return self.holder.Location(self.holder, group)

class PiecePool(Core.PieceHolder):
    """A pool in which a player can keep pieces.

    Pieces in a pool are retrieved by their piecegroup."""

    State = HolderState
    Location = PoolLocation

    def __init__(self, piecegroup_func, **kwargs):
        super().__init__(**kwargs)
        self.__piecegroup_func = piecegroup_func

    def piecegroup(self, piece):
        return self.__piecegroup_func(piece)

    def location_desc(self, location):
        # there is no fixed set of locations and nothing special to describe
        return self.LocationDesc()
