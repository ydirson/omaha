# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

import logging
logger = logging.getLogger(__name__)

class PointerBasedMoveComposer(Core.MoveComposer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__action_handles = {}

    def connect_to_gui(self):
        logger.debug("connect_to_gui")
        for specialmove_name, specialmove_args in self.gui.game.Move.special_moves.items():
            self.__action_handles[specialmove_name] = \
                self.gui.register_game_action(
                    specialmove_name,
                    self.specialmove_handler(specialmove_name, specialmove_args))
        self.gui.tk.set_mouse_release_callback(self.gui.canvas,
                                               self.mouse_release)
        self.gui.tk.set_mouse_press_callback(self.gui.canvas,
                                             self.mouse_press)
        self.gui.tk.set_mouse_move_callback(self.gui.canvas,
                                            self.mouse_move)

    def disconnect_from_gui(self):
        logger.debug("disconnect_from_gui")
        self.gui.tk.unset_mouse_release_callback(self.gui.canvas,
                                                 self.mouse_release)
        self.gui.tk.unset_mouse_press_callback(self.gui.canvas,
                                               self.mouse_press)
        self.gui.tk.unset_mouse_move_callback(self.gui.canvas,
                                              self.mouse_move)
        for handler in self.__action_handles.values():
            self.gui.unregister_game_action(handler)

    def specialmove_handler(self, name, args):
        def handler():
            gamestate = self.gui.game.last_state
            try:
                self.send_move_to_playerdriver(
                    self.gui.game.create_move(gamestate=gamestate,
                                              player=gamestate.whose_turn, **args))
            except Core.InvalidMove as ex:
                logger.error("Move refused: %s", ex)
            return True
        handler.__name__ = 'specialmove_handler for {0}'.format(name)
        return handler

    def mouse_press(self, x, y):
        location = self.gui.game_renderer.point_to_location(x, y)
        self.__highlight_maybe(location)
        return True

    def mouse_move(self, x, y, pressed):
        if pressed:
            location = self.gui.game_renderer.point_to_location(x, y)
            self.__highlight_maybe(location)
        return True

    def __highlight_maybe(self, location):
        if location is None:
            self.gui.game_renderer.clear_highlights(Core.LocHighlights.MAYBE)
            return

        hl = self.gui.game_renderer.highlights(Core.LocHighlights.MAYBE)
        if len(hl) == 0 or location != hl[0]:
            self.gui.game_renderer.clear_highlights(Core.LocHighlights.MAYBE)
            self.gui.game_renderer.add_highlights(Core.LocHighlights.MAYBE,
                                                  [location])

    def mouse_release(self, x, y):
        self.gui.game_renderer.clear_highlights(Core.LocHighlights.MAYBE)

        return True
