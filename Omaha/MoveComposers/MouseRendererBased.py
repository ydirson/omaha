# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
A human player for chess-like games.  Only capable for now of moves
of the origin-click/destination-click type.
"""

import logging

from Omaha import Core
from .abstract.PointerBased import PointerBasedMoveComposer

logger = logging.getLogger(__name__)

class MouseRendererBasedMoveComposer(PointerBasedMoveComposer):
    """
    A player driver for renderers providing areas for move generation.
    """

    def mouse_release(self, x: int, y: int) -> bool:
        """Handler for a mouse release signal in gui."""
        if not super().mouse_release(x, y):
            return False

        move_args = self.gui.game_renderer.point_to_move_args(x, y)

        if move_args is None:
            # click was out of any active area, ignore
            return True

        # build move
        gamestate=self.gui.game.last_state
        move = self.gui.game.create_move(gamestate=gamestate, player=gamestate.whose_turn,
                                         **move_args)

        # send move to GUI
        try:
            self.send_move_to_playerdriver(move)
        except Core.GameSuspended:
            logger.debug("game is suspended, ignore")
        except Core.InvalidMove as ex:
            logger.error("invalid move %s: %s", move, ex)
        except Core.IncompleteMove as ex:
            logger.error('unsupported game ("incomplete move" %s) ? %s', move, ex)

        return True
