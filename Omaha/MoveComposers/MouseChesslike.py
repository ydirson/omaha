# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
A human player for chess-like games.  Only capable for now of moves
of the origin-click/destination-click type.
"""

from Omaha import Core
from .abstract.PointerBased import PointerBasedMoveComposer
import logging

logger = logging.getLogger(__name__)

class MouseChesslikeMoveComposer(Core.AccumulatingMoveComposer, PointerBasedMoveComposer):
    """
    Move composer for moving pieces with the mouse.

    It supports games with a board and per-player pools.
    """

    def __reset_move(self):
        """Move was applied or cancelled, prepare for next one."""
        self.current_move = None
        self.gui.game_renderer.clear_highlights(Core.LocHighlights.COMPOSING)
        self.gui.game_renderer.clear_highlights(Core.LocHighlights.REACHABLE)

    def __set_source(self, location):
        self.current_move.set_source(location)
        self.gui.game_renderer.clear_highlights(Core.LocHighlights.COMPOSING)
        self.gui.game_renderer.add_highlights(Core.LocHighlights.COMPOSING,
                                              [location])
        self.gui.game_renderer.clear_highlights(Core.LocHighlights.REACHABLE)
        self.gui.game_renderer.add_highlights(
            Core.LocHighlights.REACHABLE,
            [ move.target for move in
              self.gui.game.valid_move_completions(self.current_move.gamestate, self.current_move)] )

    def mouse_release(self, x, y):
        """Handler for a mouse release signal in gui."""
        if not super().mouse_release(x, y):
            return False

        gamestate = self.gui.game.scratch_state
        location = self.gui.game_renderer.point_to_location(x, y)

        if location is None or not gamestate.valid_location(location):
            # click was out of all holders, restart move if any
            self.__reset_move()
            return True

        try:
            piece = gamestate.piece_at(location)
        except IndexError:
            # click was eg. in an empty position in a Pool, restart move if any
            self.__reset_move()
            return True
        if piece != None:
            piece_owner = gamestate.piece_owner(piece)

        player = self.gui.game.last_state.whose_turn

        if self.current_move is None:
            self.current_move = self.gui.game.create_move(gamestate=self.gui.game.last_state,
                                                          player=player)
        if self.current_move.source is None:
            if piece is None or piece_owner is not player:
                # no piece in this location, or opponent's piece
                return True

            self.__set_source(location)
        else:
            # moves must be towards board, refuse moving to pools
            if location.holder != self.gui.game.board:
                if piece_owner is player:
                    # reset source of move to piece in pool
                    self.__set_source(location)
                # else: just ignore clicks on opponent's pool
                return True

            self.current_move.set_target(location)

            # send move to game
            try:
                self._finalize_move()
                self.__reset_move()
            except Core.GameSuspended:
                logger.debug("game is suspended, cancel")
                self.__reset_move()
            except Core.InvalidMove as ex:
                if piece is not None and piece_owner is player:
                    logger.debug("invalid move, reseting source instead")
                    self.__set_source(location)
                else:
                    logger.error(
                        "invalid move %s: %s",
                        self.gui.game.default_notation.move_serialization(self.current_move), ex)
                    self.__reset_move()
            except Core.UserCancel:
                pass
        return True
