# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord
from Overlord.misc import classproperty
import logging

logger = logging.getLogger(__name__)

class MultiplexerMoveComposer(Core.MoveComposer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__drivers = []

    @classproperty
    @classmethod
    def context_key(cls):
        # This is a quick hack to avoid the multiplexer to take the
        # MoveComposer context slot.
        return MultiplexerMoveComposer

    def connect_to_gui(self):
        for driver in self.driver_plugins:
            cls = driver.Class
            drv = cls(gui=self.gui, parentcontext=self.context)
            drv.connect_to_gui()
            self.__drivers.append(drv)
        logger.debug("connect: %s", self.__drivers)

    def disconnect_from_gui(self):
        for driver in self.__drivers:
            driver.disconnect_from_gui()
        self.__drivers = []

    @Overlord.parameter
    def driver_plugins(cls, context):
        # FIXME: this indeed depends on game
        return Overlord.params.MultiChoice(
            label = 'Drivers',
            alternatives=dict(
                [ (driver.name, driver)
                  for driver in context.plugin_manager. \
                      get_plugins_list('X-Omaha-MoveComposer') ]),
            )
