# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
A human player for chess-like games.  Only capable for now of moves
of the origin-click/destination-click type.
"""

from Omaha import Core
from .abstract.PointerBased import PointerBasedMoveComposer
import logging

logger = logging.getLogger(__name__)

class MouseSingleClickMoveComposer(PointerBasedMoveComposer):
    """
    A player driver for games placing pieces on a board.

    Does not support moving a piece from one place to another.
    """

    def mouse_release(self, x, y):
        """Handler for a mouse release signal in gui."""
        if not super().mouse_release(x, y):
            return False

        location = self.gui.game_renderer.point_to_location(x, y)

        if location is None:
            # click was out of board area, ignore
            return True

        # build move
        gamestate = self.gui.game.last_state
        move = self.gui.game.create_move(gamestate=self.gui.game.last_state,
                                         player=gamestate.whose_turn)
        move.set_target(location)

        # send move to GUI
        try:
            #FIXME: lacks support for confirmation
            self.send_move_to_playerdriver(move)
        except Core.GameSuspended:
            logger.debug("game is suspended, ignore")
        except Core.InvalidMove as ex:
            logger.error("invalid move %s: %s", move, ex)
        except Core.IncompleteMove as ex:
            logger.error('unsupported game ("incomplete move" %s) ? %s', move, ex)

        return True
