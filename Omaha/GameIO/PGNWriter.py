# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.abstract import ChessGame
from Omaha.Games.MoveNotations import StandardShogiMN
from Omaha.Games import ToriShogi
from Omaha import Core
from Overlord.misc import class_fullname

class PGNGameWriter(Core.GameWriter):
    def __init__(self, match):
        if class_fullname(type(match.game)) not in pgngames:
            raise RuntimeError("Unsupported game class %r" %
                               (class_fullname(type(match.game)),))
        self.__match = match
        self.__game = match.game

    def write_to(self, stream):
        variant, notationclass = pgngames[class_fullname(type(self.__game))]
        notation = notationclass(self.__game)
        if variant is not None:
            stream.write('[Variant "%s"]\n' % (variant,))

        player_drivers = [self.__match._player_drivers[p] for p in self.__game.players]
        if player_drivers[0].name:
            stream.write('[White "%s"]\n' % (player_drivers[0].name,))
        if player_drivers[1].name:
            stream.write('[Black "%s"]\n' % (player_drivers[1].name,))
        if len(self.__game.players) > 2:
            if player_drivers[2].name:
                stream.write('[Player3 "%s"]\n' % (player_drivers[2].name,))
        if len(self.__game.players) > 3:
            if player_drivers[3].name:
                stream.write('[Player4 "%s"]\n' % (player_drivers[3].name,))

        # FIXME we may want to enhance result notation for more than 2 players
        if len(self.__game.players) == 2 and self.__game.phase is Core.GamePhase.Over:
            if   (self.__game.players[0] in self.__game.outcome and
                  self.__game.outcome[self.__game.players[0]] == self.__game.Outcomes.Winner):
                result = "1-0"
            elif (self.__game.players[1] in self.__game.outcome and
                  self.__game.outcome[self.__game.players[1]] == self.__game.Outcomes.Winner):
                result = "0-1"
            elif (self.__game.players[0] in self.__game.outcome and
                  self.__game.outcome[self.__game.players[0]] == self.__game.Outcomes.IsDraw):
                result = "1/2-1/2"
            else:
                assert False, "cannot interpret game outcome %s" % (self.__game.outcome)
        else:
            result = "*"
        stream.write('[Result "%s"]\n' % (result,))

        for move in self.__game.moves.played:
            # FIXME: should issue move numbers
            #if move.player.color is game.COLOR_WHITE:
            #    stream.write("%d. " % )
            stream.write(notation.move_serialization(move))
            stream.write(" ")
            # FIXME: should word-wrap

        # FIXME: termination
        stream.write("\n%s\n" % (result,))

pgngames = {
    "Omaha.Games.Chess.Chess": (None, ChessGame.SANNotation),
    "Omaha.Games.Shogi.Shogi": ("shogi", StandardShogiMN.SANNotation),
    "Omaha.Games.MiniShogi.MiniShogi": ("minishogi", StandardShogiMN.SANNotation),
    "Omaha.Games.GoroGoroShogi.GoroGoroShogi": ("gorogoro_shogi", StandardShogiMN.SANNotation),
    "Omaha.Games.JudkinsShogi.JudkinsShogi": ("judkinsshogi", StandardShogiMN.SANNotation),
    "Omaha.Games.ToriShogi.ToriShogi": ("torishogi", ToriShogi.SANNotation),
    #"Omaha.Games.Forchess.Forchess": ("x-omaha-forchess", ChessGame.SANNotation),
    "Omaha.Games.Forchess.CutthroatForchess": ("x-omaha-forchess-cutthroat", ChessGame.SANNotation),
    }
