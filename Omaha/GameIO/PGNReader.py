# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Games.abstract import ChessGame
from Omaha.Games.MoveNotations import StandardShogiMN
from Omaha.Games import ToriShogi
import Overlord

import logging
logger = logging.getLogger(__name__)

class PGNGameReader(Core.GameReader):
    suffix = ".pgn"

    @property
    def VARIANTS(self):
        v = {
            None: ("Chess", ChessGame.SANNotation,
                   self.chess_init_game, self.chess_inject_move),
            "shogi": ("Shogi", StandardShogiMN.SANNotation,
                      self.chess_init_game, self.chess_inject_move),
            "minishogi": ("MiniShogi", StandardShogiMN.SANNotation,
                          self.chess_init_game, self.chess_inject_move),
            "gorogoro_shogi": ("GoroGoroShogi", StandardShogiMN.SANNotation,
                               self.chess_init_game, self.chess_inject_move),
            "judkinsshogi": ("JudkinsShogi", StandardShogiMN.SANNotation,
                             self.chess_init_game, self.chess_inject_move),
            "torishogi": ("ToriShogi", ToriShogi.SANNotation,
                          self.chess_init_game, self.chess_inject_move),
            "x-omaha-forchess": ("Forchess", ChessGame.SANNotation,
                                 self.chess_init_game, self.chess_inject_move),
            "x-omaha-forchess-cutthroat": ("CutthroatForchess", ChessGame.SANNotation,
                                           self.chess_init_game, self.chess_inject_move),
        }

        # aliases due to xboard using the engine-provided name
        v["gorogoro"] = v["gorogoro_shogi"] # Fairy-Stockfish
        v["judkins"] = v["judkinsshogi"] # Fairy-Stockfish, SjaakII
        v["judkinshogi"] = v["judkinsshogi"] # CrazyWa typo

        return v

    def __init__(self, plugin_manager, parentcontext):
        self.plugin_manager = plugin_manager
        self.context = parentcontext.clone()
        self.__game = None
        self.__game_plugin = None

        # FIXME: global stuff stinks - but pyparsing seems to have
        # problem with methods, and we must(?) use functions for
        # hooks, so...
        global pgn_contents
        pgn_contents = dict(tags={}, moves=[])

    def game(self):
        return self.__game
    def game_plugin(self):
        return self.__game_plugin

    def parse(self, stream):
        assert not self.__game
        assert not self.__game_plugin

        data = ''.join(stream.readlines())
        parse_pgn(data)

        tags = pgn_contents['tags'] # shortcut
        variant = tags['Variant'] if 'Variant' in tags else None
        logger.debug("variant %r", variant)
        try:
            gamekind, notationclass, init_game, inject_move = self.VARIANTS[variant]
        except KeyError:
            raise Core.UserNotification(
                "error reading game file",
                "Game variant %r currently not supported by PGN reader" %
                (tags['Variant'],))

        self.__game_plugin = self.plugin_manager.get_plugin('X-Omaha-Game', gamekind)

        game = init_game(self.__game_plugin.Class, pgn_contents, self.context)
        game.setup()
        self.notation = notationclass(game)
        parse_error = None
        for move in pgn_contents['moves']:
            try:
                inject_move(game, move)
            except Core.ParseError as ex:
                # don't raise an exception to caller, so user can
                # still see what could be successfully loaded
                parse_error = "could not interpret next move: %s" % (ex,)
                # FIXME should keep the information in some way
                break

        game.done_importing()
        self.__game = game
        if parse_error:
            raise Core.IncompleteGame(game, parse_error)
        return game

    def chess_init_game(self, game_class, pgn_contents, context):
        # setup game instance
        param_decls = game_class.parameters_dcl(context)
        params = {}
        for paramid, decl in param_decls.items():
            if paramid == 'handicap':
                # today, only for shogi games
                param = Overlord.Param(decl=decl)
                params[paramid] = param
            else:
                raise AssertionError("Unknown parameter %r" % (paramid,))
        return game_class(parentcontext=self.context, import_mode=True, paramvalues=params)

    def chess_inject_move(self, game, move):
        logger.debug("chess_inject_move(%r)", move)
        game.make_move(
            game.scratch_state.whose_turn,
            self.notation.move_deserialization(game.last_state, move, player=game.scratch_state.whose_turn))

## parser

def record_tag(tokens):
    pgn_contents['tags'][tokens[1]] = tokens[2]
def record_move(tokens):
    # FIXME handle initial n... for black
    # FIXME check consistency of move numbers
    pgn_contents['moves'].append(tokens[0])

from pyparsing import Word, ZeroOrMore, OneOrMore, QuotedString, Optional
from pyparsing import alphanums, nums, printables, oneOf
from pyparsing import Regex

TagName = Word( alphanums + "_" )
TagValue = QuotedString(quoteChar='"', escChar='\\')
TagPair = ("[" + TagName + TagValue + "]").setParseAction(record_tag)
BraceComment = Regex(r"{[^}]*}")

Termination = oneOf('0-1 1-0 1/2-1/2 *')

MoveNum = Word( nums ) + ZeroOrMore(".") # type: ignore[arg-type] # FIXME pyparsing issue #456
Move = ( ~Termination + Optional(MoveNum) + ZeroOrMore(BraceComment) +
         Word( printables ).setParseAction(record_move) +
         ZeroOrMore(BraceComment) )

pgn_grammar = (ZeroOrMore(TagPair) + ZeroOrMore(BraceComment) +
               OneOrMore(Move) + Termination + ZeroOrMore(BraceComment))

def parse_pgn(data):
    pgn_grammar.parseString(data)
