# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
A Base PlayerDriver class speaking the Go Text Protocol for Omaha.
Spec for GTP v2 is at http://www.lysator.liu.se/~gunnar/gtp/.
"""

from .protocols.GTP2 import GTP2
from Omaha import Core
from Omaha.Core.misc import safe_popen
import Overlord
import logging

logger = logging.getLogger(__name__)

class GTP2Program(Core.AIPlayerDriver, GTP2):

    def __init__(self, **kwargs):
        super(GTP2Program, self).__init__(**kwargs)
        self.__process = None

    def disconnect(self):
        logger.debug("disconnecting GTP2Program...")
        self.gui.game.unregister_listener(self.handle_event)
        self.gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.ERROR,
                                          self.__process.stdout, self.handle_ioerror)
        self.gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                          self.__process.stdout, self.handle_data)
        self.gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                          self.__process.stderr, self.handle_stderr)
        self.write("quit")
        self.__process.communicate()
        self.__process = None

    @Overlord.parameter
    def program(cls, context):
        # FIXME: this indeed depends on game
        return Overlord.params.String(
            label = 'Command',
            default="gnugo --mode gtp",
            )

    def connect(self):
        logger.debug("Launching command {0!r}".format(self.program))
        self.__process = safe_popen(self.program)

        self.gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                        self.__process.stdout,
                                        self.handle_data)
        self.gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.ERROR,
                                        self.__process.stdout,
                                        self.handle_ioerror)
        self.gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                        self.__process.stderr,
                                        self.handle_stderr)
        self.gui.game.register_listener(self.handle_event)

        self.gtp_get_info()
        self.gtp_init_game()

        if self.gui.game.moves.nmoves_done > 0:
            logger.info("Injecting state position to AI...")
            self.set_game()

        self.gui.game.signal_ready(self.player, True)

    def handle_event(self, event):
        super(GTP2Program, self).handle_event(event)

        if isinstance(event, Core.Game.PhaseChangeEvent):
            if event.newphase is Core.GamePhase.Playing:
                # launch game
                if self.player is self.gui.game.scratch_state.whose_turn:
                    self.request_move()

    def write(self, data):
        logger.debug("%s>%s", self.color[self.player][0], data)
        self.__process.stdin.write(data + "\n")

    def handle_stderr(self, stream, arg): # pylint: disable=unused-argument
        line = stream.readline().strip()
        logger.debug("E:%s<%s", self.color[self.player][0], line)

    def handle_ioerror(self, stream, arg): # pylint: disable=unused-argument
        logger.warning("connection lost with engine")
        self.disconnect()
