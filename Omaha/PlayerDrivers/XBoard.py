# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Omaha.Core.misc import safe_popen
from Omaha.Games.abstract import ChessGame, GenericChesslike
from Omaha.Games.MoveNotations import ShogiGameMN
from Omaha.Games.PieceNotations import StandardShogiPN

# FIXME: importing everyone here is silly
from Omaha.Games import Chess, Shogi, MiniShogi, GoroGoroShogi, JudkinsShogi, ShoShogi, ToriShogi
import Overlord
from Overlord.misc import class_fullname
import os, re, logging

logger = logging.getLogger(__name__)

class XBoardNotation(GenericChesslike.ChessLikeCoordinateNotation):
    "Chess-like coordinate notation, where numbering starts at 0 for boards of height 10."
    class LocationNotation(GenericChesslike.ChessLikeCoordinateNotation.LocationNotation):
        def __init__(self, game):
            super(XBoardNotation.LocationNotation, self).__init__(game)
            self.__offset = 1 if self.game.boardheight == 10 else 0
        def rowname(self, location):
            return "%s" % (location.row + 1 + self.__offset)
        def row(self, rowname):
            return int(rowname) - 1 + self.__offset

class XBoardEditPositionNotation(Core.PositionNotation):
    LocationNotation = XBoardNotation.LocationNotation
    def position_serialization(self, gamestate):
        return '\n'.join(
            ["#"] +             # clear board first
            # first, sente pieces
            [self._piece_position(p, gamestate)
             for p in gamestate.pieces
             if gamestate.piece_owner(p) is self.game.players[0]] +
            # then, gote pieces
            ["c"] +             # switch color
            [self._piece_position(p, gamestate)
             for p in gamestate.pieces
             if gamestate.piece_owner(p) is self.game.players[1]] +
            ["."]               # done
        )
    def _piece_position(self, piece, gamestate):
        raise NotImplementedError()

class XBoardChessNotation(XBoardNotation, ChessGame.ChessCoordinateNotation):
    pass

class XBoardEditChessPositionNotation(XBoardEditPositionNotation):
    PieceNotation = ChessGame.EnglishChessNotation
    def _piece_position(self, piece, gamestate):
        piecestate = gamestate.piece_state(piece)
        pname = self.piece_notation.piece_name(piecestate)
        return pname + self.location_notation.location_name(piece.location(gamestate))

class XBoardGenericShogiNotation(XBoardNotation):
    def move_serialization(self, move):
        if move.source.holder is not self.game.board:
            return "%s@%s" % (self.piece_notation.piece_name(
                self.game.scratch_state.piece_state(move.piece)),
                              self.location_notation.location_name(move.target))
        promotion = "+" if move.promotes else ""
        return (super(XBoardGenericShogiNotation, self).move_serialization(move)
                + promotion)

    regexp = (r"(?:.?)" +
              GenericChesslike.ChessLikeCoordinateNotation.regexp +
              r"(\+?)|(.)@([a-w]+)(\d+)")
    def move_from_match(self, gamestate, match, player):
        if match.group(4):      # is a drop
            ptypename, col, row = match.groups()[3:6]
            # identify requested piece type
            piece_type = self.piece_notation.piece_type(ptypename)
            # check for piece to drop
            source = self.game.piece_location_by_type_inplayerpool(gamestate,
                                                                   piece_type, player)
            if source is None:
                raise Core.ParseError("No such piece to drop")
            return self.game.Move(
                gamestate=gamestate, player=player, source=source,
                target=self.game.board.Location(self.game.board,
                                                self.location_notation.col(col),
                                                self.location_notation.row(row)))
        move = super(XBoardGenericShogiNotation, self).move_from_match(gamestate, match, player)
        if len(match.group(3)) == 0:
            move.promotes = False
        else:
            move.promotes = True
        return move

class XBoardShogiNotation(XBoardGenericShogiNotation):
    movename = "xboard shogi coordinates move"
    PieceNotation = StandardShogiPN.EnglishShogiPieceNotation

class XBoardToriShogiNotation(XBoardGenericShogiNotation):
    movename = "xboard ToriShogi coordinates move"
    PieceNotation = ToriShogi.XboardPieceNotation

class XBoardEditShogiPositionNotation(XBoardEditPositionNotation):
    PieceNotation = StandardShogiPN.EnglishShogiPieceNotation
    def _piece_position(self, piece, gamestate):
        pname = self.piece_notation.piece_name(self.game.scratch_state.piece_state(piece))
        if gamestate.piece_holder(piece) is self.game.board:
            return (pname + self.location_notation.location_name(piece.location(gamestate)) +
                    ("+" if gamestate.piece_state(piece).promoted else ""))
        else: # prisonner
            return pname + "*"

game_defs = {
    Chess.Chess: dict(
        notation_class=XBoardChessNotation,
        san_class=ChessGame.SANNotation,
        edit_notation_class=XBoardEditChessPositionNotation,
        names=["normal"]),
    Shogi.Shogi: dict(
        notation_class=XBoardShogiNotation,
        edit_notation_class=XBoardEditShogiPositionNotation,
        names=["shogi"]),
    MiniShogi.MiniShogi: dict(
        notation_class=XBoardShogiNotation,
        edit_notation_class=XBoardEditShogiPositionNotation,
        names=["minishogi", "5x5+5_shogi"]),
    ToriShogi.ToriShogi: dict(
        notation_class=XBoardToriShogiNotation,
        edit_notation_class=XBoardEditShogiPositionNotation,
        names=["torishogi", "tori", "7x7+6_shogi"]),
    ShoShogi.ShoShogi: dict(
        notation_class=XBoardShogiNotation,
        edit_notation_class=XBoardEditShogiPositionNotation,
        names=["shoshogi", "sho", "9x9+0_shogi"]),
    GoroGoroShogi.GoroGoroShogi: dict(
        notation_class=XBoardShogiNotation,
        edit_notation_class=XBoardEditShogiPositionNotation,
        names=["gorogoro", "gorogoro_shogi"]),
    JudkinsShogi.JudkinsShogi: dict(
        notation_class=XBoardShogiNotation,
        edit_notation_class=XBoardEditShogiPositionNotation,
        names=["judkinshogi", "judkins"]),
    }

# FIXME refuse "Illegal move"
# FIXME "undo"
class XBoardPlayerDriver(Core.AIPlayerDriver):

    def __init__(self, **kwargs):
        super(XBoardPlayerDriver, self).__init__(**kwargs)
        self.__process = None
        self.__entering_move = None
        self.__hasping, self.__use_usermove = None, None
        self.__features, self.__options = {}, []
        self.__engine_variant = "normal"
        # identify reference gameclass to use when looking in game_defs
        for gamecls in game_defs.keys():
            if issubclass(type(self.gui.game), gamecls):
                self.__refgamecls = gamecls
                break
        else:
            raise RuntimeError("%r is not supported by XBoardPlayerDriver" %
                               (self.gui.game,))

        self.__notation = (
            game_defs[self.__refgamecls]['notation_class'](self.gui.game))
        if 'san_class' in game_defs[self.__refgamecls]:
            self.__san_notation = (
                game_defs[self.__refgamecls]['san_class'](self.gui.game))
        else:
            self.__san_notation = None
        self.__edit_notation = (
            game_defs[self.__refgamecls]['edit_notation_class'](self.gui.game))

        if self.player is self.gui.game.players[0]:
            self.__letter = 'W'
        else:
            self.__letter = "B"

    @classmethod
    def parameters_dcl(cls, context):
        d = dict(super(XBoardPlayerDriver, cls).parameters_dcl(context))
        name_decl = d['name']
        d['name'] = name_decl.clone(hidden=True)
        assert d['name'].hidden
        return d

    def set_game(self):
        self.write("edit")
        self.write(self.__edit_notation.position_serialization(self.gui.game.scratch_state))

        # tell engine whose turn it is
        if self.gui.game.scratch_state.whose_turn is self.gui.game.players[0]:
            self.write("white")
        else:
            self.write("black")

    def disconnect(self):
        logger.info("disconnecting XBoardPlayerDriver...")
        self.gui.game.signal_ready(self.player, False)
        self.gui.game.unregister_listener(self.handle_event)
        self.gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.ERROR,
                                          self.__process.stdout, self.handle_ioerror)
        self.gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                          self.__process.stdout, self.handle_data)
        self.gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                          self.__process.stderr, self.handle_stderr)
        self.write("quit")
        self.__process.communicate()
        self.__process = None

    @Overlord.parameter
    def program(cls, context):
        return Overlord.params.String(
            label = 'Command',
            default="gnuchess -x",
            )
    @Overlord.parameter
    def depth(cls, context):
        return Overlord.params.Int(
            label = 'Exploration Depth',
            default = 10,           # 39 by default
            minval = 1,
            maxval = 59             # arbitrary
            )

    def connect(self):
        logger.debug("Launching command {0!r}".format(self.program))
        self.__process = safe_popen(self.program)

        # default name, overridable by "feature myname"
        self.name = os.path.basename(self.program)

        self.gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                        self.__process.stdout,
                                        self.handle_data)
        self.gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.ERROR,
                                        self.__process.stdout,
                                        self.handle_ioerror)
        self.gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                        self.__process.stderr,
                                        self.handle_stderr)
        self.gui.game.register_listener(self.handle_event)

        self.__entering_move = None
        # be sure engine is using XBoard 2 protocol
        self.write("xboard")
        self.write("protover 2")

    def setup_game(self):
        self.write("new") # fairymax won't start as black without "new"
        self.write("variant " + self.__engine_variant)

        if ( self.gui.game.moves.nmoves_done > 0 or
             self.gui.game.handicap ):
            logger.info("Injecting state position to AI...")
            self.set_game()

        # request prompt for exploration depth
        self.write("sd %d" % self.depth)
        # non-standard equivalent in case this is gnuchess
        self.write("depth %d" % self.depth)

        self.gui.game.signal_ready(self.player, True)

    def write(self, data):
        logger.debug("%s>%s", self.__letter, data)
        self.__process.stdin.write(data + "\n")

    def handle_event(self, event):
        if isinstance(event, Core.Game.MoveEvent):
            if self.gui.game.scratch_state.piece_owner(event.move.piece) is not self.player:
                movestr = self.__notation.move_serialization(event.move)
                self.__entering_move = movestr
                if self.__use_usermove:
                    self.write("usermove " + movestr)
                else:
                    self.write(movestr)
        elif isinstance(event, Core.Game.PhaseChangeEvent):
            if event.newphase is Core.GamePhase.Playing:
                # launch game
                if self.player is self.gui.game.scratch_state.whose_turn:
                    self.write("go")
            elif event.newphase in (Core.GamePhase.Suspended,
                                    Core.GamePhase.Over):
                if self.gui.game.scratch_state.whose_turn is self.player:
                    self.write("force")

    def handle_data(self, stream, arg): # pylint: disable=unused-argument
        line = stream.readline()
        if line == "":
            logger.warn("%s EOF", self.player.name)
            self.disconnect()
        line = line.strip()
        logger.debug("%s<%s", self.__letter, line)

        if line.startswith('#'): # comment
            return

        if self.__entering_move:
            # accepted opponent move ?
            match = re.match(r'\d+\. (?:' + self.__entering_move + ')', line)
            if match:
                self.__entering_move = None
                return                  # move was accepted

            # illegal move from opponent ?
            match = (re.match(r'Illegal move: *(' + self.__entering_move + r')', line) or
                     re.match(r'Illegal move \((' + self.__entering_move + r')\)', line) or
                     re.match(r'Invalid move: ' + self.__entering_move, line))
            if match:
                logger.warning('AI (%s) refused move', self.name)
                self.gui.game.refuse_move(self.gui.game.moves.last, self.player)
                # refresh display
                self.gui.tk.invalidate_canvas(self.gui.canvas)
                return

        # IA move ?
        match = ( re.match(r'\d+\. \.\.\. (\S+)', line) or
                  re.match(r'move (\S+)', line) )
        if match:
            try:
                move = self.__notation.move_deserialization(self.gui.game.last_state,
                                                            match.group(1),
                                                            self.player)
            except Core.ParseError:
                if not self.__san_notation:
                    raise
                move = self.__san_notation.move_deserialization(self.gui.game.last_state,
                                                                match.group(1),
                                                                self.player)

            try:
                self.gui.game.make_move(self.player, move)
            except Core.GameSuspended:
                logger.debug("move received while game is suspended, undoing")
                self.write("undo")
            except Core.InvalidMove as e:
                logger.critical("Game refused AI (%s) move %s: %s", self.name, match.group(1), e)
                raise
            else:
                # refresh display
                self.gui.tk.invalidate_canvas(self.gui.canvas)
            return

        # v2 features
        match = re.match('feature ', line)
        if match:
            while match:
                # look for key=value in what remains of line
                line = line[match.span()[1]:]
                if len(line) == 0: # all features succesfully parsed
                    break
                match = re.match(r'([^= ]*)=([^" ]+|"[^"]*") *', line)
                featname, featvalue = match.groups()
                # type normalization
                if featvalue[0] == '"':
                    featvalue = featvalue[1:-1] # strip quotes
                else:
                    try:
                        featvalue = int(featvalue)
                    except ValueError:
                        pass # just keep featvalue as a string
                # handle them
                if featname == "done":
                    if featvalue == 1:
                        if not self.process_features():
                            return
                        self.setup_game()
                    elif featvalue != 0:
                        logger.warning("unknown feature done=%s", featvalue)
                elif featname == "option":
                    self.__options += featvalue
                else:
                    self.__features[featname] = featvalue
            else:
                logger.warning("could not parse feature string %r", line)
            return

        if  (line == "resign" or
             self.player == self.gui.game.players[0] and line == '0-1 {resign}' or
             self.player == self.gui.game.players[1] and line == '1-0 {resign}'):
            self.gui.game.declare_resignation(self.player)
            return

        # victory/defeat ?
        winner = None
        if re.match(r'1-0|White mates', line):
            winner = self.gui.game.players[0]
        elif re.match(r'0-1|Black mates', line):
            winner = self.gui.game.players[1]
        if winner:
            outcome = {winner: self.gui.game.Outcomes.Winner}
            self.gui.game.propose_outcome(self.player, outcome)
            return

        if line.startswith('1/2-1/2'):
            outcome = {self.player: self.gui.game.Outcomes.IsDraw,
                       self.player.next: self.gui.game.Outcomes.IsDraw}
            self.gui.game.propose_outcome(self.player, outcome)
            return

        logger.info("unhandled IA output: %r", line)

    def handle_stderr(self, stream, arg): # pylint: disable=unused-argument
        line = stream.readline().strip()
        logger.debug("E:%s<%s", self.__letter, line)

    def handle_ioerror(self, stream, arg): # pylint: disable=unused-argument
        logger.error("(%s) connection lost with engine", self.__letter)
        self.disconnect()

    def process_features(self):
        for featname, featvalue in self.__features.items():
            if featname == 'myname':
                logger.debug("name: %s -> %s", self.name, featvalue)
                self.name = featvalue
            elif featname == 'ping':
                self.__hasping = (featvalue == 1)
            elif featname == 'usermove':
                self.__use_usermove = (featvalue == 1)
            elif featname == 'memory' and featvalue == 1:
                self.write("memory 1024") # FIXME: should be tunable
            elif featname == 'variants':
                variants = featvalue.split(',')
                for engine_variant in game_defs[self.__refgamecls]['names']:
                    if engine_variant in variants:
                        self.__engine_variant = engine_variant
                        break
                else:
                    logger.error("None of %r in supported variants %r",
                                 game_defs[self.__refgamecls]['names'],
                                 variants)
                    self.notify_listeners(Core.PlayerDriverFatalError(
                        player_driver=self,
                        message=("don't know how to play %r with this engine" %
                                 class_fullname(type(self.gui.game)))))
                    return False
            elif (featname in ['setboard', 'sigint'] and featvalue == 0 or
                  featname in ['analyze', 'colors', 'draw', 'ics', 'name',
                               'pause', 'playother', 'reuse', 'sigterm']):
                # we don't do anything special with them, but avoid sending "rejected"
                self.write("accepted " + featname)
            else:
                self.write("rejected " + featname)

        return True
