# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha.Games.piecetypes.shogi import ShogiFacetype
from Omaha.Games.abstract import Euclidian2D as Euclidian2DGame
from Omaha.Games.abstract.RectBoardGame import (RectBoardGame,
                                                NumberRowTTBNotation, NumberColumnRTLNotation)
from Omaha import Core
from Omaha.Core.misc import safe_popen
import Overlord
import os, re, logging

logger = logging.getLogger(__name__)

class CSALocationNotation(NumberRowTTBNotation, NumberColumnRTLNotation,
                          Euclidian2DGame.CoordinateNotation):
    LOCATION_REGEXP = r"(?P<column>\d)(?P<row>\d)"
    game: RectBoardGame

class CSAPieceNotation(Core.PieceNotation):
    # This PieceNotation does not fit well in the current standard
    # frame, as it would require knowledge of the final piece state to
    # implement piece_name().  Thus it implements a specific
    # csa_piece_name() instead, and cannot be declared as
    # PieceNotation to the plugin system.
    PIECE_TEXT = {
        ShogiFacetype.KING        : "OU",
        ShogiFacetype.GOLD        : "KI",
        ShogiFacetype.SILVER      : "GI",
        ShogiFacetype.KNIGHT      : "KE",
        ShogiFacetype.LANCE       : "KY",
        ShogiFacetype.BISHOP      : "KA",
        ShogiFacetype.ROOK        : "HI",
        ShogiFacetype.PAWN        : "FU",

        ShogiFacetype.NARIGIN     : "NG",
        ShogiFacetype.NARIKEI     : "NK",
        ShogiFacetype.NARIKYO     : "NY",
        ShogiFacetype.DRAGONHORSE : "UM",
        ShogiFacetype.DRAGONKING  : "RY",
        ShogiFacetype.TOKIN       : "TO",
    }
    def csa_piece_name(self, piecestate, finally_promoted):
        face = (piecestate.piece.promoted_type if finally_promoted
                else piecestate.piece.unpromoted_type)
        return self.PIECE_TEXT[face]
    def piece_type(self, name):
        for typ, typname in self.PIECE_TEXT.items():
            if typname == name:
                return typ
        else:
            raise Core.ParseError("unknown piece type %r" % (name,))


class CSANotation(Core.MoveNotation):
    LocationNotation = CSALocationNotation
    PieceNotation = CSAPieceNotation
    PLAYER_TEXT = ( '+', '-' )
    def move_serialization(self, move):
        if move.source.holder is self.game.board:
            source_name = self.location_notation.location_name(move.source)
        else:
            # move is a drop
            source_name = "00"
        return (self.PLAYER_TEXT[self.game.players.index(move.gamestate.piece_owner(move.piece))] +
                source_name + self.location_notation.location_name(move.target) +
                self.piece_notation.csa_piece_name(
                    move.gamestate.piece_state(move.piece),
                    finally_promoted=move.originally_promoted or move.promotes))
    def move_deserialization(self, gamestate, string, player):
        assert gamestate.is_frozen
        m = re.match(r"([+-])(\d\d)(\d\d)([A-Z][A-Z])$", string)
        if m:
            # identify requested piece type
            piece_type = self.piece_notation.piece_type(m.group(4))
            # source / destination
            src_coords = m.group(2)
            dst_coords = m.group(3)
            target = self.game.board.Location(self.game.board,
                                              self.location_notation.col(dst_coords[0]),
                                              self.location_notation.row(dst_coords[1]))
            if src_coords == "00":
                # drop
                source = self.game.piece_location_by_type_inplayerpool(gamestate,
                                                                       piece_type, player)
                if source is None:
                    raise Core.ParseError("No such piece to drop")
                return self.game.Move(gamestate=gamestate, player=player,
                                      source=source, target=target)

            source = self.game.board.Location(self.game.board,
                                              self.location_notation.col(src_coords[0]),
                                              self.location_notation.row(src_coords[1]))
            piece = gamestate.piece_at(source)
            piece_state = gamestate.piece_state(piece)
            if not piece:
                raise Core.ParseError("parsing %r: no piece at source %s" % (string, source))
            move_player = self.game.players[self.PLAYER_TEXT.index(m.group(1))]
            if player is not move_player:
                raise Core.ParseError("parsing %r: attempt to move on opponent turn" % (string,))
            if player is not gamestate.piece_owner(piece):
                raise Core.ParseError("parsing %r: attempt to move opponent's piece" % (string,))
            move = self.game.Move(gamestate=gamestate, player=player, source=source, target=target)
            if piece_type is piece_state.face:
                move.promotes = False
            elif piece_type is piece.promoted_type:
                move.promotes = True
            else:
                raise Core.ParseError("parsing %r: failed to apply to %s" % (string, piece))
            return move

        raise Core.ParseError("Could not parse '%s' as a serialized shogi move" %
                              string)

# FIXME "undo"
class CSAPlayerDriver(Core.AIPlayerDriver):
    """Basic CSA support with probable GPS Shogi specificities.

    The CSA protocol lacks any form of control, even the choice of
    player side has to come from command-line.  The lack other
    available CSA engines does not seem to warrant to do better for
    now.
    """

    def __init__(self, **kwargs):
        super(CSAPlayerDriver, self).__init__(**kwargs)
        self.__notation = CSANotation(self.gui.game)
        self.__process = None
        if self.player is self.gui.game.players[0]:
            self.__letter = 'S'
        else:
            self.__letter = 'G'

    def disconnect(self):
        logger.info("disconnecting CSAPlayerDriver...")
        if not self.__process:
            logger.info("... CSAPlayerDriver was not connected")
            return
        self.gui.game.signal_ready(self.player, False)
        self.gui.game.unregister_listener(self.handle_event)
        self.gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.ERROR,
                                          self.__process.stdout, self.handle_ioerror)
        self.gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                          self.__process.stdout, self.handle_data)
        self.gui.tk.unset_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                          self.__process.stderr, self.handle_stderr)
        self.__process.stdin.close()
        self.__process = None

    @Overlord.parameter
    def program(cls, context):
        return Overlord.params.String(
            label = 'Command',
            default="gpsshogi --csa",
            )

    @classmethod
    def parameters_dcl(cls, context):
        d = dict(super(CSAPlayerDriver, cls).parameters_dcl(context))
        name_decl = d['name']
        d['name'] = name_decl.clone(hidden=True)
        assert d['name'].hidden
        return d

    def connect(self):
        command = self.program
        if self.gui.game.players.index(self.player) == 0:
            command += " --sente"

        # default name, not overridable by protocol
        self.name = os.path.basename(self.program.split(' ')[0])

        logger.debug("Launching command {0!r}".format(command))
        self.__process = safe_popen(command)

        self.gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                        self.__process.stdout,
                                        self.handle_data)
        self.gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.ERROR,
                                        self.__process.stdout,
                                        self.handle_ioerror)
        self.gui.tk.set_stream_callback(Core.UI.Toolkit.CallbackType.READ,
                                        self.__process.stderr,
                                        self.handle_stderr)
        self.gui.game.register_listener(self.handle_event)

        self.gui.game.signal_ready(self.player, True)

    def write(self, data):
        logger.debug("%s>%s", self.__letter, data)
        self.__process.stdin.write(data + "\n")

    def handle_event(self, event):
        if isinstance(event, Core.Game.MoveEvent):
            if self.gui.game.scratch_state.piece_owner(event.move.piece) is not self.player:
                movestr = self.__notation.move_serialization(event.move)
                self.write(movestr)
        #elif isinstance(event, Core.Game.PhaseChangeEvent): # FIXME!
        #    if event.newphase is Core.GamePhase.Playing:
        #        # launch game
        #        if self.player is self.gui.game.scratch_state.whose_turn:
        #            self.write("go")
        #    elif event.newphase in (Core.GamePhase.Suspended,
        #                            Core.GamePhase.Over):
        #        self.write("force")

    def handle_data(self, stream, arg): # pylint: disable=unused-argument
        line = stream.readline()
        if line == "":
            logger.warn("%s EOF", self.player.name)
            self.disconnect()
        line = line.strip()
        logger.debug("%s<%s", self.__letter, line)

        # IA move ?
        match = re.match(r'([+-]\d\d\d\d[A-Z][A-Z])$', line)
        if match:
            move = self.__notation.move_deserialization(self.gui.game.last_state,
                                                        match.group(1),
                                                        self.player)
            try:
                self.gui.game.make_move(self.player, move)
            except Core.GameSuspended:
                logger.critical("move received while game is suspended, protocol does not allow to recover")
                # FIXME should emulate "undo" by restart program and replay
                raise
            except Core.InvalidMove as e:
                logger.critical("Game refused AI (%s) move %s: %s", self.name, match.group(1), e)
                raise
            else:
                # refresh display
                self.gui.tk.invalidate_canvas(self.gui.canvas)
            return

        # resign ?
        match = re.match(r'%TORYO$', line)
        if match:
            winner = self.player.next
            outcome = {winner: self.gui.game.Outcomes.Winner}
            self.gui.game.propose_outcome(self.player, outcome)
            return

        match = re.match(r'%JISHOGI$', line)
        if match:
            outcome = {self.player: self.gui.game.Outcomes.IsDraw,
                       self.player.next: self.gui.game.Outcomes.IsDraw}
            self.gui.game.propose_outcome(self.player, outcome)
            return

        logger.info("unhandled IA output: %r", line)

    def handle_stderr(self, stream, arg): # pylint: disable=unused-argument
        line = stream.readline().strip()
        logger.debug("E:%s<%s", self.__letter, line)

    def handle_ioerror(self, stream, arg): # pylint: disable=unused-argument
        logger.warning("connection lost with engine")
        self.disconnect()
