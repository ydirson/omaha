# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

import enum
import itertools
import logging
import random
import re
from typing import cast, AbstractSet, Any, ClassVar, Iterable, Mapping, Optional, TypeVar

from frozendict import frozendict

from Omaha import Core
from Omaha.Core import InvalidMove
from .abstract import Euclidian2D
from .abstract.RectBoardGame import RectBoardGame, NumberColumn0LTRNotation, NumberRowBTTNotation
from .abstract.SingleBoardGame import OnlySingleBoardGame

logger = logging.getLogger(__name__)

class Game(RectBoardGame, OnlySingleBoardGame, Core.AutomaticMovesGame):
    # FIXME make number of players a parameter
    NUM_PLAYERS: ClassVar[int]
    NUM_COLUMNS_TO_WIN = 3
    NUM_MARKERS = 3
    NUM_DICE_FACES = 6
    DICE_GROUP_SIZE = 2
    NUM_DICE_GROUPS = 2
    NCOLUMNS = DICE_GROUP_SIZE * (NUM_DICE_FACES - 1) + 1
    NROWS = (NCOLUMNS + 1) // 2
    # check that game is always winnable
    #assert (NUM_PLAYERS - 1) * (NUM_COLUMNS_TO_WIN - 1) + NUM_COLUMNS_TO_WIN <= NCOLUMNS

    #Piece: TypeAlias = Core.Piece[None]

    def column_height(self, column: int) -> int:
        return (3 + column if column < self.NROWS
                else 3 + self.NCOLUMNS - column - 1)

    class Board(RectBoardGame.Board):
        game: Game
        def post_init(self, **kwargs: Any) -> None: # type: ignore[override]
            """Set all board squares as empty, and out-of-hex locations as not available."""
            super().post_init(**kwargs)
            for col in range(self.matrix_width):
                for row in range(self.game.column_height(col), self.matrix_height):
                    location = self.Location(self, col, row)
                    self.location_desc(location).set_available(False)

    class State(Core.AutomaticMovesGame.State):
        _TGameState = TypeVar("_TGameState", bound="Game.State")
        class TurnState(enum.Enum):
            START = enum.auto() # expect "roll" or "stop"
            ROLL = enum.auto() # expect "dice"
            ROLLED = enum.auto() # expect "fall" or "cols"

        game: Game

        def __init__(self, game: Game):
            super().__init__(game)
            self.__turn_state: Game.State.TurnState
            self.__dice_values: tuple[int, ...] = ()
            self.__locked_columns: Mapping[Core.Player, AbstractSet[int]]
            self.__locked_columns = {player: set() for player in game.players}
            self.__markers: Mapping[int, Core.Piece[None]] = {}
            self.__columns: Mapping[Core.Player, Mapping[int,Core.Piece[None]]]
            self.__columns = {player: {} for player in game.players}

        @property
        def turn_state(self) -> TurnState:
            return self.__turn_state
        def set_turn_state(self, turn_state: TurnState) -> None: # pylint: disable=method-hidden
            old_state = getattr(self, "__turn_state", None)
            logger.debug("turn state: %s -> %s", old_state, turn_state)
            assert old_state != turn_state
            self.__turn_state = turn_state

        @property
        def dice_values(self) -> tuple[int, ...]:
            return self.__dice_values
        def set_dice_values(self, # pylint: disable=method-hidden
                            dice_values: tuple[int, ...]) -> None:
            self.__dice_values = tuple(dice_values)

        def locked_columns_of_player(self, player: Core.Player) -> AbstractSet[int]:
            return self.__locked_columns[player]
        def is_locked(self, column: int) -> Optional[Core.Player]:
            "Returns player locking column (== True) or None (== False)"
            for player, locked_columns in self.__locked_columns.items():
                if column in locked_columns:
                    return player
            return None
        def lock_column(self, player: Core.Player, # pylint: disable=method-hidden
                        column: int) -> None:
            player_cols = self.__locked_columns[player]
            assert isinstance(player_cols, set)
            player_cols.add(column)

        @property
        def markers(self) -> Mapping[int,Core.Piece[None]]:
            return self.__markers
        def move_markers(self, columns: Iterable[int]) -> None: # pylint: disable=method-hidden
            assert isinstance(self.__markers, dict)
            for col in columns:
                if col in self.__markers:
                    marker = self.__markers[col]
                    old_loc = self.piece_location(marker)
                    assert isinstance(old_loc, Game.Board.Location)
                    old_row = old_loc.row
                    self.take_piece(marker)
                    nextloc = self.game.Board.Location(self.game.board, col, old_row + 1)
                    logger.debug("marker on %s", nextloc)
                    self.put_piece(nextloc, marker)
                else:
                    piece = self.player_piece_in_column(self.whose_turn, col)
                    if piece:
                        piece_loc = self.piece_location(piece)
                        assert isinstance(piece_loc, Game.Board.Location)
                        row = (1 + piece_loc.row)
                    else:
                        row = 0
                    marker = self.new_piece_at(self.game.Board.Location(self.game.board, col, row),
                                               None, None)
                    self.__markers[col] = marker
        def remove_markers(self) -> None: # pylint: disable=method-hidden
            for marker in self.__markers.values():
                self.take_piece(marker)
                self.forget_piece(marker)
            self.__markers = {}

        def is_column_choice_valid(self, column: int) -> bool:
            if self.is_locked(column):
                return False
            if column in self.markers:
                loc = self.piece_location(self.markers[column])
                assert isinstance(loc, self.game.Board.Location)
                if self.game.is_top_of_column(loc):
                    return False
                return True
            if len(self.markers) >= self.game.NUM_MARKERS:
                return False
            return True

        def valid_choices_in_combo(self, dice_combo: tuple[tuple[int,...],...]
                                   ) -> tuple[tuple[tuple[int,...],...], ...]:
            groups_valid = {}
            for dice_group in dice_combo:
                groups_valid[dice_group] = self.is_column_choice_valid(sum(dice_group))
            fullcombo_is_valid = all(groups_valid.values())

            # FIXME ad-hoc detection of forced combo splitting, for 2x2 dice
            columns = [sum(group) for group in dice_combo]
            assert len(columns) == 2
            # ... when user has to choose between 2 values
            if fullcombo_is_valid and len(self.markers) == 2:
                one_in_markers = False
                for column in columns:
                    if column in self.markers:
                        one_in_markers = True
                        break
                if columns[0] != columns[1] and not one_in_markers:
                    fullcombo_is_valid = False
            # ... when one of 2 identical values brings marker to top
            if fullcombo_is_valid and columns[0] == columns[1]:
                if columns[0] in self.markers:
                    # ... from marker
                    loc = self.piece_location(self.markers[columns[0]])
                    assert isinstance(loc, Game.Board.Location)
                    if self.game.is_top_of_column(loc, minus=1):
                        fullcombo_is_valid = False
                else:
                    # ... from token
                    piece = self.player_piece_in_column(self.whose_turn, columns[0])
                    if piece:
                        loc = self.piece_location(piece)
                        assert isinstance(loc, Game.Board.Location)
                        if self.game.is_top_of_column(loc, minus = 1):
                            fullcombo_is_valid = False

            if fullcombo_is_valid:
                return (dice_combo,)

            assert self.game.NUM_DICE_GROUPS == 2 # following line is a simplification
            return tuple((group,) for group, valid in groups_valid.items() if valid)

        @property
        def columns(self) -> Mapping[Core.Player, Mapping[int,Core.Piece[None]]]:
            return self.__columns
        def player_column_positions(self, gamestate: Game.State,
                                    player: Core.Player) -> tuple[Game.Board.Location,...]:
            return tuple(cast(Game.Board.Location, gamestate.piece_location(piece))
                         for column, piece in self.__columns[player].items())
        def player_piece_in_column(self, player: Core.Player,
                                   col: int) -> Optional[Core.Piece[None]]:
            return self.__columns[player].get(col, None)
        def new_player_piece_in_column(self, player: Core.Player, # pylint: disable=method-hidden
                                       location: Game.Board.Location) -> Core.Piece[None]:
            player_cols = self.__columns[player]
            assert isinstance(player_cols, dict)
            assert location.col not in player_cols
            piece = self.new_piece_at(location, player, None)
            player_cols[location.col] = piece
            return piece

        def freeze(self) -> None:
            super().freeze()
            self.__markers = frozendict(self.__markers)
            self.__locked_columns = frozendict(
                {player: frozenset(columns) for player, columns in self.__locked_columns.items()})
            self.__columns = frozendict({player: frozendict(columns)
                                         for player, columns in self.__columns.items()})
            self.set_turn_state = NotImplemented # type: ignore[assignment]
            self.set_dice_values = NotImplemented # type: ignore[assignment]
            self.lock_column = NotImplemented # type: ignore[assignment]
            self.move_markers = NotImplemented # type: ignore[assignment]
            self.remove_markers = NotImplemented # type: ignore[assignment]
            self.new_player_piece_in_column = NotImplemented # type: ignore[assignment]
        def clone(self: _TGameState) -> _TGameState:
            c = super().clone()
            c.__turn_state = self.__turn_state
            c.__dice_values = self.__dice_values
            c.__locked_columns = self.__locked_columns
            c.__markers = self.__markers
            c.__columns = self.__columns
            return c

    class Move(Core.Move):
        def __init__(self, is_roll:bool=False, is_stop:bool=False, is_fall:bool=False,
                     dice:Optional[tuple[int,...]]=None,
                     columns:Optional[tuple[tuple[int,...],...]]=None, **kwargs: Any):
            assert ((is_roll) ^ (is_stop) ^ (is_fall) ^
                    (dice is not None) ^ (columns is not None))
            super().__init__(**kwargs)
            self.__is_roll = is_roll
            self.__is_stop = is_stop
            self.__is_fall = is_fall
            self.__dice = dice
            self.__columns = columns
        @property
        def is_roll(self) -> bool:
            return self.__is_roll
        @property
        def is_stop(self) -> bool:
            return self.__is_stop
        @property
        def is_fall(self) -> bool:
            return self.__is_fall
        @property
        def dice(self) -> Optional[tuple[int,...]]:
            return self.__dice
        @property
        def columns(self) -> Optional[tuple[tuple[int,...],...]]:
            return self.__columns
        def __str__(self) -> str:
            return (f"CantStop.Move(is_roll={self.is_roll}, is_stop={self.is_stop}, "
                    f"is_fall={self.is_fall}, dice={self.dice}, columns={self.columns})")

    class DefaultMoveNotation(Core.MoveNotation["Game.Move"]):
        game: Game
        class LocationNotation(NumberColumn0LTRNotation, # type: ignore[misc] # FIXME mypy#14279
                               NumberRowBTTNotation,
                               Euclidian2D.CoordinateNotation):
            FORMAT = "{column}:{row}"
            LOCATION_REGEXP = r"(?P<column>\d+):(?P<row>\d+)"

        def move_serialization(self, move: Game.Move) -> str:
            if move.is_roll:
                return "roll"
            if move.is_stop:
                return "stop"
            if move.is_fall:
                return "fall"
            if move.dice:
                return "dice=%s" % ",".join(str(die) for die in move.dice)
            if move.columns:
                return "cols=%s" % ",".join("+".join(str(die) for die in dice)
                                            for dice in move.columns)
            raise NotImplementedError(f"unsupported move for serialization: {move}")
        def move_deserialization(self, gamestate, string: str,
                                 player: Core.Player) -> Game.Move:
            if string == "roll":
                return self.game.Move(gamestate=gamestate, player=player, is_roll=True)
            if string == "stop":
                return self.game.Move(gamestate=gamestate, player=player, is_stop=True)
            if string == "fall":
                return self.game.Move(gamestate=gamestate, player=player, is_fall=True)
            if string.startswith("dice="):
                try:
                    dice_str = string[5:]
                    dice = tuple(int(die) for die in dice_str.split(","))
                except ValueError as ex: # from int()
                    raise Core.ParseError(f"Could not parse {dice_str!r} as dice values") from ex
                return self.game.Move(gamestate=gamestate, player=player, dice=dice)
            if string.startswith("cols="):
                try:
                    columns_str = string[5:]
                    dice_strings = columns_str.split(",")
                    columns = tuple(tuple(int(die) for die in dice_str.split("+"))
                                    for dice_str in dice_strings)
                except ValueError as ex: # from int()
                    raise Core.ParseError(
                        f"Could not parse {columns_str!r} as columns values") from ex
                return self.game.Move(gamestate=gamestate, player=player, columns=columns)

            raise Core.ParseError(f"Could not parse {string!r} as a move")

    ### Game methods

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.boardwidth = self.NCOLUMNS
        self.boardheight = 2 + self.NROWS
        self.scratch_state: Game.State

    def set_start_position(self) -> None:
        self.scratch_state.set_turn_state(self.State.TurnState.START)
        self.scratch_state.add_pending_move(is_roll=True)

    def supplement_and_check_move(self, gamestate: Game.State, # type: ignore[override]
                                  player: Optional[Core.Player],
                                  move: Game.Move, anticipate: bool) -> None:
        super().supplement_and_check_move(gamestate, player, move, anticipate)
        if move.is_roll:
            if gamestate.turn_state is not gamestate.TurnState.START:
                raise InvalidMove("can only roll at start of turn")
            return
        if move.is_stop:
            if gamestate.turn_state is not gamestate.TurnState.START:
                raise InvalidMove("can only stop at start of turn")
            return
        if move.dice:
            if gamestate.turn_state is not gamestate.TurnState.ROLL:
                raise InvalidMove("cannot roll dice now")
            if len(move.dice) != self.NUM_DICE_GROUPS * self.DICE_GROUP_SIZE:
                raise InvalidMove(f"need {self.NUM_DICE_GROUPS * self.DICE_GROUP_SIZE} dice, "
                                  f"not {len(move.dice)}")
            for die in move.dice:
                if die not in range(self.NUM_DICE_FACES):
                    raise InvalidMove(f"bad die value {die}, "
                                      f"should be in [0-{self.NUM_DICE_FACES - 1}]")
            return
        if move.columns:
            if gamestate.turn_state is not gamestate.TurnState.ROLLED:
                raise InvalidMove("cannot move tokens now")
            for group in move.columns:
                column_num = sum(group)
                if gamestate.is_locked(column_num):
                    raise InvalidMove(f"column {column_num} is already locked")
            # FIXME check sums, check column still available, check tokens available, token not on top
            return
        if move.is_fall:
            if gamestate.turn_state is not gamestate.TurnState.ROLLED:
                raise InvalidMove("cannot fall now")
            # FIXME check nothing was possible (useless?)
            return

    def do_move(self, gamestate: State, # type: ignore[override]
                player: Optional[Core.Player], move: Move) -> None:
        if move.is_roll:
            gamestate.set_turn_state(gamestate.TurnState.ROLL)
            gamestate.add_pending_move(
                dice=tuple(sorted(random.randrange(self.NUM_DICE_FACES)
                                  for _ in range(self.DICE_GROUP_SIZE * self.NUM_DICE_GROUPS))))
            return
        if move.is_stop:
            assert player
            for marker in gamestate.markers.values(): # advance piece for each marker
                location = gamestate.piece_location(marker)
                assert isinstance(location, self.Board.Location)
                piece = gamestate.player_piece_in_column(player, location.col)
                if piece:
                    gamestate.take_piece(piece)
                    gamestate.put_piece(location, piece)
                else:
                    gamestate.new_player_piece_in_column(player, location)
            for column, marker in gamestate.markers.items():
                loc = gamestate.piece_location(marker)
                assert isinstance(loc, self.Board.Location)
                if self.is_top_of_column(loc):
                    logger.debug("%s locks column %s", player, column)
                    gamestate.lock_column(player, column)
            gamestate.remove_markers()
            if len(gamestate.locked_columns_of_player(player)) >= self.NUM_COLUMNS_TO_WIN:
                self.outcome = {player: self.Outcomes.Winner}
                self.change_phase(Core.GamePhase.Over)
                return
            gamestate.next_player() # already in TurnState.START
            gamestate.add_pending_move(is_roll=True)
            return
        if move.dice:
            gamestate.set_dice_values(move.dice)
            gamestate.set_turn_state(gamestate.TurnState.ROLLED)
            # check for fall
            possible_groups_it = itertools.permutations(move.dice, self.DICE_GROUP_SIZE)
            possible_sums = set(sum(group) for group in possible_groups_it)
            valid_cols = [col for col in possible_sums if gamestate.is_column_choice_valid(col)]
            if not valid_cols:
                gamestate.add_pending_move(is_fall=True)
            return
        if move.is_fall:
            gamestate.set_dice_values(())
            gamestate.remove_markers()
            gamestate.next_player()
            gamestate.set_turn_state(gamestate.TurnState.START)
            gamestate.add_pending_move(is_roll=True)
            return
        if move.columns:
            gamestate.set_dice_values(())
            gamestate.move_markers(sum(group) for group in move.columns)
            gamestate.set_turn_state(gamestate.TurnState.START)
            return
        raise InvalidMove(f"does not understand move {move}")

    def is_top_of_column(self, location: Game.Board.Location, *, minus: int=0) -> bool:
        "Tell if location is (within `minus` positions of) the top of its column (or higher)"
        return location.row >= self.column_height(location.col) - 1 - minus

    def history_formatmove(self, # type: ignore[override]
                           movelist: Core.MoveList, movenum: int, move: Game.Move,
                           notation: Core.MoveNotation[Game.Move]) -> str:
        movestr = f"{1 + movenum}. "
        if move.player:
            movestr += "[%s] " % (1 + self.players.index(move.player))
        movestr += notation.move_serialization(move)
        return movestr

    # FIXME likely temporarily
    has_tooltip = True
    def location_tooltip_text(self, gamestate: State, # type: ignore[override]
                              loc: Game.Board.Location) -> Optional[str]:
        pieces = gamestate.contents_at(loc).pieces
        if not pieces:
            return None
        owners = (gamestate.piece_owner(piece) for piece in pieces)
        return "\n".join(player.name if player else "token"
                         for player in owners)

class PositionNotation(Core.PositionNotation):
    """A position notation for Can't Stop.

    Syntax is line-oriented, decomposing as "<key>: <values>".
    * numeric keys are player numbers, their values are locations of the player's pieces
    * key "P" is "active player", single value is active player number
    * key "M" is "markers", values are locations of marker tokens
    * key "D" is "dice", values are dice to be chosen.  When this line exists, turn state is
      ROLLED, else it is START.  State ROLL cannot be represented.
    """
    LocationNotation = Game.DefaultMoveNotation.LocationNotation
    game: Game
    def position_serialization(self, gamestate: Game.State) -> str: # type: ignore[override]
        outputs = [] # [f"N={len(gamestate.game.players)}"] # needed ?
        for i, player in enumerate(gamestate.game.players):
            player_positions = (self.location_notation.location_name(loc)
                                for loc in gamestate.player_column_positions(gamestate, player))
            outputs.append(f"{i}: {' '.join(player_positions)}")
        outputs.append(f"P: {gamestate.game.players.index(gamestate.whose_turn)}")
        if len(gamestate.markers):
            marker_positions = (
                self.location_notation.location_name(gamestate.piece_location(marker))
                for marker in gamestate.markers.values())
            outputs.append(f"M: {' '.join(marker_positions)}")
        assert gamestate.turn_state != gamestate.TurnState.ROLL
        if gamestate.turn_state == gamestate.TurnState.ROLLED:
            outputs.append(f"D: {' '.join(str(d) for d in gamestate.dice_values)}")

        return "\n".join(outputs)
    def position_deserialization(self, string: str) -> Game.State:
        lines = string.split("\n")
        gamestate = Game.State(self.game)
        for line in lines:
            if line.startswith("P: "):
                player_num = int(line[3:])
                if not 0 <= player_num < self.game.NUM_PLAYERS:
                    raise Core.ParseError(f"invalid player number {player_num}"
                                          f" (valid 0...{self.game.NUM_PLAYERS - 1})")
                gamestate.set_whose_turn(self.game.players[player_num])
            elif line.startswith("M: "):
                marker_locs = (self.location_notation.location(pos) for pos in line[3:].split())
                for loc in marker_locs:
                    marker = gamestate.new_piece_at(loc, None, None)
                    assert isinstance(gamestate.markers, dict)
                    gamestate.markers[loc.col] = marker
            elif line.startswith("D: "):
                dice = tuple(int(value) for value in line[3:].split())
                if len(dice) != self.game.DICE_GROUP_SIZE * self.game.NUM_DICE_GROUPS:
                    raise Core.ParseError(
                        f"invalid number of dice {len(dice)}, expected"
                        f" {self.game.DICE_GROUP_SIZE * self.game.NUM_DICE_GROUPS})")
                gamestate.set_dice_values(dice)
            else:
                match = re.match(r"(\d+): (.*)", line)
                if not match:
                    raise Core.ParseError(f"invalid Can't Stop position line: {line!r}")
                player_numstr, pieces_str = match.groups()
                player = self.game.players[int(player_numstr)]
                piece_locs = (self.location_notation.location(pos) for pos in pieces_str.split())
                for loc in piece_locs:
                    piece = gamestate.new_piece_at(loc, player, None)
                    player_cols = gamestate.columns[player]
                    assert isinstance(player_cols, dict)
                    assert loc.col not in player_cols
                    player_cols[loc.col] = piece
                    if self.game.is_top_of_column(loc):
                        gamestate.lock_column(player, loc.col)

        gamestate.set_turn_state(gamestate.TurnState.ROLLED if gamestate.dice_values
                                 else gamestate.TurnState.START)

        gamestate.set_phase(Core.GamePhase.Playing) # FIXME could be Over
        return gamestate

# alias for plugin system ("import" restriction)
MoveNotation = Game.DefaultMoveNotation

class Game2Players(Game):
    NUM_PLAYERS = 2
    player_names = tuple( f"Player {i}" for i in range(1, NUM_PLAYERS + 1) )

class Game3Players(Game):
    NUM_PLAYERS = 3
    player_names = tuple( f"Player {i}" for i in range(1, NUM_PLAYERS + 1) )

class Game4Players(Game):
    NUM_PLAYERS = 4
    player_names = tuple( f"Player {i}" for i in range(1, NUM_PLAYERS + 1) )
