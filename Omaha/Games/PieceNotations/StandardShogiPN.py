# -*- coding: utf-8 -*-

# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from . import ShogiGamePN
from ..piecetypes.shogi import ShogiFacetype
from Omaha import Core

class EnglishShogiPieceNotation(ShogiGamePN.WesternShogiPieceNotation):
    PIECE_TEXT = {
        ShogiFacetype.KING   : "K",
        ShogiFacetype.GOLD   : "G",
        ShogiFacetype.SILVER : "S",
        ShogiFacetype.KNIGHT : "N",
        ShogiFacetype.LANCE  : "L",
        ShogiFacetype.BISHOP : "B",
        ShogiFacetype.ROOK   : "R",
        ShogiFacetype.PAWN   : "P",
    }

class OneKanjiShogiNotation(ShogiGamePN.OneKanjiShogiPieceNotation):
    PIECE_TEXT = {
        ShogiFacetype.KING   : "王",
#        ShogiFacetype.JADE   : "玉",
        ShogiFacetype.GOLD   : "金",
        ShogiFacetype.SILVER : "銀",
        ShogiFacetype.KNIGHT : "桂",
        ShogiFacetype.LANCE  : "香",
        ShogiFacetype.BISHOP : "角",
        ShogiFacetype.ROOK   : "飛",
        ShogiFacetype.PAWN   : "歩",

        ShogiFacetype.TOKIN       : "と",
        ShogiFacetype.NARIGIN     : "全",
        ShogiFacetype.NARIKEI     : "圭",
        ShogiFacetype.NARIKYO     : "杏",
        ShogiFacetype.DRAGONKING  : "龍",
        ShogiFacetype.DRAGONHORSE : "馬",
    }

class TwoKanjiShogiNotation(ShogiGamePN.TwoKanjiShogiPieceNotation):
    PIECE_TEXT = {
        ShogiFacetype.KING   : "王将",
#        ShogiFacetype.JADE   : "玉将",
        ShogiFacetype.GOLD   : "金将",
        ShogiFacetype.SILVER : "銀将",
        ShogiFacetype.KNIGHT : "桂馬",
        ShogiFacetype.LANCE  : "香車",
        ShogiFacetype.BISHOP : "角行",
        ShogiFacetype.ROOK   : "飛車",
        ShogiFacetype.PAWN   : "歩兵",

        ShogiFacetype.TOKIN       : "と金",
        ShogiFacetype.NARIGIN     : "成銀",
        ShogiFacetype.NARIKEI     : "成桂",
        ShogiFacetype.NARIKYO     : "成香",
        ShogiFacetype.DRAGONKING  : "龍王",
        ShogiFacetype.DRAGONHORSE : "龍馬",
    }
