# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from enum import Enum

class WesternShogiPieceNotation(Core.PieceNotation):
    "Piece notation for Shogi using the latin alphabet"
    PIECE_TEXT: dict[Enum,str]

    def piece_name(self, piecestate):
        basetext = self.PIECE_TEXT[piecestate.piece.unpromoted_type]
        if piecestate.promoted:
            return "+" + basetext
        else:
            return basetext
    def piece_type(self, name):
        for typ, typname in self.PIECE_TEXT.items():
            if typname == name:
                return typ
        else:
            raise LookupError("Invalid piece type '%s'" % name)

class OneKanjiShogiPieceNotation(Core.PieceNotation):
    "Piece notation for Shogi using a single kanji character"
    PIECE_TEXT: dict[Enum,str]

    def piece_name(self, piecestate):
        return self.PIECE_TEXT[piecestate.face]
    def piece_type(self, name):
        for typ, typname in self.PIECE_TEXT.items():
            if typname == name:
                return typ
        else:
            raise LookupError("Invalid piece type '%s'" % name)

class TwoKanjiShogiPieceNotation(Core.PieceNotation):
    PIECE_TEXT: dict[Enum,str]

    def piece_name(self, piecestate):
        # FIXME: vertical layout forced through inserted newlines,
        # will work badly for more serialization
        return '\n'.join(self.PIECE_TEXT[piecestate.face])
