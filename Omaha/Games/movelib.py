# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""A library of common displace moves from various games."""

from .abstract.GenericChesslike import MOVERANGE_INF
from .abstract.SingleBoardGame import SingleBoardGame

import itertools
import re

import logging
logger = logging.getLogger(__name__)

# Library of predicates for conditional moves
def default_move(gamestate, move, direction, nunits):
    return True
def replace_capture(gamestate, move, direction, nunits):
    return gamestate.piece_at(move.target) is not None
def unoccupied_target(gamestate, move, direction, nunits):
    return gamestate.piece_at(move.target) is None
def never_moved(gamestate, move, direction, nunits):
    return not gamestate.piece_state(move.piece).has_moved

def range_free(gamestate, move, direction, nunits):
    "Move predicate for pieces moving over a free range in straight line."
    game = move.source.holder.game
    player = gamestate.piece_owner(move.piece)
    assert player in game.players, "%s not in %s" % (move.piece.player,
                                                     [str(x) for x in game.players])
    assert isinstance(game, SingleBoardGame)
    direction_delta = move.source.Delta(*direction) # still player-relative
    direction_delta_board = game.player_to_board_delta(direction_delta, player)
    if nunits is None:
        nunits = move.source.holder.nunits_in_direction(move, direction_delta_board)
    location = move.source
    for increment in range(1, nunits):
        location += direction_delta_board
        if gamestate.piece_at(location):
            return False
    else:
        return True

def And(*predicates):
    def pred_conjunction(gamestate, move, direction, nunits):
        for pred in predicates:
            if not pred(gamestate, move, direction, nunits):
                return False
        else:
            return True
    return pred_conjunction


# (X)Betza notation support
# see:
# - https://www.chessvariants.com/page/MSbetza-notation-extended
# - https://www.gnu.org/software/xboard/Betza.html

# Current Betza coverage:
# - all basic moves and direction modifiers
# - modes "m" and "c"
# - broken: too strict on modifiers ordering (wants m/c last)
# - missing: multiplier applied to ranged aliases (eg. B2)
# - missing: infinite repetition through doubling of move letter (eg. WW)
# Current XBetza coverage:
# - inifinite repetition through "0" multiplier
# - "K" as an atom with specific modifiers
# - "h*" chiral modifiers

class BetzaMoveDecl(list): # FIXME why set would not do?
    def __init__(self, betzastring, moves):
        super().__init__(moves)
        self.betza = betzastring

def betza(descr):
    return BetzaMoveDecl(descr, regroup_by_predicate(betza_iter(descr)))

def regroup_by_predicate(movetypes):
    """Simplify a moves-type definition.

    Using a single occurence of each predicate and sets allows to
    compare move-type definitions with better accuracy, and
    optimize their use somewhat.
    """
    # regroup as mutable sets
    grouped = {}
    for pred, moves, special in movetypes:
        assert special is None, f"{special} is not None" # non-None just fully untested
        if (pred, special) not in grouped:
            grouped[pred,special] = set()
        merge_into_movetypes(grouped[pred,special], moves)
    # freeze sets so they can be inserted in a set themselves
    return frozenset((pred, frozenset(moves), special)
                     for (pred, special), moves in grouped.items())

def merge_into_movetypes(orig, new):
    """Adds movetypes from `new` into `orig` set, unless they add nothing.

    Also remove any movetype in `orig` getting superceded.  Assumes 2
    `orig` items cannot be superceded by a single new movetype (FIXME?).
    """
    for movetype_direction, movetype_range in new:
        for existing_direction, existing_range in orig:
            if movetype_direction != existing_direction:
                # no relation between them
                continue
            if existing_range != MOVERANGE_INF and (movetype_range == MOVERANGE_INF or
                                                    movetype_range > existing_range):
                # movetype includes existing, replace
                orig.remove((existing_direction, existing_range))
                orig.add((movetype_direction, movetype_range))
                break
            # already included in existing, just drop
            break
        else: # no such direction yet
            orig.add((movetype_direction, movetype_range))

# regexp for one possible move
_BEZTA_EXPR = re.compile(r'([fbrlvsh]*)([cm]?)([@A-Z])([0-9]*)')

def betza_iter(descr):
    pos = 0
    while True:
        m = _BEZTA_EXPR.match(descr, pos)
        if not m:
            raise RuntimeError("betza parsing failed separation for %r" % descr[pos:])
        dirmods, modality, atom, rge = m.groups()
        predicate = {'': range_free,
                     'm': unoccupied_target,
                     'c': replace_capture}[modality]
        if atom in _BETZA_ALIASES:
            assert rge == "", "betza parsing refuses range (%s) on alias %r" % (rge, atom)
            yield (predicate,
                   [move for move in itertools.chain(
                       *[_betza_moves(dirmods, alias_atom, alias_range)
                         for alias_atom, alias_range in _BETZA_ALIASES[atom]])
                   ], None)
        elif atom == '@':
            # See "Drops" in https://www.gnu.org/software/xboard/Betza.html
            # We can just ignore those - although we could check they are
            # in line with the game rules.
            pass
        else:
            yield (predicate,
                   [move for move in _betza_moves(dirmods, atom, rge)], None)
        if m.end() == len(descr):
            return
        pos = m.end()

def _betza_moves(dirmods, atom, _range):
    rge = (1 if _range == "" else
           MOVERANGE_INF if _range == "0" else
           int(_range))
    if atom in _BETZA_ORTHO:
        if dirmods:
            for dirmod in dirmods:
                if dirmod == 'v':
                    yield (_BETZA_ORTHO[atom]['f'], rge)
                    yield (_BETZA_ORTHO[atom]['b'], rge)
                elif dirmod == 's':
                    yield (_BETZA_ORTHO[atom]['l'], rge)
                    yield (_BETZA_ORTHO[atom]['r'], rge)
                else:
                    yield (_BETZA_ORTHO[atom][dirmod], rge)
        else:
            for move in _BETZA_ORTHO[atom].values():
                yield (move, rge)

    elif atom in _BETZA_DIAG:
        if dirmods:
            while dirmods:
                # f*
                if dirmods.startswith("fl"):
                    yield (_BETZA_DIAG[atom]["lf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("fr"):
                    yield (_BETZA_DIAG[atom]["rf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("f"):
                    yield (_BETZA_DIAG[atom]["lf"], rge)
                    yield (_BETZA_DIAG[atom]["rf"], rge)
                    dirmods = dirmods[1:]
                    continue
                # b*
                if dirmods.startswith("bl"):
                    yield (_BETZA_DIAG[atom]["lb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("br"):
                    yield (_BETZA_DIAG[atom]["rb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("b"):
                    yield (_BETZA_DIAG[atom]["lb"], rge)
                    yield (_BETZA_DIAG[atom]["rb"], rge)
                    dirmods = dirmods[1:]
                    continue
                # l*
                if dirmods.startswith("lf"):
                    yield (_BETZA_DIAG[atom]["lf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("lb"):
                    yield (_BETZA_DIAG[atom]["lb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("l"):
                    yield (_BETZA_DIAG[atom]["lf"], rge)
                    yield (_BETZA_DIAG[atom]["lb"], rge)
                    dirmods = dirmods[1:]
                    continue
                # r*
                if dirmods.startswith("rf"):
                    yield (_BETZA_DIAG[atom]["rf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("rb"):
                    yield (_BETZA_DIAG[atom]["rb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("r"):
                    yield (_BETZA_DIAG[atom]["rf"], rge)
                    yield (_BETZA_DIAG[atom]["rb"], rge)
                    dirmods = dirmods[1:]
                    continue
                #
                assert False, "betza parsing does not understand diagonal direction modifiers %r" % dirmods
        else:
            for move in _BETZA_DIAG[atom].values():
                yield (move, rge)

    elif atom in _BETZA_8FOLD:
        if dirmods:
            while dirmods:
                # f*
                if dirmods.startswith("fl"):
                    yield (_BETZA_8FOLD[atom]["fl"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("fr"):
                    yield (_BETZA_8FOLD[atom]["fr"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("fs"):
                    yield (_BETZA_8FOLD[atom]["fl"], rge)
                    yield (_BETZA_8FOLD[atom]["fr"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("ff"):
                    yield (_BETZA_8FOLD[atom]["lf"], rge)
                    yield (_BETZA_8FOLD[atom]["rf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("fh"):
                    yield (_BETZA_8FOLD[atom]["fl"], rge)
                    yield (_BETZA_8FOLD[atom]["fr"], rge)
                    yield (_BETZA_8FOLD[atom]["lf"], rge)
                    yield (_BETZA_8FOLD[atom]["rf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("f"):
                    yield (_BETZA_8FOLD[atom]["lf"], rge)
                    yield (_BETZA_8FOLD[atom]["rf"], rge)
                    dirmods = dirmods[1:]
                    continue
                # b*
                if dirmods.startswith("bl"):
                    yield (_BETZA_8FOLD[atom]["bl"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("br"):
                    yield (_BETZA_8FOLD[atom]["br"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("bb"):
                    yield (_BETZA_8FOLD[atom]["lb"], rge)
                    yield (_BETZA_8FOLD[atom]["rb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("bs"):
                    yield (_BETZA_8FOLD[atom]["bl"], rge)
                    yield (_BETZA_8FOLD[atom]["br"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("bh"):
                    yield (_BETZA_8FOLD[atom]["lb"], rge)
                    yield (_BETZA_8FOLD[atom]["rb"], rge)
                    yield (_BETZA_8FOLD[atom]["bl"], rge)
                    yield (_BETZA_8FOLD[atom]["br"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("b"):
                    yield (_BETZA_8FOLD[atom]["lb"], rge)
                    yield (_BETZA_8FOLD[atom]["rb"], rge)
                    dirmods = dirmods[1:]
                    continue
                # l*
                if dirmods.startswith("lf"):
                    yield (_BETZA_8FOLD[atom]["lf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("lb"):
                    yield (_BETZA_8FOLD[atom]["lb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("lv"):
                    yield (_BETZA_8FOLD[atom]["lf"], rge)
                    yield (_BETZA_8FOLD[atom]["lb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("ll"):
                    yield (_BETZA_8FOLD[atom]["fl"], rge)
                    yield (_BETZA_8FOLD[atom]["bl"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("lh"):
                    yield (_BETZA_8FOLD[atom]["lf"], rge)
                    yield (_BETZA_8FOLD[atom]["lb"], rge)
                    yield (_BETZA_8FOLD[atom]["fl"], rge)
                    yield (_BETZA_8FOLD[atom]["bl"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("l"):
                    yield (_BETZA_8FOLD[atom]["fl"], rge)
                    yield (_BETZA_8FOLD[atom]["bl"], rge)
                    dirmods = dirmods[1:]
                    continue
                # r*
                if dirmods.startswith("rf"):
                    yield (_BETZA_8FOLD[atom]["rf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("rb"):
                    yield (_BETZA_8FOLD[atom]["rb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("rv"):
                    yield (_BETZA_8FOLD[atom]["rf"], rge)
                    yield (_BETZA_8FOLD[atom]["rb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("rr"):
                    yield (_BETZA_8FOLD[atom]["fr"], rge)
                    yield (_BETZA_8FOLD[atom]["br"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("rh"):
                    yield (_BETZA_8FOLD[atom]["rf"], rge)
                    yield (_BETZA_8FOLD[atom]["rb"], rge)
                    yield (_BETZA_8FOLD[atom]["fr"], rge)
                    yield (_BETZA_8FOLD[atom]["br"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("r"):
                    yield (_BETZA_8FOLD[atom]["fr"], rge)
                    yield (_BETZA_8FOLD[atom]["br"], rge)
                    dirmods = dirmods[1:]
                    continue
                # s*
                if dirmods.startswith("sf"):
                    yield (_BETZA_8FOLD[atom]["fl"], rge)
                    yield (_BETZA_8FOLD[atom]["fr"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("sb"):
                    yield (_BETZA_8FOLD[atom]["bl"], rge)
                    yield (_BETZA_8FOLD[atom]["br"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("s"):
                    yield (_BETZA_8FOLD[atom]["fl"], rge)
                    yield (_BETZA_8FOLD[atom]["fr"], rge)
                    yield (_BETZA_8FOLD[atom]["bl"], rge)
                    yield (_BETZA_8FOLD[atom]["br"], rge)
                    dirmods = dirmods[1:]
                    continue
                # v*
                if dirmods.startswith("vl"):
                    yield (_BETZA_8FOLD[atom]["lf"], rge)
                    yield (_BETZA_8FOLD[atom]["lb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("vr"):
                    yield (_BETZA_8FOLD[atom]["rf"], rge)
                    yield (_BETZA_8FOLD[atom]["rb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("v"):
                    yield (_BETZA_8FOLD[atom]["lf"], rge)
                    yield (_BETZA_8FOLD[atom]["lb"], rge)
                    yield (_BETZA_8FOLD[atom]["rf"], rge)
                    yield (_BETZA_8FOLD[atom]["rb"], rge)
                    dirmods = dirmods[1:]
                    continue
                # h* (chiral)
                if dirmods.startswith("hl"):
                    yield (_BETZA_8FOLD[atom]["lf"], rge)
                    yield (_BETZA_8FOLD[atom]["bl"], rge)
                    yield (_BETZA_8FOLD[atom]["fr"], rge)
                    yield (_BETZA_8FOLD[atom]["rb"], rge)
                    dirmods = dirmods[1:]
                    continue
                if dirmods.startswith("hr"):
                    yield (_BETZA_8FOLD[atom]["fl"], rge)
                    yield (_BETZA_8FOLD[atom]["lb"], rge)
                    yield (_BETZA_8FOLD[atom]["rf"], rge)
                    yield (_BETZA_8FOLD[atom]["br"], rge)
                    dirmods = dirmods[1:]
                    continue
                #
                assert False, "betza parsing does not understand 8-fold direction modifiers %r" % dirmods
        else:
            for move in _BETZA_8FOLD[atom].values():
                yield (move, rge)

    elif atom == 'K':
        if dirmods:
            while dirmods:
                # f*
                if dirmods.startswith("fl"):
                    yield (_BETZA_KING["lf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("fr"):
                    yield (_BETZA_KING["rf"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("ff"):
                    yield (_BETZA_KING["f"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("f"):
                    yield (_BETZA_KING["f"], rge)
                    dirmods = dirmods[1:]
                    continue
                # b*
                if dirmods.startswith("bl"):
                    yield (_BETZA_KING["lb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("br"):
                    yield (_BETZA_KING["rb"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("bb"):
                    yield (_BETZA_KING["b"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("b"):
                    yield (_BETZA_KING["b"], rge)
                    dirmods = dirmods[1:]
                    continue
                # l*
                if dirmods.startswith("lf"):
                    yield (_BETZA_KING["fl"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("lb"):
                    yield (_BETZA_KING["bl"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("ll"):
                    yield (_BETZA_KING["l"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("l"):
                    yield (_BETZA_KING["l"], rge)
                    dirmods = dirmods[1:]
                    continue
                # r*
                if dirmods.startswith("rf"):
                    yield (_BETZA_KING["fr"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("rb"):
                    yield (_BETZA_KING["br"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("rr"):
                    yield (_BETZA_KING["r"], rge)
                    dirmods = dirmods[2:]
                    continue
                if dirmods.startswith("r"):
                    yield (_BETZA_KING["r"], rge)
                    dirmods = dirmods[1:]
                    continue
                #
                assert False, "betza parsing does not understand 8-fold direction modifiers %r" % dirmods
        else:
            for move in _BETZA_KING.values():
                yield (move, rge)

    else:
        raise RuntimeError("betza parsing does not support atom %r" % atom)

_BETZA_ORTHO = dict(
    W = dict(f=(0, 1), b=(0, -1), l=(-1, 0), r=(1, 0)),
    D = dict(f=(0, 2), b=(0, -2), l=(-2, 0), r=(2, 0)),
    H = dict(f=(0, 3), b=(0, -3), l=(-3, 0), r=(3, 0)),
)

_BETZA_DIAG = dict(
    F = dict(lf=(-1, 1), rf=(1, 1), lb=(-1, -1), rb=(1, -1),
             #l="lf lb", r="rf rb", f="lf rf", b="lb rb",
             #fl="lf", ...
    ),
    A = dict(lf=(-2, 2), rf=(2, 2), lb=(-2, -2), rb=(2, -2)),
    G = dict(lf=(-3, 3), rf=(3, 3), lb=(-3, -3), rb=(3, -3)),
)

_BETZA_8FOLD = dict(
    N = dict(lf=(-1, 2), rf=(1, 2),
             fl=(-2, 1), fr=(2, 1),
             bl=(-2, -1), br=(2, -1),
             lb=(-1, -2), rb=(1, -2)),
    C = dict(lf=(-1, 3), rf=(1, 3),
             fl=(-3, 1), fr=(3, 1),
             bl=(-3, -1), br=(3, -1),
             lb=(-1, -3), rb=(1, -3)),
    Z = dict(lf=(-2, 3), rf=(2, 3),
             fl=(-3, 2), fr=(3, 2),
             bl=(-3, -2), br=(3, -2),
             lb=(-2, -3), rb=(2, -3)),
)
# old-style equivalents
_BETZA_8FOLD["J"] = _BETZA_8FOLD["C"]
_BETZA_8FOLD["L"] = _BETZA_8FOLD["Z"]

_BETZA_KING = dict(f=(0, 1), b=(0, -1), l=(-1, 0), r=(1, 0),
                   lf=(-1, 1), rf=(1, 1),
                   lb=(-1, -1), rb=(1, -1))

_BETZA_ALIASES = dict(
    B = (("F", "0"),),
    R = (("W", "0"),),
    Q = (("K", "0"),),
)


# Library of moves

MOVETYPE_HEXCHESS_KING = (((1, 0), 1), ((-1, 0), 1),
                          ((0, 1), 1), ((0, -1), 1),
                          ((1, 1), 1), ((-1, -1), 1),
                          # ^^ 6 orthogonals
                          # vv 6 diagonals
                          ((1, 2), 1), ((-1, 1), 1),
                          ((-2, -1), 1), ((-1, -2), 1),
                          ((1, -1), 1), ((2, 1), 1),
                         )
MOVETYPE_HEXCHESS_QUEEN = (((1, 0), MOVERANGE_INF), ((-1, 0), MOVERANGE_INF),
                           ((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
                           ((1, 1), MOVERANGE_INF), ((-1, -1), MOVERANGE_INF),
                           # ^^ 6 orthogonals
                           # vv 6 diagonals
                           ((1, 2), MOVERANGE_INF), ((-1, 1), MOVERANGE_INF),
                           ((-2, -1), MOVERANGE_INF), ((-1, -2), MOVERANGE_INF),
                           ((1, -1), MOVERANGE_INF), ((2, 1), MOVERANGE_INF),
                          )
MOVETYPE_HEXCHESS_BISHOP = (((1, 2), MOVERANGE_INF), ((-1, 1), MOVERANGE_INF),
                            ((-2, -1), MOVERANGE_INF), ((-1, -2), MOVERANGE_INF),
                            ((1, -1), MOVERANGE_INF), ((2, 1), MOVERANGE_INF))
MOVETYPE_HEXCHESS_ROOK = (((1, 0), MOVERANGE_INF), ((-1, 0), MOVERANGE_INF),
                          ((0, 1), MOVERANGE_INF), ((0, -1), MOVERANGE_INF),
                          ((1, 1), MOVERANGE_INF), ((-1, -1), MOVERANGE_INF))
MOVETYPE_HEXCHESS_KNIGHT = (((1, 3), 1), ((-1, 2), 1),
                            ((-2, 1), 1), ((-3, -1), 1),
                            ((-3, -2), 1), ((-2, -3), 1),
                            ((-1, -3), 1), ((1, -2), 1),
                            ((2, -1), 1), ((3, 1), 1),
                            ((3, 2), 1), ((2, 3), 1),
                           )

MOVETYPE_GLINSKICHESS_PAWNCAPTURE = (((1, 1), 1), ((-1, 0), 1))
