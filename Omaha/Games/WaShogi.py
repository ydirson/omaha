# -*- coding: utf-8 -*-

# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import logging

from .abstract.GenericChesslike import PreciousKings
from .abstract.ShogiGame import (ShogiGame, ShogiGameWithDrops,
                                 ShogiGameWithoutDrops, OptionPromotionZoneShogiGame)
from .abstract.RectBoardGame import RectBoard2PSideGame
from .PieceNotations import ShogiGamePN
from .MoveNotations import ShogiGameMN
from .piecetypes.washogi import WaShogiFacetype
from . import movelib

logger = logging.getLogger(__name__)

class Piece(ShogiGame.Piece):
    MOVETYPES = {
        WaShogiFacetype.KING: movelib.betza("K"),
        WaShogiFacetype.FOX: movelib.betza("vWFvDA"),
        WaShogiFacetype.EAGLE: movelib.betza("fbRfF3K"), # wikipedia fbRfB3K ?
        WaShogiFacetype.FALCON: movelib.betza("fWB"),
        WaShogiFacetype.RABBIT: movelib.betza("fRFbW"),
        WaShogiFacetype.WOLF: movelib.betza("WfF"),
        WaShogiFacetype.STAG: movelib.betza("FfW"),
        WaShogiFacetype.DOG: movelib.betza("fFrlbW"),
        WaShogiFacetype.MONKEY: movelib.betza("fFfbW"),
        WaShogiFacetype.GOOSE: movelib.betza("fFfbW"),
        WaShogiFacetype.COCK: movelib.betza("fFrlW"),
        WaShogiFacetype.WINGS: movelib.betza("WrlR"),
        WaShogiFacetype.CROW: movelib.betza("fWbF"),
        WaShogiFacetype.OWL: movelib.betza("fWbF"),
        WaShogiFacetype.HORSE: movelib.betza("fRbW2"), # wikipedia fRbR2 ?
        WaShogiFacetype.OXCART: movelib.betza("fR"),
        WaShogiFacetype.SPARROW: movelib.betza("fW"),

        WaShogiFacetype.TFALCON: movelib.betza("fbRBW"),
        WaShogiFacetype.BEAR: movelib.betza("K"),
        WaShogiFacetype.BOAR: movelib.betza("FfrlW"),
        WaShogiFacetype.RAIDFALCON: movelib.betza("fbRWfF"),
        WaShogiFacetype.SWALLOW: movelib.betza("R"),
        WaShogiFacetype.HHORSE: movelib.betza("fbN"),
        WaShogiFacetype.OX: movelib.betza("K"),
        WaShogiFacetype.GOLD: movelib.betza("WfF"),
    }

    PROMOTIONS = {
        WaShogiFacetype.KING: None,
        WaShogiFacetype.FOX: None,
        WaShogiFacetype.EAGLE: None,
        WaShogiFacetype.FALCON: WaShogiFacetype.TFALCON,
        WaShogiFacetype.RABBIT: WaShogiFacetype.FOX,
        WaShogiFacetype.WOLF: WaShogiFacetype.BEAR,
        WaShogiFacetype.STAG: WaShogiFacetype.BOAR,
        WaShogiFacetype.DOG: WaShogiFacetype.WOLF,
        WaShogiFacetype.MONKEY: WaShogiFacetype.STAG,
        WaShogiFacetype.GOOSE: WaShogiFacetype.WINGS,
        WaShogiFacetype.COCK: WaShogiFacetype.RAIDFALCON,
        WaShogiFacetype.WINGS: WaShogiFacetype.SWALLOW,
        WaShogiFacetype.CROW: WaShogiFacetype.FALCON,
        WaShogiFacetype.OWL: WaShogiFacetype.EAGLE,
        WaShogiFacetype.HORSE: WaShogiFacetype.HHORSE,
        WaShogiFacetype.OXCART: WaShogiFacetype.OX,
        WaShogiFacetype.SPARROW: WaShogiFacetype.GOLD,
    }

class EnglishPieceNotation(ShogiGamePN.WesternShogiPieceNotation):
    # promoted pieces use "+" notation, not abbrev of promotion
    PIECE_TEXT = {
        WaShogiFacetype.KING       : "CK",
        WaShogiFacetype.FOX        : "TF",
        WaShogiFacetype.EAGLE      : "CE",
        WaShogiFacetype.FALCON     : "FF",
        WaShogiFacetype.RABBIT     : "RR",
        WaShogiFacetype.WOLF       : "VW",
        WaShogiFacetype.STAG       : "VS",
        WaShogiFacetype.DOG        : "BD",
        WaShogiFacetype.MONKEY     : "CM",
        WaShogiFacetype.GOOSE      : "FG",
        WaShogiFacetype.COCK       : "FC",
        WaShogiFacetype.WINGS      : "SW",
        WaShogiFacetype.CROW       : "SC",
        WaShogiFacetype.OWL        : "SO",
        WaShogiFacetype.HORSE      : "LH",
        WaShogiFacetype.OXCART     : "OC",
        WaShogiFacetype.SPARROW    : "SP",

        #WaShogiFacetype.TFALCON    : "TF",
        #WaShogiFacetype.BEAR       : "BE",
        #WaShogiFacetype.BOAR       : "RB",
        #WaShogiFacetype.RAIDFALCON : "RF",
        #WaShogiFacetype.SWALLOW    : "GS",
        #WaShogiFacetype.HHORSE     : "HH",
        #WaShogiFacetype.OX         : "PO",
        #WaShogiFacetype.GOLD       : "GB",
    }

class OneKanjiNotation(ShogiGamePN.OneKanjiShogiPieceNotation):
    PIECE_TEXT = {
        WaShogiFacetype.KING       : "靏",
        WaShogiFacetype.FOX        : "狐",
        WaShogiFacetype.EAGLE      : "鷲",
        WaShogiFacetype.FALCON     : "鷹",
        WaShogiFacetype.RABBIT     : "兎",
        WaShogiFacetype.WOLF       : "狼",
        WaShogiFacetype.STAG       : "鹿",
        WaShogiFacetype.DOG        : "犬",
        WaShogiFacetype.MONKEY     : "猿",
        WaShogiFacetype.GOOSE      : "鳫",
        WaShogiFacetype.COCK       : "鶏",
        WaShogiFacetype.WINGS      : "燕",
        WaShogiFacetype.CROW       : "烏",
        WaShogiFacetype.OWL        : "鴟",
        WaShogiFacetype.HORSE      : "風",
        WaShogiFacetype.OXCART     : "車",
        WaShogiFacetype.SPARROW    : "歩",

        # "Chinese character kei 9dc4 529b" is not in Unicode!
        WaShogiFacetype.TFALCON    : "�",
        WaShogiFacetype.BEAR       : "熊",
        WaShogiFacetype.BOAR       : "猪",
        WaShogiFacetype.RAIDFALCON : "延",
        WaShogiFacetype.SWALLOW    : "行",
        WaShogiFacetype.HHORSE     : "天",
        WaShogiFacetype.OX         : "牛",
        WaShogiFacetype.GOLD       : "金",
    }

class TwoKanjiNotation(ShogiGamePN.TwoKanjiShogiPieceNotation): # FIXME
    PIECE_TEXT = {
        WaShogiFacetype.KING       : "靏玉",
        WaShogiFacetype.FOX        : "隠狐",
        WaShogiFacetype.EAGLE      : "雲鷲",
        WaShogiFacetype.FALCON     : "飛鷹",
        WaShogiFacetype.RABBIT     : "走兎",
        WaShogiFacetype.WOLF       : "猛狼",
        WaShogiFacetype.STAG       : "猛鹿",
        WaShogiFacetype.DOG        : "盲犬",
        WaShogiFacetype.MONKEY     : "登猿",
        WaShogiFacetype.GOOSE      : "鳫飛",
        WaShogiFacetype.COCK       : "鶏飛",
        WaShogiFacetype.WINGS      : "燕羽",
        WaShogiFacetype.CROW       : "烏行",
        WaShogiFacetype.OWL        : "鴟行",
        WaShogiFacetype.HORSE      : "風馬",
        WaShogiFacetype.OXCART     : "牛車",
        WaShogiFacetype.SPARROW    : "萑歩",

        # "Chinese character kei 9dc4 529b" is not in Unicode!
        WaShogiFacetype.TFALCON    : "�鷹",
        WaShogiFacetype.BEAR       : "熊眼",
        WaShogiFacetype.BOAR       : "行猪",
        WaShogiFacetype.RAIDFALCON : "延鷹",
        WaShogiFacetype.SWALLOW    : "燕行",
        WaShogiFacetype.HHORSE     : "天馬",
        WaShogiFacetype.OX         : "歬牛",
        WaShogiFacetype.GOLD       : "金鳥",
    }


class EnglishWaNotation(ShogiGameMN.WesternShogiWithDropsNotation):
    PieceNotation = EnglishPieceNotation


class BaseGame(OptionPromotionZoneShogiGame, ShogiGame, # type: ignore[misc] # FIXME mypy#14279
                  PreciousKings, RectBoard2PSideGame):
    """Pieces, start position, and handicap for Wa Shogi.
    """
    Piece = Piece
    DefaultMoveNotation = EnglishWaNotation

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth, self.boardheight = 11, 11
        self.promotion_rows_count = 3

    def has_king_status(self, piecestate):
        return piecestate.face is WaShogiFacetype.KING

    # no handicap supported yet
    handicap = ()

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""

        for player in self.players:
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 0, 0),
                                    player, WaShogiFacetype.OXCART)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 0),
                                    player, WaShogiFacetype.DOG)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 2, 0),
                                    player, WaShogiFacetype.CROW)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 3, 0),
                                    player, WaShogiFacetype.GOOSE)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 4, 0),
                                    player, WaShogiFacetype.WOLF)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 5, 0),
                                    player, WaShogiFacetype.KING)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 6, 0),
                                    player, WaShogiFacetype.STAG)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 7, 0),
                                    player, WaShogiFacetype.COCK)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 8, 0),
                                    player, WaShogiFacetype.OWL)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 9, 0),
                                    player, WaShogiFacetype.MONKEY)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 10, 0),
                                    player, WaShogiFacetype.HORSE)

            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 1),
                                    player, WaShogiFacetype.FALCON)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 5, 1),
                                    player, WaShogiFacetype.WINGS)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 9, 1),
                                    player, WaShogiFacetype.EAGLE)

            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 3, 2),
                                    player, WaShogiFacetype.FOX)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 7, 2),
                                    player, WaShogiFacetype.RABBIT)

            for i in range(0, self.boardwidth):
                row = 3 if i in (3,7) else 2
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, i, row),
                                        player, WaShogiFacetype.SPARROW)

class Game(BaseGame, ShogiGameWithoutDrops): # type: ignore[misc] # FIXME mypy#14279
    pass
class GameWithDrops(BaseGame, ShogiGameWithDrops): # type: ignore[misc] # FIXME mypy#14279
    pass
