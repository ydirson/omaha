# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from ..abstract import Euclidian2D
from ..abstract.RectBoardGame import (RectBoardGame, NumberColumnRTLNotation, LetterRowTTBNotation,
                                      LetterColumnLTRNotation, NumberRowBTTNotation)
from ..PieceNotations import ShogiGamePN
from Omaha import Core

from enum import Enum
import re

import logging
logger = logging.getLogger(__name__)

class WesternShogiNotation(Core.MoveNotation):
    """Notation for Shogi with coordinates in european "1a" style"""
    PieceNotation = ShogiGamePN.WesternShogiPieceNotation
    class LocationNotation(Euclidian2D.CoordinateNotation,
                           NumberColumnRTLNotation,
                           LetterRowTTBNotation):
        "LocationNotation for WesternShogiNotation"
        game: RectBoardGame

    def move_serialization(self, move):
        return "%s%s%s%s%s" % (
            self.piece_notation.piece_name(move.gamestate.piece_state(move.piece)),
            self.location_notation.location_name(move.source),
            "x" if move.capture is not None else "-",
            self.location_notation.location_name(move.target),
            "+" if move.promotes else "")

    def move_deserialization(self, gamestate, string, player):
        assert gamestate.is_frozen
        # check for a normal move
        # FIXME: check for type/capture/promotion
        m = re.match(r"((?:\+?[A-Za-z]+)?)((\d+)([a-w]+))([-x]?)(\d+)([a-w]+)(\+?)$", string)
        if m:
            source = self.game.board.Location(self.game.board,
                                              self.location_notation.col(m.group(3)),
                                              self.location_notation.row(m.group(4)))
            target = self.game.board.Location(self.game.board,
                                              self.location_notation.col(m.group(6)),
                                              self.location_notation.row(m.group(7)))
            move = self.game.Move(gamestate=gamestate, player=player, source=source, target=target)
            if len(m.group(8)) == 0:
                move.promotes = False
            else:
                move.promotes = True
            return move

        raise Core.ParseError("Could not parse '%s' as a serialized shogi move" %
                              string)

class WesternShogiWithDropsNotation(WesternShogiNotation):
    def move_serialization(self, move):
        if move.source.holder is not self.game.board:
            return "%s*%s" % (
                self.piece_notation.piece_name(move.gamestate.piece_state(move.piece)),
                self.location_notation.location_name(move.target))
        return super().move_serialization(move)

    def move_deserialization(self, gamestate, string, player):
        assert gamestate.is_frozen
        # check for a drop
        m = re.match(r"(.*)\*(\d+)([a-w]+)$", string)
        if m:
            # identify requested piece type
            piece_type = self.piece_notation.piece_type(m.group(1))
            # check for piece to drop
            source = self.game.piece_location_by_type_inplayerpool(gamestate, piece_type, player)
            if source is None:
                raise Core.ParseError("No such piece to drop")
            return self.game.Move(
                gamestate=gamestate, player=player, source=source,
                target=self.game.board.Location(self.game.board,
                                                self.location_notation.col(m.group(2)),
                                                self.location_notation.row(m.group(3))))

        return super().move_deserialization(gamestate, string, player)

class BaseSANNotation(Core.MoveNotation):
    "Generic SAN move for Shogi games, to be subclassed"
    movename: str
    default_piece_type: Enum
    PieceNotation = ShogiGamePN.WesternShogiPieceNotation

    class LocationNotation(Euclidian2D.CoordinateNotation,
                           LetterColumnLTRNotation, NumberRowBTTNotation):
        "MoveNotation for ShogiGame.SANNotation"
        game: RectBoardGame

    def move_serialization(self, move):
        # drops
        if move.source.holder is not self.game.board:
            return "%s@%s" % (self.piece_notation.piece_name(
                move.gamestate.piece_state(move.piece)),
                              self.location_notation.location_name(move.target))

        # pawn special case
        if move.piece.type is self.default_piece_type:
            if move.capture is not None:
                s = "%sx%s" % (self.location_notation.colname(move.source),
                               self.location_notation.location_name(move.target))
            else:
                s = self.location_notation.location_name(move.target)

        # FIXME: spec requires emitting a minimized source
        # FIXME: should issue "=" when promotable move does not promote
        return (self.piece_notation.piece_name(move.gamestate.piece_state(move.piece))
                + self.location_notation.location_name(move.source)
                + ("x" if move.capture is not None else "")
                + self.location_notation.location_name(move.target)
                + ("+" if move.promotes else ""))

    regexp = r"(\+?[A-Z]?)([a-w]*)(\d*)(x?)([a-w]+)(\d+)([+=]?)(?:#?)"
    def move_deserialization(self, gamestate, string, player):
        assert gamestate.is_frozen
        logger.debug("move_deserialization(%s, player=%s)", string, player)
        # check for a drop
        m = re.match(r"(.)@([a-w]+)(\d+)(?:#?)$", string)
        if m:
            # identify requested piece type
            piece_type = self.piece_notation.piece_type(m.group(1))
            # check for piece to drop
            source = self.game.piece_location_by_type_inplayerpool(gamestate, piece_type, player)
            if source is None:
                raise Core.ParseError("No such piece to drop")
            return self.game.Move(
                gamestate=gamestate, player=player, source=source,
                target=self.game.board.Location(self.game.board,
                                                self.location_notation.col(m.group(2)),
                                                self.location_notation.row(m.group(3))))

        source_col = source_row = None
        match = re.match(self.regexp + "$", string)
        if not match:
            raise Core.ParseError("Could not parse %r as a move" % (string,))

        if match.group(1):
            if match.group(1)[0] == '+':
                if len(match.group(1)) == 1:
                    raise Core.ParseError(
                        "Could not parse %r as a promoted piece piece in %r" %
                        (match.group(1), string))
                piece_type = self.piece_notation.piece_type(match.group(1)[1])
                promoted = True
            else:
                piece_type = self.piece_notation.piece_type(match.group(1))
                promoted = False
        else:
            piece_type = self.default_piece_type
            promoted = False

        if match.group(2):
            source_col = self.location_notation.col(match.group(2))
        if match.group(3):
            source_row = self.location_notation.row(match.group(3))

        target = self.game.board.Location(self.game.board,
                                          self.location_notation.col(match.group(5)),
                                          self.location_notation.row(match.group(6)))

        try:
            target_piece = gamestate.piece_at(target)
        except IndexError:
            raise Core.ParseError("Move %r refers to non-existent square" % (string,))
        if match.group(4) and target_piece is None:
            raise Core.ParseError("Move to empty %s cannot be a capture" % (target,))

        promotes = (match.group(7) == '+')

        candidates = []
        for piece in gamestate.pieces:
            if player and gamestate.piece_owner(piece) is not player:
                continue
            if piece.unpromoted_type is not piece_type:
                continue
            if gamestate.piece_state(piece).promoted != promoted:
                continue
            loc = gamestate.piece_location_in(piece, self.game.board)
            if loc is None:     # not on board, probably captured
                continue
            if source_col is not None and source_col != loc.col:
                continue
            if source_row is not None and source_row != loc.row:
                continue
            #
            move = self.game.Move(gamestate=gamestate, player=player, source=loc, target=target)
            move.promotes = promotes
            try:
                self.game.supplement_and_check_move(gamestate,
                                                    gamestate.whose_turn, move, anticipate=True)
            except Core.InvalidMove as ex:
                logger.debug("%s not suitable for %s: %s", loc, string, ex.message)
                continue
            candidates.append(move)
        if len(candidates) == 0:
            # too detailed ...
            raise Core.ParseError("Found no suitable %s for %s" % (piece_type, string))
        if len(candidates) > 1:
            raise Core.ParseError("Could not disambiguate %s between: %s" %
                                  (string, ' or '.join(str(c.source) for c in candidates)))
        move = candidates[0]

        return move
