# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.GenericChesslike import PreciousKings
from .abstract.ShogiGame import ShogiGameWithDrops, OptionPromotionZoneShogiGame
from .abstract import ModernShogi
from .abstract.RectBoardGame import RectBoard2PSideGame
from .MoveNotations import StandardShogiMN
from .piecetypes.shogi import ShogiFacetype
import Overlord

class MiniShogi(ShogiGameWithDrops, ModernShogi.Game, # type: ignore[misc] # FIXME mypy#14279
                PreciousKings, RectBoard2PSideGame, OptionPromotionZoneShogiGame):

    DefaultMoveNotation = StandardShogiMN.EnglishShogiWithDropsNotation

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth, self.boardheight = 5, 5
        self.promotion_rows_count = 1

    @Overlord.parameter
    def handicap(cls, context):
        return Overlord.params.Choice(
            label = 'Handicap',
            alternatives = {
                "0 - None": (),
                "1 - Bishop": (ShogiFacetype.BISHOP,),
                "2 - Rook": (ShogiFacetype.ROOK,),
                "3 - Rook and bishop": (ShogiFacetype.ROOK,
                                        ShogiFacetype.BISHOP) },
            default = ())

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""

        for player in self.players:
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 0, 0),
                                    player, ShogiFacetype.KING)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 0),
                                    player, ShogiFacetype.GOLD)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 2, 0),
                                    player, ShogiFacetype.SILVER)
            if player is self.players[0] or \
                   ShogiFacetype.BISHOP not in self.handicap:
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 3, 0),
                                        player, ShogiFacetype.BISHOP)
            if player is self.players[0] or \
                   ShogiFacetype.ROOK not in self.handicap:
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, 4, 0),
                                        player, ShogiFacetype.ROOK)

            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 0, 1),
                                    player, ShogiFacetype.PAWN)

    def has_king_status(self, piecestate):
        return piecestate.face is ShogiFacetype.KING

    def get_writer_class(self, filename):
        assert filename.endswith(".pgn")
        from Omaha.GameIO.PGNWriter import PGNGameWriter
        return PGNGameWriter
