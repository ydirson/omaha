# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Generic support for standard/modern shogi variants on a square board.

This means standard type of promotion, but not necessarily drops.
"""
from . import GenericChesslike
from .RectBoardGame import OnePiecePerLocRectBoardGame
from Omaha.Core import IncompleteMove, InvalidMove
from Omaha import Core
import Overlord

import logging
import sys
if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

logger = logging.getLogger(__name__)

class Capture(GenericChesslike.Game.Move.Capture):
    def __init__(self, piece):
        super().__init__(piece)
        self.promoted = None

class ShogiMove(GenericChesslike.Game.Move):
    Capture = Capture
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.originally_promoted = False # Whether that piece was
                                         # promoted before moving
        self.promotes = None

    def store_captured_piece(self, piecestate):
        super().store_captured_piece(piecestate)
        self.capture.promoted = piecestate.promoted

    def __str__(self):
        result = super().__str__()
        if self.capture:
            result += " [x%s]" % self.gamestate.piece_description(self.capture.piece)
        return result

class ShogiPiece(GenericChesslike.Piece):
    # pylint: disable=abstract-method
    def __init__(self, piecetype):
        super().__init__(piecetype)
        assert piecetype in self.PROMOTIONS
        self.__unpromoted_type = piecetype
        self.__promoted_type = self.PROMOTIONS[piecetype]
    def has_promotion(self):
        return self.__promoted_type is not None
    @property
    def promoted_type(self):
        return self.__promoted_type
    @property
    def unpromoted_type(self):
        return self.__unpromoted_type

class PieceState(Core.FacetedPieceState):
    def __init__(self, piece):
        super().__init__(piece)
        self.__promoted = False
    def clone(self):
        c = super().clone()
        c.__promoted = self.__promoted
        return c
    def freeze(self):
        super().freeze()
        self.promote = NotImplemented
        self.unpromote = NotImplemented
    @property
    def face(self):
        if self.__promoted:
            return self.piece.promoted_type
        else:
            return self.piece.unpromoted_type
    @property
    def promoted(self):
        return self.__promoted
    def promote(self): # pylint: disable=method-hidden
        self.__promoted = True
    def unpromote(self): # pylint: disable=method-hidden
        self.__promoted = False

class GameState(Core.OnePieceTypePerLocGameState, Core.AlternatingTurnsGame.State):
    PieceState = PieceState
 
class ShogiGame(OnePiecePerLocRectBoardGame,   # type: ignore[misc] # FIXME mypy#14279
                GenericChesslike.Game):

    SIDE_SENTE = "sente"
    SIDE_GOTE  = "gote"

    Piece: TypeAlias = ShogiPiece
    Move = ShogiMove
    State = GameState

    player_names = ("Sente", "Gote")

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth, self.boardheight = None, None
        self.promotion_rows_count = None

    def setup_players(self):
        super().setup_players()
        self.players[0].color = self.SIDE_SENTE
        self.players[1].color = self.SIDE_GOTE
        if self.handicap != ():
            self.scratch_state.set_whose_turn(self.players[1])

    def _ensure_move_is_complete(self, gamestate, move):
        super()._ensure_move_is_complete(gamestate, move)
        move.promotes = False

    def do_move(self, gamestate, player, move):
        super().do_move(gamestate, player, move)

        # promotion
        if move.promotes is True:
            gamestate.piece_state(move.piece).promote()

        # put (possibly modified) piece at target location
        gamestate.put_piece(move.target, move.piece)

    def _undo_capture(self, move):
        # give back to its player
        self.scratch_state.set_piece_owner(move.capture.piece, self.scratch_state.whose_turn.next)
        # re-promote piece it was when captured
        if move.capture.promoted:
            self.scratch_state.piece_state(move.capture.piece).promote()
        self.scratch_state.put_piece(move.target, move.capture.piece)

    def _undo_move(self, move):
        super()._undo_move(move)

        # un-promote
        if move.promotes is True:
            self.scratch_state.piece_state(move.piece).unpromote()

    @property
    def handicap(self):
        raise NotImplementedError()

    ###

    def location_tooltip_text(self, gamestate, loc):
        # this overrides the GenericChesslike.Game implementation
        if loc is None:
            return None
        piece = gamestate.piece_at(loc)
        if piece is None:
            return None
        # FIXME try:
        betza = " (%s)" % self.Piece.MOVETYPES[gamestate.piece_state(piece).face].betza
        if piece.has_promotion():
            if not gamestate.piece_state(piece).promoted:
                otherbetza = " (%s)" % self.Piece.MOVETYPES[piece.promoted_type].betza
                return "{3.face.value}{1}\nPromotes to {0.promoted_type.value}{2}".format(
                    piece, betza, otherbetza, gamestate.piece_state(piece))
            else:
                otherbetza = " (%s)" % self.Piece.MOVETYPES[piece.unpromoted_type].betza
                return "{3.face.value}{1}\nPromoted from {0.unpromoted_type.value}{2}".format(
                    piece, betza, otherbetza, gamestate.piece_state(piece))
        else:
            return "{0.face.value}{1}".format(gamestate.piece_state(piece), betza)

class OptionPromotionZoneShogiGame(ShogiGame):
    # pylint: disable=abstract-method

    def _in_promotion_zone(self, location, player):
        return (location.holder is self.board and
                abs(location.row - self.player_homerow(player.next)) + 1 <=
                self.promotion_rows_count)

    def supplement_and_check_move(self, gamestate, player, move, anticipate):
        super().supplement_and_check_move(gamestate, player, move, anticipate)

        self.check_standard_moves(gamestate, player, move, anticipate)

        # adjustements
        if gamestate.piece_state(move.piece).promoted:
            move.originally_promoted = True

        # handle piece promotion
        if  ((self._in_promotion_zone(move.source, player) or
              self._in_promotion_zone(move.target, player)) and
             move.source.holder is self.board and
             move.piece.has_promotion() and
             not gamestate.piece_state(move.piece).promoted):
            logger.debug("(%x) move has unspecified promotion: %s", id(self), move)
            if move.promotes is None:
                raise IncompleteMove({
                    'promotes': Overlord.params.Choice(
                        label = "Promotes",
                        alternatives = dict(yes = True,
                                            no = False),
                        default = True)
                    })
        elif move.promotes is True:
            raise InvalidMove("illegal promotion request")

###

class ShogiGameWithoutDrops(ShogiGame):
    # pylint: disable=abstract-method
    "Trivial class minimally distinct from `ShogiGameWithDrops`"
    pass

###

from .PlayerPools import PlayerPools

class ShogiGameWithDrops(PlayerPools, ShogiGame): # type: ignore[misc] # FIXME mypy#14279
    # pylint: disable=abstract-method
    def _process_captured_piece(self, gamestate, target_piece, player):
        """Move captured piece to player's pool"""
        gamestate.set_piece_owner(target_piece, player)
        gamestate.piece_state(target_piece).unpromote()
        gamestate.put_piece(
            self.pool_of(player).Location(self.pool_of(player), None),
            target_piece)

    def _undo_move(self, move):
        # first remove any captured piece from pool, to avoid any bad
        # interaction with its move back onto the board
        if move.capture is not None:
            self.scratch_state.take_piece(move.capture.piece)
        super()._undo_move(move)

    def check_standard_moves(self, gamestate, player, move, anticipate):
        if move.source.holder is self.board:
            super().check_standard_moves(gamestate, player, move, anticipate)
        else:
            # check drop validity
            if gamestate.piece_at(move.target) is not None:
                raise InvalidMove("cannot drop on occupied location")
            # FIXME: lacks "no more than 2 unpromoted pawns in file"
            # FIXME: lacks "dropped pawn must not checkmate"

###

Core.UndoableGame.register(ShogiGame)
