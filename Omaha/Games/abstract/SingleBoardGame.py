# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import re
import sys
from typing import Sequence

from Omaha.Holders.abstract.Euclidian2D import Vector
from Omaha import Core
from Overlord.misc import classproperty

if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

class SingleBoardGame(Core.Game):
    # pylint: disable=abstract-method
    "A game with a single board, but maybe other holders."

    __innerclasses__ = {'Board'}
    Board: TypeAlias = Core.PieceHolder

    @classproperty
    @classmethod
    def holder_classes(cls) -> tuple[type[Core.PieceHolder], ...]:
        holder_classes = super().holder_classes
        return holder_classes + (cls.Board,)

    @property
    def board(self) -> Board:
        return self._holders['board']

    def new_piece_on_board(self, gamestate: Core.GameState, coords: Sequence, *piece_args) -> None:
        """Convenience method to create a new piece on self.board.

        This is only meaningful for single-board games."""
        assert isinstance(gamestate, self.State)
        gamestate.new_piece_at(self.board.Location(self.board, *coords), *piece_args)


class PlayerAreasGame(SingleBoardGame):
    "A single-board game where player have symmetric positions"

    @property
    def player_to_board_rotations(self):
        """Rotations to transform player-relative direction to board-relative.

        Tuple of player index -> Xform.
        """
        raise NotImplementedError()

    @property
    def board_to_player_rotations(self):
        """Rotations to transform board-relative direction to player-relative.

        Tuple of player index -> Xform.
        """
        raise NotImplementedError()

    @property
    def player_to_board_xlats(self):
        """Translation component of player-to-board coordinate xform.

        Should be applied after `player_to_board_rotations` to transform a
        player-relative position to board-relative.

        Tuple of player index -> Xform.

        """
        raise NotImplementedError()

    @property
    def board_to_player_xlats(self):
        """Translation component of board-to-player coordinate xform.

        Should be applied before `board_to_player_rotations` to transform a
        board-relative position to player-relative.

        Tuple of player index -> Xform.

        """
        raise NotImplementedError()

    def player_to_board_rot(self, player):
        assert player in self.players, "%s not in %s" % (player, self.players)
        return self.player_to_board_rotations[self.players.index(player)]
    def board_to_player_rot(self, player):
        return self.board_to_player_rotations[self.players.index(player)]

    def player_to_board_xlat(self, player):
        return self.player_to_board_xlats[self.players.index(player)]
    def board_to_player_xlat(self, player):
        return self.board_to_player_xlats[self.players.index(player)]

    def player_to_board_xform(self, player):
        return self.player_to_board_xlat(player) * self.player_to_board_rot(player)
    def board_to_player_xform(self, player):
        return self.board_to_player_rot(player) * self.board_to_player_xlat(player)

    def player_to_board_delta(self, delta, player):
        """
        Turn a player-relative delta into a board-relative one.
        """
        return delta.xformed(self.player_to_board_rot(player))
    def board_to_player_delta(self, delta, player):
        """
        Turn a board-relative delta into a player-relative one.
        """
        return delta.xformed(self.board_to_player_rot(player))

    def player_to_board_pos(self, pos, player):
        """
        Turn a player-relative delta into a board-relative one.
        """
        assert isinstance(pos, Vector)
        return self.player_to_board_xform(player) * pos
    def board_to_player_pos(self, pos, player):
        """
        Turn a board-relative delta into a player-relative one.
        """
        assert isinstance(pos, Vector)
        return self.board_to_player_xform(player) * pos


class OnePiecePerLocSingleBoardGame(SingleBoardGame):
    Board = Core.OnePiecePerLocHolder
    State: TypeAlias = Core.OnePieceTypePerLocGameState

class OnlySingleBoardGame(SingleBoardGame):
    # pylint: disable=abstract-method
    """A game with a single board and no other holder.

    Adds no specific behaviour, but allows finer game filtering for
    renderers.
    """
