# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

import logging
from string import ascii_lowercase
import sys

from . import Euclidian2D
from .SingleBoardGame import OnePiecePerLocSingleBoardGame, PlayerAreasGame
from Omaha import Core
from Omaha.Holders.RectBoard import RectBoard, OnePiecePerLocRectBoard
from Omaha.Holders.abstract.Euclidian2D import Xform, Vector, Euclidian2DLocation

if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

logger = logging.getLogger(__name__)

class RectBoardGame(Euclidian2D.Game):
    # pylint: disable=abstract-method
    """Abstract base class for RectBoard-based games."""
    Board: TypeAlias = RectBoard
    board: Board

    boardwidth: int
    boardheight: int

    def create_holders(self):
        super().create_holders()
        self._holders['board'] = self.Board(game=self, parentcontext=self.context,
                                            ncolumns=self.boardwidth, nrows=self.boardheight)


class PlayerAreasRectGame(PlayerAreasGame):

    def player_relative_pos(self, player, col, row):
        v = self.player_to_board_pos(Vector.make_vector(col, row), player)
        assert isinstance(v, Vector)
        return (v.x, v.y)

    def player_relative_location(self, player, col, row):
        coords = self.player_relative_pos(player, col, row)
        return self.board.Location(self.board, *coords)

    def player_orientation(self, player: Core.Player) -> tuple[int,int]:
        res = self.player_to_board_rot(player) * Vector.make_vector(0, +1)
        return (res.x, res.y)


class OnePiecePerLocRectBoardGame(RectBoardGame, OnePiecePerLocSingleBoardGame):
    Board: TypeAlias = OnePiecePerLocRectBoard

    def dump_board(self, gamestate: OnePiecePerLocRectBoardGame.State) -> None:
        "Debug helper: dump the state of the board to stdout."
        for row in range(self.boardheight):
            for column in range(self.boardwidth):
                piece = gamestate.piece_at(self.board.Location(self.board, column, row))
                if piece:
                    piece_state = gamestate.piece_state(piece)
                    print(self.default_notation.piece_notation.piece_name(piece_state), end='')
                else:
                    print(".", end='')
            print()


class RectBoard2PSideGame(RectBoardGame, PlayerAreasRectGame):
    # pylint: disable=abstract-method
    """Abstract base class for 2-player RectBoard-based games moving from opposing sides."""

    @property
    def player_to_board_rotations(self):
        return (Xform.from_22array( ((1,  0),
                                     (0,  1)) ),
                Xform.from_22array( ((-1, 0),
                                     (0, -1)) ),
        )
    @property
    def board_to_player_rotations(self):
        # 2-player is fully symmetric
        return self.player_to_board_rotations

    @property
    def player_to_board_xlats(self):
        return (Xform.make_translation(0, 0),
                Xform.make_translation(self.boardwidth - 1, self.boardheight - 1),
        )
    @property
    def board_to_player_xlats(self):
        return (Xform.make_translation(0, 0),
                Xform.make_translation(1 - self.boardwidth, 1 - self.boardheight),
        )

    @property
    def player_homerows(self):
        return (0, self.boardheight - 1)

    def player_homerow(self, player):
        return self.player_homerows[self.players.index(player)]

    def player_relative_row(self, player, row):
        return self.player_relative_pos(player, 0, row)[1]

    def player_relative_column(self, player, col):
        return self.player_relative_pos(player, col, 0)[0]


class RectBoard4PCornerGame(RectBoardGame, PlayerAreasRectGame):
    """Abstract base class for 4-player RectBoard-based games moving from corners to center."""

    @property
    def player_to_board_rotations(self):
        return (Xform.from_22array( ((1,  0),
                                     (0,  1)) ),
                Xform.from_22array( ((0,  1),
                                     (-1, 0)) ),
                Xform.from_22array( ((-1, 0),
                                     (0, -1)) ),
                Xform.from_22array( ((0, -1),
                                     (1,  0)) ),
        )
    @property
    def board_to_player_rotations(self):
        return (Xform.from_22array( ((1,  0),
                                     (0,  1)) ),
                Xform.from_22array( ((0, -1),
                                     (1,  0)) ),
                Xform.from_22array( ((-1, 0),
                                     (0, -1)) ),
                Xform.from_22array( ((0,  1),
                                     (-1, 0)) ),
        )

    @property
    def player_to_board_xlats(self):
        return (Xform.make_translation(0, 0),
                Xform.make_translation(0, self.boardheight - 1),
                Xform.make_translation(self.boardwidth - 1, self.boardheight - 1),
                Xform.make_translation(self.boardwidth - 1, 0),
        )
    @property
    def board_to_player_xlats(self):
        return (Xform.make_translation(0, 0),
                Xform.make_translation(0, 1 - self.boardheight),
                Xform.make_translation(1 - self.boardwidth, 1 - self.boardheight),
                Xform.make_translation(1 - self.boardwidth, 0),
        )

    @property
    def player_homecorners(self):
        return ((0, 0), (0, self.boardheight - 1),
                (self.boardwidth - 1, self.boardheight - 1),
                (self.boardwidth - 1, 0))

    def player_homecorner(self, player):
        return self.player_homecorners[self.players.index(player)]


# common notation classes
# FIXME: not all directions implemented yet, fill as needed

class LetterColumnLTRNotation(Core.LocationNotation):
    # pylint: disable=abstract-method
    "Columns lettered from left to right"
    game: RectBoardGame
    def colname(self, location: Euclidian2DLocation) -> str:
        return ascii_lowercase[location.col]
    def col(self, colname: str) -> int:
        return ascii_lowercase.index(colname)
class NumberColumn0LTRNotation(Core.LocationNotation):
    # pylint: disable=abstract-method
    "Columns numbered from left to right starting from 0"
    game: RectBoardGame
    def colname(self, location: Euclidian2DLocation) -> str:
        return str(location.col)
    def col(self, colname: str) -> int:
        return int(colname)
class NumberColumnRTLNotation(Core.LocationNotation):
    # pylint: disable=abstract-method
    "Columns numbered from right to left"
    game: RectBoardGame
    def colname(self, location: Euclidian2DLocation) -> str:
        return "%s" % (self.game.boardwidth - location.col)
    def col(self, colname: str) -> int:
        return self.game.boardwidth - int(colname)

class LetterRowTTBNotation(Core.LocationNotation):
    # pylint: disable=abstract-method
    "Rows lettered from top to bottom"
    game: RectBoardGame
    def rowname(self, location: Euclidian2DLocation) -> str:
        return ascii_lowercase[self.game.boardheight - 1 - location.row]
    def row(self, rowname: str) -> int:
        try:
            return self.game.boardheight - 1 - ascii_lowercase.index(rowname)
        except ValueError:
            logger.error("{0!r} not a lowercase char".format(rowname))
            raise
class NumberRowBTTNotation(Core.LocationNotation):
    # pylint: disable=abstract-method
    "Rows numbered from bottom to top"
    game: RectBoardGame
    def rowname(self, location: Euclidian2DLocation) -> str:
        return "%s" % (location.row + 1)
    def row(self, rowname: str) -> int:
        return int(rowname) - 1
class NumberRowTTBNotation(Core.LocationNotation):
    # pylint: disable=abstract-method
    "Rows numbered from top to bottom"
    game: RectBoardGame
    def rowname(self, location: Euclidian2DLocation) -> str:
        return "%s" % (self.game.boardheight - location.row)
    def row(self, rowname: str) -> int:
        return self.game.boardheight - int(rowname)
