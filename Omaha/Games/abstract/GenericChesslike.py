# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from enum import Enum
import logging
import re
import sys
from typing import cast, Any, Callable, Optional, Union
if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

from Omaha import Core
from Omaha.Core import InvalidMove
import Overlord

from . import Euclidian2D
from .RectBoardGame import RectBoardGame, LetterColumnLTRNotation, NumberRowBTTNotation

logger = logging.getLogger(__name__)

MOVERANGE_INF = "infinity"

class Capture:
    "Container tracking info on a move's capture, for applying and undoing the capture"
    def __init__(self, piece):
        assert piece
        self.piece = piece
    def __str__(self):
        raise DeprecationWarning("Capture.__str__")
        return "Capture(%s)" % (self.piece,)

MovePredicate: TypeAlias = Callable # FIXME
# FIXME: "str" here should be Literal[MOVERANGE_INF]
MoveDescription: TypeAlias = tuple[tuple[tuple, Union[int,str]], ...]
MoveTypeId: TypeAlias = Any

class Piece(Core.Piece):
    # class variable to overload with the actual declaration
    # for a given game
    MOVETYPES: dict[Enum,tuple[tuple[MovePredicate,MoveDescription,MoveTypeId],...]]
    PROMOTIONS: dict[Enum,Optional[Enum]]

class GenericChesslikeMove(Core.SimpleDisplaceMove,
                           metaclass=Overlord.OuterClass):
    __innerclasses__ = {'Capture'}
    Capture: TypeAlias = Capture
    def __init__(self, **kwargs):
        self.piece = None       # The piece being moved
        super().__init__(**kwargs)
        self.type = None        # castling, en-passant, etc.
        self.capture = None # a Capture object, or None
    def set_source(self, location):
        super().set_source(location)
        assert self.source is location
        if self.source is not None:
            self.piece = self.gamestate.piece_at(self.source)
            assert self.piece, "no piece at {0}".format(self.source)
    def store_captured_piece(self, piecestate):
        "Store `piece` as being captured by the move, with enough info for undo"
        assert self.capture is None, \
            "capture already %s (wants to capture %s)" % (self.capture, piecestate.piece)
        self.capture = self.Capture(piecestate.piece)

class Game(Core.AlternatingTurnsGame):

    Piece: TypeAlias = Piece
    Move: TypeAlias = GenericChesslikeMove
    State: TypeAlias = Core.OnePieceTypePerLocGameState

    def supplement_and_check_move(self, gamestate, player, move, anticipate):
        super().supplement_and_check_move(gamestate, player, move, anticipate)

        if move.source is None:
            raise InvalidMove("move must have a source location")
        if move.target is None:
            # FIXME: should be IncompleteMove ?
            raise InvalidMove("move must have a target location")

        targetpiece = gamestate.piece_at(move.target)
        # basic checks
        if move.piece is None:
            raise InvalidMove("no piece to move")
        piece_owner = gamestate.piece_owner(move.piece)
        assert piece_owner in self.players, \
            "%s has bad owner, expecting one of %s" % (move.piece, self.players)
        if piece_owner is not player:
            raise InvalidMove("player %s can only move his own pieces, not %s" %
                              (player, gamestate.piece_description(move.piece)))
        if targetpiece:
            if gamestate.piece_owner(targetpiece) is player:
                raise InvalidMove("player cannot capture his own pieces")
            if move.capture is not None:
                # capture was already filled, eg. because we're a clone
                assert move.capture.piece is targetpiece
            else:
                move.store_captured_piece(gamestate.piece_state(targetpiece))

    def check_standard_moves(self, gamestate, player, move, anticipate):
        assert gamestate.is_frozen
        piece_face = gamestate.piece_state(move.piece).face
        if piece_face in self.Piece.MOVETYPES:
            # select movetypes entry
            assert len(self.Piece.MOVETYPES[piece_face]) > 0
            for predicate, movetypes, typeid in self.Piece.MOVETYPES[piece_face]:
                # check if this move is allowed in this movetypes set
                for movetype_direction, movetype_range in movetypes:
                    movetype_dirdelta = move.source.Delta(*movetype_direction)
                    if not move.source.holder.move_follows_direction(
                            move, self.player_to_board_delta(movetype_dirdelta, player)):
                        continue
                    if movetype_range is MOVERANGE_INF:
                        if predicate(gamestate, move, movetype_direction, None):
                            # allowed, stop searching
                            break
                    nunits = move.source.holder.nunits_in_direction(
                        move, self.player_to_board_delta(movetype_dirdelta, player))
                    if isinstance(movetype_range, tuple):
                        range_min, range_max = movetype_range
                        if  (nunits and nunits <= range_max
                             and nunits >= range_min):
                            if predicate(gamestate, move, movetype_direction, nunits):
                                # allowed, stop searching
                                break
                    else:
                        if nunits and (movetype_range == "infinity" or
                                       nunits <= movetype_range):
                            if predicate(gamestate, move, movetype_direction, nunits):
                                # allowed, stop searching
                                break
                else:
                    # move not accepted by this movetypes set, try next
                    continue

                # move was accepted, skip next movetypes sets
                break

            else:
                # all movetypes sets exhausted without a match
                raise InvalidMove("cannot move there")

            # register what type this movetype has
            move.type = typeid
        else:
            raise InvalidMove("no moves defined for %s" % piece_face)

        # forbid moving into check
        if anticipate:
            piece_owner = gamestate.piece_owner(move.piece)
            assert piece_owner is player, "%s != %s" % (piece_owner, player)
            logger.debug("move: %s", move)
            state_clone = gamestate.clone()
            try:
                self._make_move(state_clone, player, move, anticipate=False)
            except Exception as ex:
                logger.error("move failed on clone")
                raise
            checking_piece = self.checking_piece(state_clone, player)
            if checking_piece:
                raise InvalidMove("player's king would be in check by %s" %
                                  gamestate.piece_description(checking_piece))

    ###

    def _process_captured_piece(self, gamestate, target_piece, player):
        """Do whatever is needed with `target_piece` captured by `player`.

        Nothing to do by default (we just forget about that piece).
        """
        pass

    def do_move(self, gamestate, player, move):
        assert isinstance(player, Core.Player)
        assert move.source.holder in self.holders
        assert move.target.holder is self.board
        assert gamestate.piece_holder(move.piece) is move.source.holder

        # Capturing
        if move.capture is not None:
            captured_piece = move.capture.piece
            assert gamestate.piece_holder(captured_piece) is move.target.holder
            gamestate.take_piece(captured_piece)
            self._process_captured_piece(gamestate, captured_piece, player)

        # remove piece from original location
        gamestate.take_piece(move.piece)

        # we don't put piece back into place yet, as some games like
        # Chess handle promotions by using a new piece instead

    def _undo_capture(self, move):
        raise NotImplementedError()

    def _undo_move(self, move):
        assert self.scratch_state.piece_holder(move.piece) is move.target.holder

        # remove piece from target location
        self.scratch_state.take_piece(move.piece)
        # put piece back to source loc
        self.scratch_state.put_piece(move.source, move.piece)

        # put captured piece back into place, promoted if needed
        if move.capture is not None:
            self._undo_capture(move)

    ###

    def checking_piece(self, gamestate, player):
        "Return a piece causing `player` to be in check, or `None`."
        raise NotImplementedError()

    def is_attacked(self, gamestate, *, piece):
        player = gamestate.piece_owner(piece)
        location = gamestate.piece_location(piece)
        frozen_gamestate = gamestate.frozen()
        for piece in gamestate.pieces:
            if piece.location(gamestate) is None:
                # piece permanenly removed from the game, cannot attack
                continue
            piece_owner = gamestate.piece_owner(piece)
            if piece_owner is player:
                # don't check player's own pieces
                continue
            logger.debug("(%x)  does %s attack %s ?", id(self),
                         gamestate.piece_description(piece), location)
            move = self.Move(gamestate=frozen_gamestate, player=piece_owner,
                             source=piece.location(gamestate), target=location)
            self._ensure_move_is_complete(frozen_gamestate, move)
            try:
                self.supplement_and_check_move(frozen_gamestate, piece_owner, move, anticipate=False)
                # move is correct: location is attacked
                return piece
            except InvalidMove:
                # piece does not attack, try next one
                continue
            except Core.IncompleteMove as ex:
                import traceback
                assert False, "_ensure_move_is_complete did not work: IncompleteMove(%s), %s" % (
                    ex.paramlist.keys(), traceback.format_exc(ex),)
        return False

    def _ensure_move_is_complete(self, gamestate, move):
        """Complete (capturing) move if needed so it can be applied.

        This is only used to check if a move can attack a given
        location, so any arbitrary way of making the move complete
        will do, as long as it does not change the target location.
        """
        pass

    def valid_move_completions(self, gamestate, incomplete_move):
        """
        The list of moves that would be accepted given (incomplete) move.
        """
        if incomplete_move.source is None:
            return []
        # FIXME: what if incomplete_move.target is not None ?

        # check if Location has Delta support
        try:
            Delta = incomplete_move.source.holder.Location.Delta
        except AttributeError:
            return []

        piece = incomplete_move.piece
        piece_face = gamestate.piece_state(piece).face

        if piece_face in self.Piece.MOVETYPES:
            valid_moves = []
            for predicate, movetypes, typeid in self.Piece.MOVETYPES[piece_face]:
                for movetype_direction, movetype_range in movetypes:
                    # generate moves according to direction and range
                    delta = self.player_to_board_delta(Delta(*movetype_direction),
                                                       gamestate.whose_turn)
                    target = incomplete_move.source
                    target += delta
                    # loop while target is valid
                    if isinstance(movetype_range, tuple):
                        range_min, range_max = movetype_range
                    else:
                        range_min, range_max = (1, movetype_range)
                    deltacount = 1 # how far we are currently looking
                    while (gamestate.valid_location(target) and
                           (range_max is MOVERANGE_INF or
                            deltacount <= range_max)):
                        # build the move and check the movetype's predicate
                        move = self.Move(gamestate=gamestate, player=incomplete_move.player,
                                         source=incomplete_move.source,
                                         target=target)
                        if  (deltacount >= range_min and
                             predicate(gamestate, move, movetype_direction, None)):
                            valid_moves.append(move)
                        # next iteration
                        target += delta
                        deltacount += 1
            return valid_moves
        else:
            return []

    ###

    has_tooltip = True

    def location_tooltip_text(self, gamestate, loc):
        if loc is None:
            return None
        if not gamestate.valid_location(loc):
            return None
        piece = gamestate.piece_at(loc)
        if piece is None:
            return None
        return gamestate.piece_state(piece).face.value

class ChessLikeCoordinateNotation(Core.MoveNotation[Core.SimpleDisplaceMove]):
    """"Move notation of the style "a1b2". """
    movename = "generic chesslike coordinates move"
    class LocationNotation(Euclidian2D.CoordinateNotation,
                           LetterColumnLTRNotation, NumberRowBTTNotation):
        "LocationNotation for ChessLikeCoordinateNotation"
        game: RectBoardGame

    # default implementation for "a1" style serialization
    def __init__(self, game: "Game"):
        #assert isinstance(game, SingleBoardGame)
        super().__init__(game)

    def move_serialization(self, move: Core.SimpleDisplaceMove) -> str:
        assert move.source
        assert move.target
        return "%s%s" % (self.location_notation.location_name(move.source),
                         self.location_notation.location_name(move.target))

    regexp = r"([a-w]+\d+)([a-w]+\d+)"
    def move_from_match(self, gamestate: "Core.GameState", match: re.Match[str], player: Core.Player
                        ) -> Core.SimpleDisplaceMove:
        MoveClass: TypeAlias = cast(type[Core.SimpleDisplaceMove], self.game.Move) # FIXME cast
        assert issubclass(MoveClass, Core.SimpleDisplaceMove)
        return MoveClass(gamestate=gamestate, player=player,
                         source=self.location_notation.location(match.group(1)),
                         target=self.location_notation.location(match.group(2)))

    def move_deserialization(self, gamestate: "Core.GameState", string: str,
                             player: Core.Player) -> Core.SimpleDisplaceMove:
        assert gamestate.is_frozen
        # only match exact string, don't leave any trailing chars behind
        m = re.match(self.regexp + "$", string)
        if m:
            return self.move_from_match(gamestate, m, player)
        raise Core.ParseError("could not parse '%s' as a %s" %
                              (string, self.movename))

###

class PreciousKings(Game):
    """A chess-like game where the game is won by mating any king.

    Of course, applies to standard/FIDE chess, as a variant with a
    single king."""

    def has_king_status(self, piecestate):
        "Must return True if the piece is considered like a king for mating."
        raise NotImplementedError()

    def checking_piece(self, gamestate, player):
        "Return a piece checking (one of) player's king(s)."
        for p in gamestate.pieces:
            if p.location(gamestate):
                assert p.location(gamestate).holder in self.holders
        for p in gamestate.pieces:
            if gamestate.piece_owner(p) is not player:
                continue # only consider player's own king(s)
            if not self.has_king_status(gamestate.piece_state(p)):
                continue
            checking_piece = self.is_attacked(gamestate, piece=p)
            if checking_piece:
                return checking_piece
            break # King is OK
        else:
            assert False, "%s has no king" % (player,)
        # not in check
        return False


class LastKingStanding(Game):
    "A chess-like game where the game is won by mating the opponent's last king."

    def has_king_status(self, piecestate):
        "Must return True if the piece is considered like a king for mating."
        raise NotImplementedError()

    def checking_piece(self, gamestate, player):
        logger.debug("LastKingStanding.checking_piece")
        for p in gamestate.pieces:
            if p.location(gamestate):
                assert p.location(gamestate).holder in self.holders
        kings_count = 0
        for p in gamestate.pieces:
            if gamestate.piece_owner(p) is not player:
                continue # only consider player's own king
            if not self.has_king_status(gamestate.piece_state(p)):
                continue
            if p.location(gamestate) is None:
                continue
            kings_count += 1
            checking_piece = self.is_attacked(gamestate, piece=p)
            if not checking_piece:
                return False # One royal piece is not attacked
        assert kings_count, "%s has no king" % (player,)
        if kings_count == 1:
            # single remaining royal piece is in check
            return checking_piece
        # all royal pieces are in check, but at least one could survive
        return False
