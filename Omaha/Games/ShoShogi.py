# -*- coding: utf-8 -*-

# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.GenericChesslike import LastKingStanding
from .abstract.ShogiGame import ShogiGame, ShogiGameWithoutDrops, OptionPromotionZoneShogiGame
from .abstract import ModernShogi
from .abstract.SingleBoardGame import OnlySingleBoardGame
from .abstract.RectBoardGame import RectBoard2PSideGame
from .MoveNotations import ShogiGameMN
from .PieceNotations import ShogiGamePN, StandardShogiPN
from .piecetypes.shogi import ShogiFacetype
from .piecetypes.shoshogi import ShoShogiFacetype
from . import movelib

class Piece(ShogiGame.Piece):
    MOVETYPES = dict(ModernShogi.Piece.MOVETYPES)
    MOVETYPES.update({
        ShoShogiFacetype.DRUNKENELEPHANT: movelib.betza("FsfW"),
        ShoShogiFacetype.CROWNPRINCE: movelib.betza("K"),
    })

    PROMOTIONS = dict(ModernShogi.Piece.PROMOTIONS)
    PROMOTIONS[ShoShogiFacetype.DRUNKENELEPHANT] = ShoShogiFacetype.CROWNPRINCE

class EnglishPieceNotation(ShogiGamePN.WesternShogiPieceNotation):
    PIECE_TEXT = dict(StandardShogiPN.EnglishShogiPieceNotation.PIECE_TEXT)
    PIECE_TEXT[ShoShogiFacetype.DRUNKENELEPHANT] = "DE"

class OneKanjiPieceNotation(ShogiGamePN.OneKanjiShogiPieceNotation):
    PIECE_TEXT = dict(StandardShogiPN.OneKanjiShogiNotation.PIECE_TEXT)
    PIECE_TEXT[ShoShogiFacetype.DRUNKENELEPHANT] = "酔"
    PIECE_TEXT[ShoShogiFacetype.CROWNPRINCE]     = "太"

class TwoKanjiPieceNotation(ShogiGamePN.TwoKanjiShogiPieceNotation):
    PIECE_TEXT = dict(StandardShogiPN.TwoKanjiShogiNotation.PIECE_TEXT)
    PIECE_TEXT[ShoShogiFacetype.DRUNKENELEPHANT] = "酔象"
    PIECE_TEXT[ShoShogiFacetype.CROWNPRINCE]     = "太子"

class EnglishNotation(ShogiGameMN.WesternShogiNotation):
    PieceNotation = EnglishPieceNotation

class SANNotation(ShogiGameMN.BaseSANNotation):
    movename = "SAN move for Sho Shogi"
    PieceNotation = EnglishPieceNotation
    default_piece_type = ShogiFacetype.PAWN

class ShoShogi(RectBoard2PSideGame, ShogiGameWithoutDrops, # type: ignore[misc] # FIXME mypy#14279
               LastKingStanding, OnlySingleBoardGame, OptionPromotionZoneShogiGame):
    Piece = Piece

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth, self.boardheight = 9,9
        self.promotion_rows_count = 3

    # no handicap supported yet
    handicap = ()

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""

        for player in self.players:
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 0, 0),
                                    player, ShogiFacetype.LANCE)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 0),
                                    player, ShogiFacetype.KNIGHT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 2, 0),
                                    player, ShogiFacetype.SILVER)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 3, 0),
                                    player, ShogiFacetype.GOLD)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 4, 0),
                                    player, ShogiFacetype.KING)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 5, 0),
                                    player, ShogiFacetype.GOLD)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 6, 0),
                                    player, ShogiFacetype.SILVER)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 7, 0),
                                    player, ShogiFacetype.KNIGHT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 8, 0),
                                    player, ShogiFacetype.LANCE)

            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 1, 1),
                                    player, ShogiFacetype.BISHOP)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 4, 1), player,
                                    ShoShogiFacetype.DRUNKENELEPHANT)
            self.new_piece_on_board(self.scratch_state,
                                    self.player_relative_pos(player, 7, 1),
                                    player, ShogiFacetype.ROOK)

            for i in range(0, self.boardwidth):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, i, 2),
                                        player, ShogiFacetype.PAWN)

    DefaultMoveNotation = EnglishNotation

    def has_king_status(self, piecestate):
        return piecestate.face in (ShogiFacetype.KING, ShoShogiFacetype.CROWNPRINCE)
