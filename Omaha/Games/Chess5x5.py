# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .Chess import Chess
from .piecetypes.chess import ChessFacetype

class Chess5x5(Chess):
    """Variant of Chess on a 5x5 board."""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.boardwidth, self.boardheight = 5, 5

    def set_start_position(self):
        """Initial game setup.  Not designed to be called twice."""
        for player in self.players:
            self.new_piece_on_board(self.scratch_state,
                                    (0, self.player_relative_row(player, 0)),
                                    player, ChessFacetype.KING)
            self.new_piece_on_board(self.scratch_state,
                                    (1, self.player_relative_row(player, 0)),
                                    player, ChessFacetype.QUEEN)
            self.new_piece_on_board(self.scratch_state,
                                    (2, self.player_relative_row(player, 0)),
                                    player, ChessFacetype.BISHOP)
            self.new_piece_on_board(self.scratch_state,
                                    (3, self.player_relative_row(player, 0)),
                                    player, ChessFacetype.KNIGHT)
            self.new_piece_on_board(self.scratch_state,
                                    (4, self.player_relative_row(player, 0)),
                                    player, ChessFacetype.ROOK)

            for i in range(0, self.boardwidth):
                self.new_piece_on_board(self.scratch_state,
                                        self.player_relative_pos(player, i, 1),
                                        player, ChessFacetype.PAWN)
