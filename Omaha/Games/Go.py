# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract import Euclidian2D
from .abstract.RectBoardGame import (RectBoardGame, OnePiecePerLocRectBoardGame,
                                     RectBoard2PSideGame, NumberRowBTTNotation)
from .abstract.PlayerPools import PlayerPools
from Omaha import Core
from Omaha.Core import InvalidMove, ParseError
import Overlord
import re
from collections import OrderedDict
import logging
from string import ascii_lowercase

logger = logging.getLogger(__name__)

class GoMove(Core.PutOnlyMove):
    special_moves = OrderedDict(Pass = dict(is_pass=True))

    def __init__(self, is_pass=None, **kwargs):
        self.is_pass = is_pass
        super().__init__(**kwargs)
        self.captured_groups = [] # groups captured

        if self.is_pass:
            assert self.target is None, \
                "unexpected target %s" % (self.target,)
    def set_target(self, location):
        if self.is_pass:
            assert location is None
        super().set_target(location)
    @property
    def target(self):
        target = super().target
        if self.is_pass:
            assert target is None
        return target
    def assert_locations(self, holders):
        if self.is_pass:
            return
        super().assert_locations(holders)

class GoLetterColumnLTRNotation(Core.LocationNotation):
    "Letter based notation not using 'i', as used for Go."
    def colname(self, location):
        # allow calling with a column number for recursion
        if isinstance(location, Core.Location):
            col = location.col
        else:
            col = location
        # effective calculation
        if col >= 25:
            l = self.colname(col - 25)
            return 2*l
        if col < 8:
            return ascii_lowercase[col]
        else:
            return ascii_lowercase[col+1]
    def col(self, colname):
        if len(colname) == 2:
            assert colname[0] == colname[1]
            return 25 + self.col(colname[0])
        if colname < "i":
            return ascii_lowercase.index(colname)
        else:
            return ascii_lowercase.index(colname) - 1

# for "a1" style serialization
class GoHybridNotation(Core.MoveNotation):
    class LocationNotation(Euclidian2D.CoordinateNotation,
                           GoLetterColumnLTRNotation, NumberRowBTTNotation):
        "LocationNotation for GoHybridNotation"
        game: RectBoardGame

    def move_serialization(self, move):
        if move.is_pass:
            return "PASS"
        return self.location_notation.location_name(move.target)

    def move_deserialization(self, gamestate, string, player):
        assert gamestate.is_frozen
        if string.lower() == 'pass':
            return self.game.Move(gamestate=gamestate, player=player, is_pass=True)

        m = re.match(r"([a-z]+)(\d+)$", string.lower())
        if m:
            return self.game.Move(gamestate=gamestate, player=player,
                                  target=self.game.board.Location(
                                      self.game.board,
                                      self.location_notation.col(m.group(1)),
                                      self.location_notation.row(m.group(2))))

        raise ParseError("Could not parse '%s' as a serialized go move",
                         string)

class GoPhase(Core.GamePhase):
    Counting = Core.GamePhase("Counting")

class Go(RectBoard2PSideGame,   # type: ignore[misc] # FIXME mypy#14279
         PlayerPools,Core.AlternatingTurnsGame, OnePiecePerLocRectBoardGame):
    COLOR_WHITE = "white"
    COLOR_BLACK = "black"

    Move = GoMove
    DefaultMoveNotation = GoHybridNotation

    player_names = ("Black", "White")

    def __init__(self, **kwargs):
        self.__groups = None # FIXME should move to GameState
        super().__init__(**kwargs)

    def check_playing_phase(self):
        if self.phase is GoPhase.Counting:
            raise InvalidMove("game is over")
        super().check_playing_phase()

    @Overlord.parameter
    def boardsize(cls, context):
        return Overlord.params.Int(label = 'Board size',
                                   default = 19,
                                   minval = 5, maxval = 50)
    @Overlord.parameter
    def komi(cls, context):
        return Overlord.params.Float(label = 'Komi',
                                     default = 6.5)
    @Overlord.parameter
    def handicap(cls, context):
        # FIXME: handicap cannot be 1 - use a choice and minval=2 ?
        return Overlord.params.Int(label = 'Handicap stones',
                                   default = 0,
                                   minval = 0, maxval = 9,
                                   depends_on = dict(
                                       boardsize=cls.__update_handicap))

    @staticmethod
    def __max_handicap(width):
        if width < 7:
            return 0
        if width % 2 == 0 or width == 7:
            return 4
        return 9

    @staticmethod
    def __update_handicap(param,            # boardsize
                          oldvalue,
                          dependant_param): # handicap
        dependant_param.decl.max = Go.__max_handicap(param.value)

    # constraint between several params
    # FIXME: should be a __check_parameters__() instead
    def set_params(self, params):
        super().set_params(params)
        # check handicap validity
        if self.handicap == 1:
            raise Overlord.ParameterError("Minimum handicap is 2")
        if self.handicap > Go.__max_handicap(self.boardsize):
            raise Overlord.ParameterError(
                "Handicap %s > %s too large for board size %s" %
                (self.handicap, Go.__max_handicap(self.boardsize), self.boardsize))
        # propagate changes
        self.boardwidth, self.boardheight = self.boardsize, self.boardsize

    def create_players(self):
        super().create_players()
        self.__groups = { player: [] for player in self.players }

    def setup_players(self):
        super().setup_players()
        self.players[0].color = self.COLOR_BLACK
        self.players[1].color = self.COLOR_WHITE
        if self.handicap > 0:
            self.scratch_state.set_whose_turn(self.players[1])

    def set_start_position(self):
        if self.handicap:
            for l in self.fixed_handicap():
                self.scratch_state.new_piece_at(l, self.players[0], None)

    def supplement_and_check_move(self, gamestate, player, move, anticipate):
        super().supplement_and_check_move(gamestate, player, move, anticipate)

        # player can pass at any time
        if move.is_pass:
            return

        # target location must be free
        if gamestate.piece_at(move.target) is not None:
            raise InvalidMove("target location is occupied")

        nearby = self.__nearby_groups(player, move.target)
        move.connects = nearby[0]

        # check captures
        for group in nearby[1]:
            if self.__liberties_count(gamestate, group) == 1:
                move.captured_groups.append(group)

        ## create/merge groups
        # new group from connecting stone
        move.newgrp = Group(move.target, self)
        # merge any connected groups
        for connected_grp in move.connects:
            move.newgrp.merge_in(connected_grp)

        # FIXME:
        # - check liberties of group
        # - check ko

    def do_move(self, gamestate, player, move):
        if move.is_pass:
            return

        gamestate.new_piece_at(move.target, player, None)

        # update player's groups after connecting
        for connected_grp in move.connects:
            self.__groups[player].remove(connected_grp)
        self.__groups[player].append(move.newgrp)

        # apply captures
        for group in move.captured_groups:
            logger.debug("capturing group of %s stones", len(group))
            for loc in group.locations():
                p = gamestate.piece_at(loc)
                gamestate.take_piece(p)
                pool = self.pool_of(player)
                gamestate.put_piece(pool.Location(pool, None), p)
            self.__groups[player.next].remove(group)

    def postmove_state_update(self, gamestate, move):
        super().postmove_state_update(gamestate, move)
        if move.is_pass and self.moves.last.is_pass:
            self.change_phase(GoPhase.Counting)

    def _undo_move(self, move):
        self.scratch_state.take_piece(self.scratch_state.piece_at(move.target))

        # unapply captures
        for group in move.captured_groups:
            for loc in group.locations():
                pool = self.pool_of(self.scratch_state.whose_turn)
                p = self.scratch_state.piece_at(pool.Location(pool, None))
                self.scratch_state.take_piece(p)
                self.scratch_state.put_piece(loc, p)

            self.__groups[self.scratch_state.whose_turn.next].append(group)

        # restore previous groups state (ie. maybe deconnect)
        for connected_grp in move.connects:
            self.__groups[self.scratch_state.whose_turn].append(connected_grp)
        self.__groups[self.scratch_state.whose_turn].remove(move.newgrp)

    def get_writer_class(self, filename):
        assert filename.endswith(".sgf")
        from Omaha.GameIO.SGFWriter import SGFGameWriter
        return SGFGameWriter

    ## groups/liberties handling

    def __nearby_groups(self, player, location):
        nearby = ([], []) # set of groups for each player

        for group in self.__groups[player]:
            for pos in self.__neighbours(location):
                if pos in group:
                    nearby[0].append(group)
                    break
        for group in self.__groups[player.next]:
            for pos in self.__neighbours(location):
                if pos in group:
                    nearby[1].append(group)
                    break
        return nearby

    def __neighbours(self, location):
        if location.row > 0:
            yield self.board.Location(self.board,
                                      location.col, location.row - 1)
        if location.col > 0:
            yield self.board.Location(self.board,
                                      location.col - 1, location.row)
        if location.col < self.boardwidth - 1:
            yield self.board.Location(self.board,
                                      location.col + 1, location.row)
        if location.row < self.boardheight - 1:
            yield self.board.Location(self.board,
                                      location.col, location.row + 1)

    def __liberties_count(self, gamestate, group):
        empty_neighbours = set()
        for stoneloc in group.locations():
            for neighbour in self.__neighbours(stoneloc):
                if not gamestate.piece_at(neighbour):
                    empty_neighbours.add(neighbour)
        return len(empty_neighbours)

    def hoshi_iter(self):
        "An iterator returning the hoshi positions."
        assert self.boardheight == self.boardwidth
        if self.boardwidth < 13:
            minline, maxline = 2, self.boardwidth - 3
        else:
            minline, maxline = 3, self.boardwidth - 4
        yield self.board.Location(self.board, minline, minline)
        if minline == maxline: # size 5
            return
        yield self.board.Location(self.board, minline, maxline)
        yield self.board.Location(self.board, maxline, minline)
        yield self.board.Location(self.board, maxline, maxline)

        # even sizes and small boards only have the 4 corner ones
        if self.boardwidth % 2 == 0 or self.boardwidth < 9:
            return

        center = self.boardwidth // 2
        yield self.board.Location(self.board, center, center)

        if self.boardwidth < 11:
            return

        yield self.board.Location(self.board, minline, center)
        yield self.board.Location(self.board, center, minline)
        yield self.board.Location(self.board, maxline, center)
        yield self.board.Location(self.board, center, maxline)

    def fixed_handicap(self):
        "An iterator returning the fixed handicap positions."
        if self.handicap < 2:
            return

        assert self.boardheight == self.boardwidth
        if self.boardwidth < 13:
            minline, maxline = 2, self.boardwidth - 3
        else:
            minline, maxline = 3, self.boardwidth - 4

        yield self.board.Location(self.board, minline, minline)
        yield self.board.Location(self.board, maxline, maxline)
        if self.handicap < 3:
            return
        yield self.board.Location(self.board, minline, maxline)
        if self.handicap < 4:
            return
        yield self.board.Location(self.board, maxline, minline)
        if self.handicap < 5:
            return

        # even sizes and small boards have been ruled out in set_params
        assert self.boardwidth % 2 == 1
        center = self.boardwidth // 2

        if self.handicap % 2 == 1:
            yield self.board.Location(self.board, center, center)
        if self.handicap < 6:
            return

        yield self.board.Location(self.board, minline, center)
        yield self.board.Location(self.board, maxline, center)
        if self.handicap < 8:
            return
        yield self.board.Location(self.board, center, minline)
        yield self.board.Location(self.board, center, maxline)

    ###

    has_tooltip = True

    def location_tooltip_text(self, gamestate, loc):
        # nothing to add: we get location name in board and number of
        # captures in pool
        return ""

class Group:
    """A collection of adjacent locations on the boards.

    Stores the set of locations of the group, and provides set
    manipulation on this group.
    """
    def __init__(self, loc=None, game=None):
        self.__locs = set()
        self.__mainloc = None # a human-readable "handle" on the group
        self.__game = game
        if loc is not None:
            self.add(loc)
    def __len__(self):
        return len(self.__locs)
    def __str__(self):
        return self.__game.default_location.location_notation.location_name(self.__mainloc)

    def __contains__(self, loc):
        return loc in self.__locs

    def add(self, loc):
        self.__locs.add(loc)
        if self.__mainloc is None:
            self.__mainloc = loc
    def locations(self):
        for loc in self.__locs:
            yield loc
    def merge_in(self, other):
        for loc in other.locations():
            self.add(loc)

Core.UndoableGame.register(Go)
