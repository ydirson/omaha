# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
import Overlord

class Launcher(Core.Launcher):
    def gameapp_class(self):
        raise NotImplementedError()

    def app(self, context, **kwargs):
        """Return a new GameApp described by `kwargs`.

        If `context` contains has a `game_plugin` attribute, it is
        expected to have as value an `Overlord.Plugin` object with
        plugin_type "X-Omaha-Game"; it will be used to create a new
        game.  If `context` has no such key, the user will be queried
        for a file to load.
        """
        App = self.gameapp_class()
        return App(plugin_manager=self.plugin_manager, toolkit=self.tk,
                   parentcontext=context, **kwargs)

    def _run_app(self, game_plugin):
        context = Core.Context(plugin_manager=self.plugin_manager)
        # ask user for any parameters
        game_params = game_plugin.Class.fresh_parameters_for(context)
        if len(game_params) > 0:
            self.tk.open_params_dialog(
                "omaha - %s parameters" % game_plugin.name, game_params,
                app_ui=self,
                accept_cb=lambda params: self.__setup_game_with_params(game_plugin, context, params),
                parent_plugin=game_plugin)
        else:
            self.__setup_game_with_params(game_plugin, context, game_params)

    def __setup_game_with_params(self, game_plugin, context, params):
        game = game_plugin.Class(parentcontext=context, paramvalues=params)

        match_context = game.context.clone()
        match_context.game_plugin = game_plugin
        match = Core.Match(parentcontext=match_context, game=game)
        match_params = match.fresh_parameters_for(match_context)

        app = self.app(context=match.context, match=match)

        self.tk.open_params_dialog(
            "omaha - player configuration", match_params,
            app_ui=self, accept_cb=lambda params: self.__run_app_with_match_params(app, params),
            parent_plugin=None)

    def __run_app_with_match_params(self, app, params):
        app.match.set_params_using_app(params, app)
        app.game.setup()
        app.run()

    def _loadgame(self):
        self.tk.select_load_file(self.__loadgame_cb)

    def __loadgame_cb(self, filename):
        context = Core.Context(plugin_manager=self.plugin_manager)
        # FIXME: hardcoded, should be pluggable
        if filename.endswith(".sgf"):
            from Omaha.GameIO.SGFReader import SGFGameReader as GameReader
        elif filename.endswith(".pgn"):
            from Omaha.GameIO.PGNReader import PGNGameReader as GameReader
        else:
            raise AssertionError("Unknown file extension for %r" % (filename,))

        try:
            with open(filename) as f:
                reader = GameReader(self.plugin_manager, parentcontext=context)
                try:
                    self.game = reader.parse(f)
                except Core.IncompleteGame as ex:
                    self.tk.notify("error reading game file",
                                   "could not interpret next move: %s" % (ex.error,))
                    self.game = ex.game

                assert isinstance(self.game, Core.Game)
        except Core.UserNotification as ex:
            self.tk.notify(ex.title, ex.message)
            return # parse error, etc

        game = reader.game()

        match_context = game.context.clone()
        match_context.game_plugin = reader.game_plugin()
        match = Core.Match(parentcontext=match_context, game=game)
        match_params = match.fresh_parameters_for(match_context)

        app = self.app(context=match.context, match=match)
        match.set_params_using_app(match_params, app)

        app.run()
