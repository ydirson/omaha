# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from collections import OrderedDict

from Omaha import Core

from Omaha.Toolkits.Qt import Qt as Toolkit
from ..abstract.Launcher import Launcher as AbstractLauncher
import Overlord

import os

import Omaha.Toolkits.Qt
QtCore = Omaha.Toolkits.Qt.QtCore
QtGui = Omaha.Toolkits.Qt.QtGui
QtWidgets = Omaha.Toolkits.Qt.QtWidgets
QAction = Omaha.Toolkits.Qt.QAction

class Launcher(AbstractLauncher):

    def __init__(self, args, **kwargs):
        super(Launcher, self).__init__(toolkit=Toolkit(cli_args=args),
                                       args=args, **kwargs)

    def _create_gui(self):
        self.window = QtWidgets.QMainWindow()
        self.window.setWindowTitle("omaha - launcher")

        # default menus
        self.toolbar = QtWidgets.QToolBar("main toolbar", self.window)
        self.window.addToolBar(QtCore.Qt.TopToolBarArea, self.toolbar)

        self.actionQuit = QAction(QtGui.QIcon.fromTheme("application-exit"),
                                  "&Quit", self.window,
                                  shortcut=QtGui.QKeySequence.Quit,
                                  triggered=Omaha.Toolkits.Qt._app.exit)
        self.toolbar.addAction(self.actionQuit)

        self.actionLoad = QAction(QtGui.QIcon.fromTheme("document-open"),
                                  "&Open", self.window,
                                  triggered=self._loadgame_hook)
        self.toolbar.addAction(self.actionLoad)

        self.actionPlay = QAction(QtGui.QIcon.fromTheme("media-playback-start"),
                                  "&Play", self.window,
                                  triggered=self._playgame_hook)
        self.toolbar.addAction(self.actionPlay)

        # chooser
        self.chooser = QtWidgets.QListWidget()
        self.window.setCentralWidget(self.chooser)
        # hooks index pluginlist by string
        self.pluginlist = OrderedDict(
            (plugin.name, plugin)
            for plugin in sorted(self.plugin_manager.get_plugins_list('X-Omaha-Game'),
                                 key=lambda p: p.name))
        for plugin_name, plugin in self.pluginlist.items():
            item = QtWidgets.QListWidgetItem(plugin_name)
            item.setToolTip(plugin.comment or plugin_name)
            if plugin.icon != '':
                if os.path.exists(plugin.icon):
                    icon = QtGui.QIcon(plugin.icon)
                else:
                    icon = QtGui.QIcon.fromTheme("image-missing")
            else:
                icon = QtGui.QIcon.fromTheme("applications-games")
            item.setIcon(icon)
            self.chooser.addItem(item)
        self.chooser.itemDoubleClicked.connect(self._playgameitem_hook)

        self.window.show()

    def _playgame_hook(self, *args): # FIXME: args==?
        selected = self.chooser.selectedItems()
        if not selected:
            self.tk.notify("error", "No game selected")
            return
        assert len(selected) == 1
        self._run_app(self.pluginlist[selected[0].text()])

    def _loadgame_hook(self):
        self._loadgame()

    def _playgameitem_hook(self, list_item):
        self._run_app(self.pluginlist[list_item.text()])

    def gameapp_class(self, **kwargs):
        from Omaha.Apps.Qt.Play import QtPlay
        return QtPlay
