# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# FIXME: display outcome and disable game controls when Finished

from Omaha import Core
from ..abstract.Play import Play as AbstractPlay
import Overlord

import logging
from gi.repository import Gtk, Gdk, GObject

logger = logging.getLogger(__name__)

class GtkPlay(AbstractPlay):

    def __init__(self, **kwargs):
        self.widgets = {}
        self.__moveentry_callbacks = {}
        self.__connections = []
        super(GtkPlay, self).__init__(**kwargs)

    @property
    def canvas_size(self):
        rect = self.canvas.get_allocation()
        return rect.width, rect.height

    def __add_toolbutton(self, hook, key=None, tooltip=None, **kwargs):
        if 'stock_id' in kwargs:
            assert len(kwargs) == 1
            button = Gtk.ToolButton.new_from_stock(kwargs['stock_id'])
        else:
            button = Gtk.ToolButton.new(**kwargs)
        if tooltip:
            button.set_tooltip_text(tooltip)
        button.connect("clicked", lambda w: hook())
        self.widgets['game_toolbar'].add(button)
        if key:
            self.widgets[key] = button

    def _create_gui(self):
        # The container window and vbox
        self.window = Gtk.Window()
        self.__connections.append(
            (self.window, self.window.connect("destroy", lambda w: self._quit_hook())) )
        game_name = self.context.game_plugin.name
        self.window.set_title("omaha - {0}".format(game_name))
        self.window.set_default_size(800, 600)

        vbox = Gtk.VBox()
        self.window.add(vbox)
        vbox.show()

        icon_theme = Gtk.IconTheme.get_default()

        # A toolbar
        self.widgets['game_toolbar'] = Gtk.Toolbar()
        self.widgets['game_toolbar'].set_style(Gtk.ToolbarStyle.BOTH)
        vbox.pack_start(self.widgets['game_toolbar'], expand=False, fill=True, padding=0)
        icon_size = Gtk.icon_size_lookup(
            self.widgets['game_toolbar'].get_icon_size())[0]
        # tool buttons
        self.__add_toolbutton(self._quit_hook, stock_id=Gtk.STOCK_CLOSE, tooltip="Quit")
        self.__add_toolbutton(self._saveas_hook, stock_id=Gtk.STOCK_SAVE, tooltip="Save")
        self.__add_toolbutton(self._undo_hook, 'undo', tooltip="Undo",
                              stock_id=Gtk.STOCK_UNDO)
        self.__add_toolbutton(self._redo_hook, 'redo', tooltip="Redo",
                              stock_id=Gtk.STOCK_REDO)
        self.__add_toolbutton(
            self._refuse_hook, 'refuse', tooltip="Refuse",
            icon_widget = Gtk.Image.new_from_pixbuf(icon_theme.load_icon(
                "process-stop", icon_size, 0)),
            label="Refuse")
        self.__add_toolbutton(
            self._resign_hook, tooltip="Resign",
            icon_widget = Gtk.Image.new_from_pixbuf(icon_theme.load_icon(
                "edit-delete", icon_size, 0)),
            label="Resign")
        self.__add_toolbutton(self._prefs_hook, tooltip="Preferences",
                              stock_id=Gtk.STOCK_PREFERENCES)

        self.widgets['game_toolbar'].show_all()

        # layout for canvas and history
        hpaned = Gtk.HPaned()
        vbox.add(hpaned)
        hpaned.show_all()

        # hbox for canvas and gameinfo
        hbox = Gtk.HBox(spacing=10)
        hpaned.pack2(hbox, resize=True)
        hbox.show()

        # the canvas
        self.canvas = Gtk.DrawingArea()
        self.canvas.set_size_request(100, 100) # minimum size
        self.canvas.set_events(Gdk.EventMask.BUTTON_PRESS_MASK |
                               Gdk.EventMask.BUTTON_RELEASE_MASK |
                               Gdk.EventMask.POINTER_MOTION_MASK |
                               Gdk.EventMask.POINTER_MOTION_HINT_MASK)
        hbox.pack_start(self.canvas, expand=True, fill=True, padding=0)

        if self.game.has_tooltip:
            self.canvas.set_property('has-tooltip', True)
            self.canvas.connect("query-tooltip", self.set_tooltip)

        self.canvas.show_all()

        # move history
        # FIXME why do we need a dummy bool column?
        self.widgets['history_lstore'] = Gtk.ListStore(bool, str)
        history_treeview = Gtk.TreeView(model=self.widgets['history_lstore'])

        column = Gtk.TreeViewColumn('Move History', Gtk.CellRendererText(), text=1)
        history_treeview.append_column(column)

        hist_scroll = Gtk.ScrolledWindow()
        hist_scroll.add(history_treeview)
        hist_scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.ALWAYS)
        hist_scroll.show_all()
        hpaned.pack1(hist_scroll, resize=False)
        history_treeview.show_all()
        if self.game.moves.nmoves_done:
            self._history_listener(self.game.moves, 0, self.game.moves.nmoves_done)
        self.game.moves.register_listener(self._history_listener)

        # gameinfo panel if GameRenderer needs it
        if not self.game_renderer.handles_player_display:
            infopanel = Gtk.VBox()
            radiogroup_ref = None # for radiobutton grouping
            for player in self.game.players:
                playerbox = Gtk.HBox(spacing=10)
                if radiogroup_ref:
                    turnindicator = Gtk.RadioButton.new_from_widget(radiogroup_ref)
                else:
                    turnindicator = Gtk.RadioButton()
                    radiogroup_ref = turnindicator
                turnindicator.set_sensitive(False)
                playerbox.pack_start(turnindicator, expand=False, fill=True, padding=0)
                self.widgets['playerturn_' + player.name] = turnindicator

                genericname = Gtk.Label()
                genericname.set_markup("<b><big>%s</big></b>" % player.name)
                playerbox.pack_start(genericname, expand=False, fill=True, padding=0)

                playername = Gtk.Label()
                playerbox.pack_start(playername, expand=False, fill=True, padding=0)
                self.widgets['playername_' + player.name] = playername

                drivertype = Gtk.Label()
                playerbox.pack_end(drivertype, expand=False, fill=True, padding=10)
                self.widgets['playerdrivertype_' + player.name] = drivertype

                infopanel.pack_start(playerbox, expand=False, fill=True, padding=5)

                drv = self.match._player_drivers[player]
                drvcls_param = self.match._playerdrivers_classes[player]
                assert drvcls_param.value.Class is type(drv), \
                    "%s is not %s" % (drvcls_param.value.Class, type(drv))
                self.fill_from_playerdriver(player, drv)

            hbox.pack_start(infopanel, expand=False, fill=True, padding=0)
            infopanel.show_all()

        # textfield for manually-enterered moves (not shown initially)
        self.widgets['moveentry_hbox'] = Gtk.HBox()
        self.widgets['moveentry_hbox'].pack_start(Gtk.Label(label="Move:"),
                                                  expand=False, fill=True, padding=0)
        self.widgets['move_entry'] = Gtk.Entry()
        self.widgets['moveentry_hbox'].pack_start(self.widgets['move_entry'],
                                                  expand=True, fill=True, padding=0)
        vbox.pack_end(self.widgets['moveentry_hbox'], expand=False, fill=True, padding=0)

        self.window.show()
        hpaned.set_position(200)

        self.__connections.append(
            (self.canvas,
             self.canvas.connect("draw",
                                 lambda w, evt: self.game_renderer.draw(
                                     self.canvas.get_window().cairo_create(),
                                     self.game.scratch_state))) )
        self.tk.invalidate_canvas(self.canvas)

    def release_backrefs(self):
        self.game.moves.unregister_listener(self._history_listener)
        for widget, connection in self.__connections:
            widget.disconnect(connection)
        super(GtkPlay, self).release_backrefs()

    def set_player_turn(self, player):
        if self.game_renderer.handles_player_display or not self.widgets:
            return              # UI not created when parameter is first set
        playerturn_radio = self.widgets['playerturn_' + player.name]
        playerturn_radio.set_active(True)

    def set_playerdriver_type(self, player, driver_text):
        if self.game_renderer.handles_player_display or not self.widgets:
            return              # UI not created when parameter is first set
        drivertype_label = self.widgets['playerdrivertype_' + player.name]
        drivertype_label.set_text(driver_text)

    def set_playerdriver_name(self, player, name):
        if self.game_renderer.handles_player_display or not self.widgets:
            return              # UI not created when parameter is first set
        playername_label = self.widgets['playername_' + player.name]
        playername_label.set_text(name)

    def _add_game_action(self, label, callback):
        button = Gtk.ToolButton(label=label)
        handle = button.connect("clicked", lambda w: callback())
        self.widgets['game_toolbar'].add(button)
        button.show_all()
        return (button, handle)

    def _remove_game_action(self, game_action):
        button, handle = game_action
        self.widgets['game_toolbar'].remove(button)
        button.disconnect(handle)
        button.destroy()

    @property
    def main_window(self):
        return self.window

    ###

    def _enable_undo(self):
        for btn_name in ('undo', 'redo', 'refuse'):
            self.widgets[btn_name].set_sensitive(True)
    def _disable_undo(self):
        for btn_name in ('undo', 'redo', 'refuse'):
            self.widgets[btn_name].set_sensitive(False)

    ###

    def ack_moveentry(self):
        # stop propagation of click event (to other MouseChesslikePlayer's)
        self.widgets['move_entry'].stop_emission("activate")

    def set_manual_move_entry_callback(self, callback):
        self.__moveentry_callbacks[callback] = \
            self.widgets['move_entry'].connect("activate",
                                               lambda w: callback(w.get_text()))
        self.widgets['moveentry_hbox'].show_all()
    def unset_manual_move_entry_callback(self, callback):
        self.widgets['move_entry'].disconnect(
            self.__moveentry_callbacks[callback])
        del self.__moveentry_callbacks[callback]
        if len(self.__moveentry_callbacks) == 0:
            self.widgets['moveentry_hbox'].hide()
    def clear_manual_move_entry(self):
        self.widgets['move_entry'].set_text('')

    ###

    def set_tooltip(self, widget, x, y, keyboard_mode, tooltip):
        assert widget is self.canvas
        # FIXME: return early if recompute_areas was not called yet,
        # or presence of pointer in game window when loading game
        # triggers (harmless) exception
        location = self.game_renderer.point_to_location(x, y)
        text = self.game_renderer.location_tooltip_text(self.game.scratch_state, location)
        if not text:
            return False
        tooltip.set_text(text)
        return True

    ###

    def _history_append(self, movestr):
        self.widgets['history_lstore'].append([False, movestr])

    def _history_size(self):
        if 'history_lstore' not in self.widgets:
            return 0
        return self.widgets['history_lstore'].iter_n_children(None)

    def _history_delete(self, first, last):
        it = self.widgets['history_lstore'].get_iter(first)
        for i in range(first, last):
            if not self.widgets['history_lstore'].remove(it):
                if i < last -1:
                    logger.error("_history_delete: iterator finished to soon")
