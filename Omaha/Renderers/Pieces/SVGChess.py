# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.StackedVector import StackedVectorPieceRenderer, svgLayer
from Omaha.Games.piecetypes.chess import ChessFacetype

class SVGChessPieceRenderer(StackedVectorPieceRenderer):

    def _piece_key(self, gamestate, piece):
        return (self.gui.game.players.index(gamestate.piece_owner(piece)),
                gamestate.piece_state(piece).face)

    # colors 3 and 4 are named Yellow and Red, an arbitrary choice for
    # ForChess colors, but could be anything else
    filename_start = [ "w", "b", "y", "r" ]
    filename_end = { ChessFacetype.KING: "k",
                     ChessFacetype.QUEEN: "q",
                     ChessFacetype.BISHOP: "b",
                     ChessFacetype.KNIGHT: "n",
                     ChessFacetype.ROOK: "r",
                     ChessFacetype.PAWN: "p" }

    # skin interface definition for Chess
    def skin_schema(self, piece_key):
        piece_playernum, piece_type = piece_key
        return ( (svgLayer, (0, 0, 100, 100,
                             self.filename_start[piece_playernum] +
                             self.filename_end[piece_type])) ,)
