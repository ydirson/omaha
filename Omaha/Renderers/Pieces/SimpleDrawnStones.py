# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import itertools
import logging

from Omaha import Core
import Overlord
from Overlord.misc import pretty

logger = logging.getLogger(__name__)

class SimpleDrawnStones(Core.PieceRenderer):
    def __init__(self, **kwargs):
        super(SimpleDrawnStones, self).__init__(**kwargs)
        self.radius = {}

    def set_piece_size(self, set_id, newsize, angle=0):
        assert angle == 0
        self.radius[set_id] = int(newsize // 2)

    def draw_piece(self, drawing_context, gamestate, center, set_id, piece):
        center_x, center_y = center
        owner = gamestate.piece_owner(piece)
        offset = 3 * gamestate.contents_at(gamestate.piece_location(piece)).pieces.index(piece)
        game_renderer = self.gui.game_renderer
        assert isinstance(game_renderer, Core.ColoredPlayersGameRenderer)
        self.gui.tk.fill_circle(drawing_context,
                                center_x + offset, center_y - offset, self.radius[set_id],
                                fillpattern=self.gui.tk.SolidFillPattern(
                                    self.gui.tk.color(self.gui.canvas,
                                                      game_renderer.colornames[owner])))
