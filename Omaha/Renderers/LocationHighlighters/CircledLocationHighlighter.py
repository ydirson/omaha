# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

class CircledLocationHighlighter(Core.LocationHighlighter):
    def __init__(self, game_renderer,
                 colorname="black", percent_radius=90):
        self.__game_renderer = game_renderer
        self.__color = game_renderer.gui.tk.color(self.__game_renderer.gui.canvas, colorname)
        self.__percent_radius = percent_radius

    def highlight_location(self, drawing_context, location):
        gr = self.__game_renderer.holder_renderers[location.holder]
        xcenter, ycenter = gr.tile_center(location)
        self.__game_renderer.gui.tk.draw_circle(
            drawing_context,
            xcenter, ycenter,
            int(gr.tile_inscribed_radius * self.__percent_radius / 100),
            color=self.__color, width=2)
