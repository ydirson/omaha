# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core

class MoveTargetHighlighter(Core.MoveHighlighter):
    """A move highligter that just highlights target location.

    LocationHighlighter for target ought to be a param, but
    highlighters are no real plugins yet, so better report the
    hardcoded stuff to caller :).
    """
    def __init__(self, game_renderer, lochl_cls, **args):
        self.__loc_highlighter = lochl_cls(game_renderer, **args)

    def highlight_move(self, drawing_context, move):
        if move.target:
            self.__loc_highlighter.highlight_location(drawing_context, move.target)
