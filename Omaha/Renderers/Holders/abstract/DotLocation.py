# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .Euclidian2D import Euclidian2DRendererWithLocation

class DotLocation(Euclidian2DRendererWithLocation):
    """
    Generic class for showing locations using dots.
    """

    def draw(self, drawing_context, rectangle, gamestate):
        super(DotLocation, self).draw(drawing_context, rectangle, gamestate)

        dotsize = max(1, min(int(self.delta_x), int(self.delta_y)) // 10)
        fillpattern=self.gui.tk.SolidFillPattern(self.gui.tk.color(self.gui.canvas, "black"))
        for loc in gamestate.all_locations(self.holder):
            x, y = self.tile_center(loc)
            self.gui.tk.fill_circle(drawing_context, x, y, dotsize, fillpattern=fillpattern)

    def tile_center(self, location):
        raise NotImplementedError()
