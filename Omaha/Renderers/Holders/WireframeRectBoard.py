# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.RectBoard import RectBoard
from Omaha import Core

class WireframeRectBoard(RectBoard):

    halfwidth = 1

    def draw(self, drawing_context, rectangle, gamestate):
        """Draw board within specified rectangle of self.gui."""

        super(WireframeRectBoard, self).draw(drawing_context, rectangle, gamestate)

        black = self.gui.tk.color(self.gui.canvas, "black")
        self.gui.tk.draw_rectangle(drawing_context,
                                   self.board_x0, self.board_y0,
                                   self.board_xn - self.board_x0,
                                   self.board_yn - self.board_y0,
                                   color=black, width=2*self.halfwidth)
        for i in range(1, self.holder.matrix_width):
            x = self.tile_corner_x(i)
            self.gui.tk.draw_line(drawing_context,
                                  x, self.board_y0, x, self.board_yn,
                                  color=black, width=2*self.halfwidth)
        for i in range(1, self.holder.matrix_height):
            y = self.tile_corner_y(i)
            self.gui.tk.draw_line(drawing_context,
                                  self.board_x0, y, self.board_xn, y,
                                  color=black, width=2*self.halfwidth)

    def tile_square(self, location):
        r = super(WireframeRectBoard, self).tile_square(location)
        return Core.Rectangle(r.x + self.halfwidth,
                              r.y + self.halfwidth,
                              r.width - 2 * self.halfwidth,
                              r.height - 2 * self.halfwidth)
