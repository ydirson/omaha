# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from Omaha import Core
from Overlord.misc import class_fullname

class SingleBoardGameRenderer(Core.HolderDelegatingGameRenderer):
    """
    Base class for Renderers' whose main holder is a single board.

    Note that additionnal holders may be handled by subclasses.
    """
    def __init__(self, **kwargs):
        super(SingleBoardGameRenderer, self).__init__(**kwargs)
        self.areas = {}

        self.__location_highlighters = None
        self.__move_highlighters = None
        self.__highlights = {
            Core.LocHighlights.COMPOSING: [],
            Core.LocHighlights.MAYBE: [],
            Core.LocHighlights.REACHABLE: [],
            Core.MoveHighlights.LASTMOVES: [],
            }
        self.__highlight_order = (
            Core.LocHighlights.REACHABLE,
            Core.LocHighlights.COMPOSING, Core.LocHighlights.MAYBE)
        self.game.register_listener(self.__update_recent_moves)

    def release_backrefs(self):
        self.game.unregister_listener(self.__update_recent_moves)
        super(SingleBoardGameRenderer, self).release_backrefs()

    def __init_highlighters(self):
        # select location highlighter according to board renderer
        # FIXME: needs to be pluggable
        # FIXME: not specific to single-board
        self.__location_highlighters = {}
        self.__move_highlighters = {}
        if class_fullname(type(self.game)).startswith('Omaha.Games.CantStop.Game'):
            # no highlight
            return
        for ancestor in type(self.board_renderer()).mro():
            if  (class_fullname(ancestor) == 'Omaha.Renderers.Holders.'
                 'CheckeredRectBoard.CheckeredRectBoard'):
                from ..LocationHighlighters.SimpleSquareHighlighter \
                    import SimpleSquareHighlighter
                self.__location_highlighters[Core.LocHighlights.COMPOSING] = \
                    SimpleSquareHighlighter(self, colorname="lightblue")
                self.__location_highlighters[Core.LocHighlights.MAYBE] = \
                    SimpleSquareHighlighter(self, colorname="lightgreen")
                self.__location_highlighters[Core.LocHighlights.REACHABLE] = \
                    SimpleSquareHighlighter(self, colorname="green")
                from ..MoveHighlighters.MoveTargetHighlighter \
                    import MoveTargetHighlighter
                self.__move_highlighters[Core.MoveHighlights.LASTMOVES] = \
                    MoveTargetHighlighter(self, SimpleSquareHighlighter,
                                          colorname="blue")
                break
            if  (class_fullname(ancestor) == 'Omaha.Renderers.Holders.'
                 'WireframeRectBoard.WireframeRectBoard'):
                from ..LocationHighlighters.ColorSquareHighlighter \
                    import ColorSquareHighlighter
                self.__location_highlighters[Core.LocHighlights.COMPOSING] = \
                    ColorSquareHighlighter(self)
                self.__location_highlighters[Core.LocHighlights.MAYBE] = \
                    ColorSquareHighlighter(self, colorname="#ddffdd")
                self.__location_highlighters[Core.LocHighlights.REACHABLE] = \
                    ColorSquareHighlighter(self, colorname="lightgreen")
                from ..MoveHighlighters.MoveTargetHighlighter \
                    import MoveTargetHighlighter
                self.__move_highlighters[Core.MoveHighlights.LASTMOVES] = \
                    MoveTargetHighlighter(self, ColorSquareHighlighter,
                                          colorname="lightblue")
                break
            if  (class_fullname(ancestor) == 'Omaha.Renderers.Holders.'
                 'GoBan.GoBan'):
                from ..LocationHighlighters.CircledLocationHighlighter \
                    import CircledLocationHighlighter
                from ..LocationHighlighters.CrossHairHighlighter \
                    import CrossHairHighlighter
                self.__location_highlighters[Core.LocHighlights.COMPOSING] = \
                    CircledLocationHighlighter(self, colorname="black")
                self.__location_highlighters[Core.LocHighlights.MAYBE] = \
                    CrossHairHighlighter(self, colorname="lightgreen")
                self.__location_highlighters[Core.LocHighlights.REACHABLE] = \
                    CircledLocationHighlighter(self, colorname="green")
                from ..MoveHighlighters.MoveTargetHighlighter \
                    import MoveTargetHighlighter
                self.__move_highlighters[Core.MoveHighlights.LASTMOVES] = \
                    MoveTargetHighlighter(self, CircledLocationHighlighter,
                                          colorname="blue")
                break
            # fallback
            if  (class_fullname(ancestor) == 'Omaha.Renderers.Holders.'
                 'abstract.Euclidian2D.Euclidian2DRenderer'):
                from ..LocationHighlighters.CircledLocationHighlighter \
                    import CircledLocationHighlighter
                self.__location_highlighters[Core.LocHighlights.COMPOSING] = \
                    CircledLocationHighlighter(self, colorname="black")
                self.__location_highlighters[Core.LocHighlights.MAYBE] = \
                    CircledLocationHighlighter(self, colorname="lightgreen")
                self.__location_highlighters[Core.LocHighlights.REACHABLE] = \
                    CircledLocationHighlighter(self, colorname="green")
                from ..MoveHighlighters.MoveTargetHighlighter \
                    import MoveTargetHighlighter
                self.__move_highlighters[Core.MoveHighlights.LASTMOVES] = \
                    MoveTargetHighlighter(self, CircledLocationHighlighter,
                                          colorname="blue")
                break

    def __update_recent_moves(self, event):
        # we only keep track of a single move for now
        if isinstance(event, Core.Game.MoveEvent):
            self.clear_highlights(Core.MoveHighlights.LASTMOVES)
            self.__do_update_recent_moves([event.move])
            return
        if isinstance(event, Core.Game.UndoEvent):
            self.clear_highlights(Core.MoveHighlights.LASTMOVES)
            if event.game.moves.nmoves_done > 0:
                self.__do_update_recent_moves([event.game.moves.last])
            return
    def __do_update_recent_moves(self, moves):
        self.add_highlights(Core.MoveHighlights.LASTMOVES, moves)

    @property
    def margin(self):
        return 10

    @property
    def available_area_for_board(self):
        return (self.margin, self.margin,
                self.gui.canvas_size[0] - 2 * self.margin,
                self.gui.canvas_size[1] - 2 * self.margin)

    def board_renderer(self):
        return self.holder_renderers[self.game.board]

    def recompute_areas(self):
        avail_x0, avail_y0, avail_width, avail_height = self.available_area_for_board
        self.current_board_width = min(avail_width,
                                       int(avail_height / self.board_renderer().onscreen_ratio))
        self.current_board_height = \
            int(self.current_board_width * self.board_renderer().onscreen_ratio)
        self.areas[self.game.board] = \
            Core.Rectangle(avail_x0, avail_y0,
                           self.current_board_width, self.current_board_height)

    # FIXME: make more generic, could support more than 1 board
    def point_to_location(self, x, y):
        if self.game.board not in self.areas:
            # Return early if recompute_areas was not called yet, or
            # presence of pointer in game window when loading game
            return None
        area = self.areas[self.game.board]
        if area.contains(x, y):
            return self.board_renderer().point_to_location(x, y)

    def clear_highlights(self, mode):
        if mode in self.__highlights:
            self.__highlights[mode] = []
    def add_highlights(self, mode, items):
        if mode in self.__highlights:
            self.__highlights[mode].extend(items)
    def highlights(self, mode):
        if mode in self.__highlights:
            return tuple(self.__highlights[mode])
        else:
            return ()

    def draw_holders(self, drawing_context, gamestate):
        super().draw_holders(drawing_context, gamestate)
        self.recompute_areas()

        # draw board itself
        # FIXME should be done from HolderDelegatingGameRenderer, for all
        self.board_renderer().draw(drawing_context, self.areas[self.game.board], gamestate)

        if self.__location_highlighters is None:
            self.__init_highlighters()

        # highlighted locations
        for hl_type in ([ t for t in self.__highlight_order
                          if t in self.__location_highlighters.keys() ] +
                        [ t for t in  self.__location_highlighters.keys()
                          if t not in self.__highlight_order ]):
            highlighter = self.__location_highlighters[hl_type]
            if hl_type in self.__highlights:
                for location in self.__highlights[hl_type]:
                    highlighter.highlight_location(drawing_context, location)
        # highlighted moves
        for hl_type, highlighter in self.__move_highlighters.items():
            if hl_type in self.__highlights:
                for move in self.__highlights[hl_type]:
                    highlighter.highlight_move(drawing_context, move)
