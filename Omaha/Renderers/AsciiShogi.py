# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from .abstract.SingleBoardAndPools import SingleBoardAndPools
from .AsciiChess import AsciiChessRenderer
from ..Games.MiniShogi import MiniShogi

class AsciiShogiRenderer(SingleBoardAndPools, AsciiChessRenderer):
    MARGIN = 20 # percent

    def __init__(self, **kwargs):
        super(AsciiShogiRenderer, self).__init__(**kwargs)
        # FIXME: dependant on square-size
        self.pool_height = 20

    def _draw_piece(self, drawing_context, gamestate, piece):
        piece_owner = gamestate.piece_owner(piece)
        attrs = {}
        if piece_owner.color is MiniShogi.SIDE_SENTE:
            attrs["bold"] = True

        if gamestate.piece_state(piece).promoted:
            attrs["color"] = self.gui.tk.color(self.gui.canvas, "red")

        xcenter, ycenter = self.holder_renderers[gamestate.piece_holder(piece)]. \
                           tile_center(piece.location(gamestate))
        xcenter, ycenter = int(xcenter), int(ycenter)
        self.gui.tk.draw_centered_text(
            drawing_context,
            xcenter, ycenter,
            self.game.default_notation.piece_notation.piece_name(gamestate.piece_state(piece)),
            **attrs)

        # draw piece outline
        half_square_size = int(
            self.holder_renderers[self.game.board].delta_x / 2 *
            (100 - self.MARGIN)) // 100
        slant_offset = half_square_size * 3 // 10
        self.gui.tk.draw_polygon(
            drawing_context,
            (   # bottom left
                (xcenter - half_square_size,
                 ycenter + self.game.player_orientation(piece_owner)[1] * half_square_size),
                # top left
                (xcenter - (half_square_size - slant_offset),
                 ycenter - self.game.player_orientation(piece_owner)[1] * (half_square_size -
                                                                           slant_offset)),
                # top wedge
                (xcenter,
                 ycenter - self.game.player_orientation(piece_owner)[1] * half_square_size),
                # top right
                (xcenter + (half_square_size - slant_offset),
                 ycenter - self.game.player_orientation(piece_owner)[1] * (half_square_size -
                                                                           slant_offset)),
                # bottom right
                (xcenter + half_square_size,
                 ycenter + self.game.player_orientation(piece_owner)[1] * half_square_size),
                ),
            color=self.gui.tk.color(self.gui.canvas, "black"))
