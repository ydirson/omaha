# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

from typing import TYPE_CHECKING, Any

import Overlord
from Overlord.misc import classproperty

from .game import Game
from .notations import PieceNotation

if TYPE_CHECKING:
    from .context import Context
    from .game import GameState
    from .game_app import GameApp
    from .piece import Piece
    from .UI import DrawingContext

__all__ = [ 'PieceRenderer', 'NotationBasedPieceRenderer' ]

class PieceRenderer(Overlord.Parametrizable):
    def __init__(self, gui: GameApp, **kwargs: Any):
        super().__init__(**kwargs)
        self.__gui = gui
    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return PieceRenderer
    @property
    def gui(self) -> GameApp:
        return self.__gui
    def set_piece_size(self, set_id: object, newsize: float, angle: float=0) -> None:
        raise NotImplementedError()
    def draw_piece(self, drawing_context: DrawingContext, gamestate: GameState,
                   center: tuple[int,int], set_id: object, piece: Piece) -> None:
        raise NotImplementedError()

    def clone(self) -> PieceRenderer:
        # prevent calling of Parametrizable.clone()
        raise SyntaxError("clone() is not supported in PieceRenderer")

class NotationBasedPieceRenderer(PieceRenderer):
    # pylint: disable=abstract-method

    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
        self.__notation: PieceNotation

    @Overlord.parameter
    def notation_class(cls, context: Context) -> Overlord.params.Plugin:
        game_plugin = context.game_plugin
        assert isinstance(game_plugin, Overlord.Plugin)
        game_class = game_plugin.Class
        assert issubclass(game_class, Game)
        if game_plugin.has_datafield("X-Omaha-DefaultPieceNotation"):
            default_notation = game_plugin.datafield("X-Omaha-DefaultPieceNotation")
#            logger.info("Using %s as requested by %s",
#                        default_renderer, game_plugin.name)
        else:
            default_notation = None
        return Overlord.params.Plugin(
            label = 'Character set',
            plugintype = 'X-Omaha-PieceNotation',
            context = context,
            pattern = Overlord.plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_class),
            default_by_name = default_notation,
            )

    @property
    def notation(self) -> PieceNotation:
        notationcls = self.notation_class.Class
        # (re)instanciate notation if required
        if not hasattr(self, "__notation") or not isinstance(self.__notation, notationcls):
            self.__notation = notationcls(self.gui.game)
        assert isinstance(self.__notation, PieceNotation)
        return self.__notation
