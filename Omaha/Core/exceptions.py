# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from typing import TYPE_CHECKING, Any

import Overlord
if TYPE_CHECKING:
    from .game import Game

class MessageException(Exception):
    def __init__(self, message: str):
        assert isinstance(message, str)
        self.__message = message
    @property
    def message(self) -> str:
        return self.__message
    def __str__(self) -> str:
        return self.message

class OmahaException(MessageException):
    """Base class for all Omaha exceptions."""

class InvalidMove(OmahaException):
    pass

class GameSuspended(OmahaException):
    "Move was emited while game is suspended (must undo)"
    def __init__(self) -> None:
        super().__init__("Game is suspended")

class NotMyTurn(OmahaException):
    pass

class IncompleteMove(OmahaException):
    """Signals a move that requires more information to be performed."""
    def __init__(self, paramlist: dict[str,Overlord.ParamDecl[Any]]):
        """Records a dict of Param instances for unspecified aspects of the move."""
        super().__init__("incomplete move")
        self.paramlist = paramlist

class ParseError(OmahaException):
    pass

class IncompleteGame(OmahaException):
    "Signals a game that failed to completely load"
    def __init__(self, game: "Game", error: str):
        super().__init__(error)
        self.game, self.error = game, error

class UserCancel(OmahaException):
    def __init__(self) -> None:
        super().__init__("Cancelled by user")

class UnsatisfiableConstraint(MessageException):
    """A parameter's constraints left no available choices."""

class UserNotification(OmahaException):
    """Exception that should be shown to user."""
    def __init__(self, title: str, text: str):
        super().__init__(text)
        self.title = "omaha - " + title
class UserErrorNotification(UserNotification):
    "Convenience subclass of `UserNotification` for errors."
    def __init__(self, text: str):
        UserNotification.__init__(self, "error", text)
