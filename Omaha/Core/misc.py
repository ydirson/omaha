# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import logging
import subprocess
from typing import Sequence

__all__: Sequence[str] = ()

###

def log_stack_trace(logger: logging.Logger) -> None:
    import traceback
    logger.warn(''.join(traceback.format_stack()))

def sign(n: int) -> int:
    """Return 0, 1 or -1 depending on the sign of n."""
    if n == 0: return 0
    if n < 0: return -1
    return 1

def safe_popen(program: str) -> subprocess.Popen[str]:
    try:
        process = subprocess.Popen(
            program,
            shell=True,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            bufsize=0,
            universal_newlines=True)
    except OSError as ex:
        # in python3, only catches missing shell
        raise RuntimeError("could not exec %s: %s" %
                           (program, ex))
    # workaround https://github.com/python/cpython/issues/85394
    assert process.stdout
    process.stdout._CHUNK_SIZE = 1 # type: ignore[attr-defined]

    return process

# FIXME: this implementation seems rather inefficient, and only
# supports group size of 2
def dice_combinations(values: tuple[int, ...],
                      group_size: int) -> set[tuple[tuple[int, ...], ...]]:
    """Enumerate all ways of grouping elements of `values` in groups of `group_size`."""
    assert group_size == 2 # limitation of current algorithm
    assert len(values) % group_size == 0
    assert values == tuple(sorted(values))
    if len(values) == group_size:
        return {(values,)}
    die1 = values[0] # always part of a group, no need to iterate
    remaining1 = values[1:]
    ret = set()
    for die2 in sorted(set(remaining1)):
        index2 = remaining1.index(die2)
        remaining2 = remaining1[:index2] + remaining1[index2+1:]

        #INDENT = " " * (6 - len(values))
        #print(INDENT, "RECURSE", die1, die2, ":", remaining2)
        remaining_combinations = dice_combinations(remaining2, group_size)
        #print(INDENT, "COMBOS", remaining_combinations)
        product = set((tuple(sorted(((die1, die2),) + combo)))
                      for combo in remaining_combinations)
        #print(INDENT, "PROD", product)
        ret.update(product)
    return ret
