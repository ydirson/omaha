# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

import itertools
import logging
from typing import (TYPE_CHECKING, overload, Any, Generic, Iterable,
                    Optional, Sequence, TypeVar, Union)

import Overlord
from Overlord.misc import class_fullname, classproperty
from Overlord import plugin_pattern

from .context import Context
from .game import Game, GameState
from .moves import Move
from .piece import Piece
from .piece_holder import PieceHolder, Location
from .UI import DrawingContext

# imports for GameRenderer subclasses
from .holder_renderer import HolderRenderer, LocHighlights
from .piece_renderer import PieceRenderer

if TYPE_CHECKING:
    from .game_app import GameApp
    from .player import Player

__all__ = [ 'GameRenderer', 'HolderDelegatingGameRenderer',
            'PieceDelegatingGameRenderer', 'SinglePieceSetGameRenderer',
            'ColoredPlayersGameRenderer',
            'MoveHighlights' ]

logger = logging.getLogger(__name__)

class GameRenderer(Overlord.Parametrizable):
    """
    Base class for game renderers.

    Used as hierarchy root for proper call chaining of __init__ and
    other methods, and for checking by introspection.
    """

    # whether this GameRenderer class shows player/turn (else relies on Play App)
    handles_player_display = False

    def __init__(self, game: Game, gui: GameApp, **kwargs: Any):
        super().__init__(**kwargs)
        self.game = game
        self.gui = gui

    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return GameRenderer

    @overload
    def clear_highlights(self, mode: LocHighlights) -> None:
        pass
    @overload
    def clear_highlights(self, mode: MoveHighlights) -> None:
        pass
    def clear_highlights(self, mode):
        """
        Deregister all moves/locations for highlighting with specified mode.

        Subclasses are not required to support this mechanism, but
        can do so by overloading this method.
        """
    @overload
    def add_highlights(self, mode: LocHighlights, items: Sequence[Location]) -> None:
        pass
    @overload
    def add_highlights(self, mode: MoveHighlights, items: Sequence[Move]) -> None:
        pass
    def add_highlights(self, mode, items):
        """
        Register moves/locations for highlighting with specified mode.

        Subclasses are not required to support this mechanism, but
        can do so by overloading this method.
        """
    @overload
    def highlights(self, mode: LocHighlights) -> tuple[Location, ...]:
        pass
    @overload
    def highlights(self, mode: MoveHighlights) -> tuple[Move, ...]:
        pass
    def highlights(self, mode):
        """
        Return a tuple of the moves/locations for specified highlighting mode.
        """

    def draw(self, drawing_context: DrawingContext, gamestate: GameState) -> None:
        self.gui.tk.clear(drawing_context)
        self.draw_holders(drawing_context, gamestate)
        self.draw_pieces(drawing_context, gamestate)

    def draw_holders(self, drawing_context: DrawingContext, gamestate: GameState) -> None:
        pass

    def draw_pieces(self, drawing_context: DrawingContext, gamestate: GameState) -> None:
        for piece in gamestate.pieces:
            if piece.location(gamestate) is not None:
                self._draw_piece(drawing_context, gamestate, piece)

    def _draw_piece(self, drawing_context: DrawingContext,
                    gamestate: GameState, piece: Piece) -> None:
        raise NotImplementedError()

    def point_to_location(self, x: int, y: int) -> Location:
        raise NotImplementedError()

    def point_to_move_args(self, x: int, y: int) -> Optional[dict[str,Any]]:
        "Return arguments for a meaningful game.Move() call at point (x,y)"
        return None

    def location_tooltip_text(self, gamestate: GameState, loc: Location) -> str:
        try:
            return self.game.location_tooltip_text(gamestate, loc)
        except IndexError:
            return ""

    def clone(self) -> GameRenderer:
        # prevent calling of Parametrizable.clone()
        raise SyntaxError("clone() is not supported in GameRenderer")

class HolderDelegatingGameRenderer(GameRenderer):
    # pylint: disable=abstract-method
    """
    Game renderer delegating rendering of holders to HolderRenderers.
    """

    __default_renderers: dict[tuple[type[Game],type[PieceHolder]],Optional[str]] = {}

    @Overlord.parameter
    def background_color(cls, context: Context) -> Overlord.params.String:
        return Overlord.params.String(label = 'Background color',
                                      default = "#005500")
    @background_color.xformer   # type: ignore[no-redef]
    def background_color(self, value: str) -> object:
        return self.gui.tk.color(self.gui.canvas, value)

    def draw_holders(self, drawing_context: DrawingContext, gamestate: GameState) -> None:
        super().draw_holders(drawing_context, gamestate)
        self.gui.tk.fill_rectangle(
            drawing_context,
            0, 0, self.gui.canvas_size[0], self.gui.canvas_size[1],
            fillpattern=self.gui.tk.SolidFillPattern(self.background_color))

    @classmethod
    def __init_default_renderer(cls, context: Context,
                                game_class: type[Game], holder_class: type[PieceHolder]) -> None:
        # Look for a default HolderRenderer declaration matching this holder
        # 1. in the Game decl
        for ancestor in holder_class.mro():
            game_plugin = context.game_plugin
            default = game_plugin.datafield("X-Omaha-DefaultRenderer-%s" % ancestor.__name__)
            if default != '':
                logger.info("Using %s as requested by %s", default, class_fullname(game_class))
                break
        else:
            logger.info("No matching X-Omaha-DefaultRenderer-* for %s", class_fullname(cls))

            # 2. in the GameRenderer decl as fallback
            for ancestor in holder_class.mro():
                default = getattr(cls, 'x-plugin-desc'). \
                          datafield("X-Omaha-DefaultRenderer-%s" % ancestor.__name__)
                if default != '':
                    logger.info("Using %s as requested by %s", default, class_fullname(cls))
                    break
            else:
                logger.info("No matching X-Omaha-DefaultRenderer-* for %s", class_fullname(cls))
                default = None
        cls.__default_renderers[(game_class, holder_class)] = default

    @classmethod
    def parameters_dcl(cls, context: Context) -> dict[str,Overlord.ParamDecl[Any]]:
        p = dict(super().parameters_dcl(context))
        game_plugin = context.game_plugin
        game_class = game_plugin.Class
        assert issubclass(game_class, Game)
        holder_types = context.holder_types
        for holder_class in holder_types:
            paramid = "HolderRenderer-" + class_fullname(holder_class)
            assert paramid not in p
            if (game_class, holder_class) not in cls.__default_renderers:
                cls.__init_default_renderer(context, game_class, holder_class)
            p[paramid] = Overlord.params.Plugin(
                label = 'Renderer for %s %s' % (game_plugin.name,
                                                holder_class.__name__),
                plugintype = 'X-Omaha-HolderRenderer',
                context=context,
                pattern = plugin_pattern.ParentOf('X-Omaha-Holder-Categories',
                                                  holder_class),
                default_by_name = cls.__default_renderers[(game_class, holder_class)]
                )
        return p

    def set_params(self, params: dict[str,Union[Overlord.Param[Any],Any]]) -> None:
        parentparams = dict(params)

        self.holder_renderers = {}
        if not self.game.holders:
            logger.error("cannot set_params without holders")
        for paramid, param in params.items():
            assert isinstance(param, Overlord.Param), \
                   "%s %s is not a Param" % (type(param), param)
            if paramid.startswith("HolderRenderer-"):
                # one renderer for each holder (possibly several of a class)
                found = False
                for holder in self.game.holders:
                    if paramid != "HolderRenderer-" + class_fullname(type(holder)):
                        continue
                    found = True # we may match more than one

                    setattr(self, paramid, param)

                    # setup context for this param
                    context = self.context.clone()
                    assert PieceHolder not in context
                    context.add('PieceHolder', type(holder))

                    # instantiate a renderer for this holder
                    brclass = param.value.Class
                    assert issubclass(brclass, HolderRenderer), \
                           "%r is not a %r" % (brclass, HolderRenderer)
                    self.holder_renderers[holder] = brclass(holder=holder, gui=self.gui,
                                                            parentcontext=context)
                    if  (param.value.name in param.subparams and
                         param.subparams[param.value.name]):
                        self.holder_renderers[holder].set_params(
                            param.subparams[param.value.name])

                if not found:
                    logger.error("param %s does not match any holder's class", paramid)
                del parentparams[paramid]

        # postcondition: check each holder got its renderer
        if self.game.is_set_up:
            for holder in self.game.holders:
                assert holder in self.holder_renderers, \
                       "holder %r not in %r" % (holder, self.holder_renderers.keys())

        super().set_params(parentparams)

    def draw_pieces(self, drawing_context: DrawingContext, gamestate: GameState) -> None:
        """Overrides generic implementation by delegating to holder renderers.

        Some holders will want to order piece drawing in a specific
        way, or to draw only some of the pieces they hold.
        """
        for holder in self.game.holders:
            self.holder_renderers[holder].draw_pieces(drawing_context, gamestate)

    def location_tooltip_text(self, gamestate: GameState, loc: Location) -> str:
        if not loc:
            return ""
        tooltip = super().location_tooltip_text(gamestate, loc)
        holder_tooltip: str = self.holder_renderers[loc.holder].location_tooltip_text(gamestate, loc)
        if not holder_tooltip:
            return tooltip
        if tooltip:
            return holder_tooltip + "\n" + tooltip
        else:
            return holder_tooltip

_TPieceSet = TypeVar("_TPieceSet")

class PieceDelegatingGameRenderer(HolderDelegatingGameRenderer, Generic[_TPieceSet]):
    """
    Game renderer delegating rendering of pieces to a PieceRenderer.
    """

    # FIXME: make it a param
    MARGIN = 10 # percent

    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
        self.__piece_renderer: PieceRenderer

    @Overlord.parameter
    def piecerenderer_class(cls, context: Context) -> Overlord.params.Plugin:
        game_plugin = context.game_plugin
        game_class = game_plugin.Class
        assert issubclass(game_class, Game)
        cls_plugin = getattr(cls, 'x-plugin-desc')

        # look for a default renderer declaration matching this piece
        for ancestor in game_class.mro():
            default = cls_plugin.datafield("X-Omaha-DefaultPieceRenderer-%s" %
                                           ancestor.__name__)
            if default != '':
                break
        else:
            default = None

        return Overlord.params.Plugin(
            label = 'Piece renderer for %s' % (game_plugin.name,),
            plugintype = 'X-Omaha-PieceRenderer',
            context=context,
            pattern = plugin_pattern.And(
                plugin_pattern.ParentOf('X-Omaha-Game-Categories',
                                        game_class),
                plugin_pattern.ParentOf('X-Omaha-GameRenderer-Categories',
                                        cls)),
            default_by_name = default
            )

    def set_params(self, params: dict[str,Union[Overlord.Param[Any],Any]]) -> None:
        piecerenderer_param = None
        for paramid, param in params.items():
            if paramid == "piecerenderer_class":
                # HACK: keep to apply subparams
                piecerenderer_param = param

        super().set_params(params)

        # propagate piecerenderer_class change
        if piecerenderer_param != None:
            assert isinstance(piecerenderer_param, Overlord.Param)
            self._apply_pluginparams("_PieceDelegatingGameRenderer__piece_renderer",
                                     piecerenderer_param,
                                     gui=self.gui)
            assert isinstance(self.__piece_renderer, PieceRenderer)

    def draw_pieces(self, drawing_context: DrawingContext, gamestate: GameState) -> None:
        # notify the piece renderer of the piece size for each set
        for set_id in self._piece_sets:
            self.__piece_renderer.set_piece_size(
                set_id,
                self._piece_set_size(set_id),
                self._piece_set_rotation(set_id))

        # do render
        super().draw_pieces(drawing_context, gamestate)

    def _draw_piece(self, drawing_context: DrawingContext,
                    gamestate: GameState, piece: Piece) -> None:
        holder = gamestate.piece_holder(piece)
        assert holder
        center = self.holder_renderers[holder].tile_center(piece.location(gamestate))
        self.__piece_renderer.draw_piece(drawing_context, gamestate,
                                         center, self._piece_set(gamestate, piece),
                                         piece)

    @property
    def _piece_sets(self) -> tuple[_TPieceSet, ...]:
        raise NotImplementedError()
    def _piece_set(self, gamestate: GameState, piece: Piece) -> _TPieceSet:
        raise NotImplementedError()
    def _piece_set_rotation(self, set_id: _TPieceSet) -> float:
        raise NotImplementedError()
    def _piece_set_size(self, set_id: _TPieceSet) -> float:
        raise NotImplementedError()

class SinglePieceSetGameRenderer(PieceDelegatingGameRenderer[PieceHolder]):
    # pylint: disable=abstract-method
    """
    PieceDelegatingGameRenderer using a single set of pieces per holder.
    """

    @property
    def _piece_sets(self) -> tuple[PieceHolder, ...]:
        return tuple(self.game.holders)

    def _piece_set(self, gamestate: GameState, piece: Piece) -> PieceHolder:
        holder = gamestate.piece_holder(piece)
        assert holder
        return holder

    def _piece_set_rotation(self, set_id: PieceHolder) -> float:
        assert set_id in self._piece_sets, \
               "%r not in %r" % (set_id, self._piece_sets)
        return 0
    def _piece_set_size(self, set_id: PieceHolder) -> float:
        assert set_id in self._piece_sets, \
               "%r not in %r" % (set_id, self._piece_sets)
        # size of images
        holder_renderer = self.holder_renderers[set_id]
        dx, dy = holder_renderer.delta_x, holder_renderer.delta_y # FIXME needs Euclidian2D
        assert dx or dy, "cannot determine piece size without delta_x or delta_y"
        size: float
        if not dx:
            size = dy
        elif not dy:
            size = dx
        else:
            size = min(holder_renderer.delta_x, holder_renderer.delta_y)
        size = max(1, (100 - 2 * self.MARGIN) * size / 100)
        return size


class ColoredPlayersGameRenderer(GameRenderer):
    # pylint: disable=abstract-method
    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
        self.colornames: dict[Optional[Player],str] = {}

    @classmethod
    def parameters_dcl(cls, context: Context) -> dict[str,Overlord.ParamDecl[Any]]:
        p = dict(super().parameters_dcl(context))
        default_colors: Iterable[str]
        if context.game_plugin.has_datafield("X-Omaha-Default-PlayerColors"):
            default_colors = context.game_plugin.listfield("X-Omaha-Default-PlayerColors")
        else:
            default_colors = itertools.chain(["grey", # neutral
                                              "red", "green", "blue", "cyan", "magenta", "yellow"],
                                             itertools.repeat("brown")) # 7th player and above

        game_class = context.game_plugin.Class
        assert issubclass(game_class, Game)
        for player_name, defcolor in zip(itertools.chain(["Neutral"], game_class.player_names),
                                         default_colors):
            if not defcolor:    # a game with no Neutral gives it no color
                continue

            paramid = "Color-" + player_name
            assert paramid not in p
            p[paramid] = Overlord.params.String(label=f'Stone color for {player_name}',
                                                default=defcolor)

        return p

    def set_params(self, params: dict[str,Union[Overlord.Param[Any],Any]]) -> None:
        parentparams = dict(params)
        for paramid, param in params.items():
            if paramid.startswith("Color-"):
                found = False
                for owner in itertools.chain([None], self.gui.game.players):
                    owner_name = owner.name if owner else "Neutral"
                    if paramid != "Color-" + owner_name:
                        continue
                    found = True
                    self.colornames[owner] = param.value

                if not found:
                    logger.error("param %s does not match any holder's class", paramid)
                del parentparams[paramid]

        super().set_params(parentparams)


class MoveHighlights:
    "Namespacing-only class for type of move highlights"

    # recently-made moves
    LASTMOVES = "lastmoves"
