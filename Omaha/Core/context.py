# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

from typing import TYPE_CHECKING, TypeVar

import Overlord
if TYPE_CHECKING:
    from .piece_holder import PieceHolder

__all__ = ['Context']

class Context(Overlord.Context):
    _TContext = TypeVar("_TContext", bound="Context") # poor man's Self
    def __init__(self, plugin_manager: Overlord.PluginManager):
        super().__init__(plugin_manager)
        self.holder_types: set[type[PieceHolder]]
        self.game_plugin: Overlord.Plugin
    def clone(self: _TContext) -> _TContext:
        new = super().clone()
        if hasattr(self, "holder_types"):
            new.holder_types = set(self.holder_types)
        if hasattr(self, "game_plugin"):
            new.game_plugin = self.game_plugin
        return new
