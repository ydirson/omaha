# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

import getpass
import logging
from typing import TYPE_CHECKING, Any, Callable, Optional, TypeVar

import Overlord
from Overlord.misc import classproperty

from .exceptions import IncompleteMove
from .player import Player

if TYPE_CHECKING:
    from .game_app import GameApp
    from .moves import Move

__all__ = [ 'PlayerDriver',
            'LocalPlayerDriver', 'AIPlayerDriver', 'RemotePlayerDriver',
            'PlayerDriverEvent', 'PlayerDriverFatalError',
            'MoveComposer', 'AccumulatingMoveComposer',
]

logger = logging.getLogger(__name__)

TPlayerDriver = TypeVar("TPlayerDriver", bound="PlayerDriver")

class PlayerDriver(Overlord.Parametrizable):
    """
    Class for objects that drive a player in a game.
    """
    def __init__(self, gui: "GameApp", player: Player, **kwargs: Any):
        super().__init__(**kwargs)
        self.gui = gui
        self.player = player
        self.__listeners: list[Callable[["PlayerDriverEvent"],None]] = []

    @classproperty
    @classmethod
    def class_description(cls) -> Optional[str]:
        "Grouping label, or None if not to be grouped with other PlayerDriver's"
        logger.fatal("no class_description for %s", cls)
        raise NotImplementedError()

    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return PlayerDriver

    @Overlord.parameter
    def name(cls, context: Overlord.Context) -> Overlord.params.String:
        return Overlord.params.String(
            label = "Name",
            default = "",
        )

    def connect(self) -> None:
        raise NotImplementedError()
    def disconnect(self) -> None:
        raise NotImplementedError()

    def supports_undo(self) -> bool:
        return False

    def register_listener(self, l: Callable[["PlayerDriverEvent"],None]) -> None:
        "Register function of one argument to be called for PlayerDriverEvent."
        self.__listeners.append(l)
    def unregister_listener(self, l: Callable[["PlayerDriverEvent"],None]) -> None:
        self.__listeners.remove(l)
    def notify_listeners(self, event: "PlayerDriverEvent") -> None:
        for l in self.__listeners:
            l(event)

    def clone(self: TPlayerDriver) -> TPlayerDriver:
        # prevent calling of Parametrizable.clone()
        raise SyntaxError("clone() is not supported in PlayerDriver")

class MoveComposer(Overlord.Parametrizable):
    """An object to which LocalPlayerDriver delegates construction of Move object.

    It has no knowledge of the player making the move, and broadcasts
    the move to all connected PlayerDriver's, which are responsible
    for deciding whether sending them to the Game.
    """

    def __init__(self, gui: GameApp, **kwargs: Any):
        super().__init__(**kwargs)
        self.gui = gui

    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return MoveComposer

    def send_move_to_playerdriver(self, move: Move) -> None:
        for player_driver in self.gui.local_playerdrivers:
            player_driver.publish_composed_move(move)

    def connect_to_gui(self) -> None:
        raise NotImplementedError()
    def disconnect_from_gui(self) -> None:
        raise NotImplementedError()


class AccumulatingMoveComposer(MoveComposer):
    # pylint: disable=abstract-method
    "Player driver accumulating interactions into current_move"
    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
        self.current_move = None

    def __new_param(self, decl: Overlord.ParamDecl) -> Overlord.Param:
        p = Overlord.Param(decl)
        p.owner = self
        return p

    def _finalize_move(self) -> None:
        try:
            assert self.current_move
            self.send_move_to_playerdriver(self.current_move)
        except IncompleteMove as ex:
            paramlist = { paramid: self.__new_param(decl)
                          for paramid, decl in ex.paramlist.items() }
            self.gui.tk.askuser_params("Finalize move", paramlist, app_ui=self.gui)
            for attribute, param in paramlist.items():
                logger.debug("Finalizing move with %s=%s", attribute, param.value)
                setattr(self.current_move, attribute, param.value)
            # retry with this updated move
            self._finalize_move()

class LocalPlayerDriver(PlayerDriver):
    # pylint: disable=abstract-method
    "Player driver for local user interaction"
    @classproperty
    @classmethod
    def class_description(cls) -> Optional[str]:
        # one of its kind, no group
        return None
    def supports_undo(self) -> bool:
        return True
    @Overlord.parameter
    def name(cls, context: Overlord.Context) -> Overlord.params.String: # type: ignore[override] # WTF
        return Overlord.params.String(
            label = "Name",
            default = getpass.getuser(),
        )

    def publish_composed_move(self, move: Move) -> None:
        "Send to game a move, composed by a MoveComposer"
        if move.player is not self.player:
            return
        self.gui.game.make_move(self.player, move)

    def connect(self) -> None:
        self.gui.register_playerdriver(self)
        self.gui.game.signal_ready(self.player, True)
    def disconnect(self) -> None:
        self.gui.game.signal_ready(self.player, False)
        self.gui.unregister_playerdriver(self)

class AIPlayerDriver(PlayerDriver):
    # pylint: disable=abstract-method
    "Player driver for AI engines"
    @classproperty
    @classmethod
    def class_description(cls) -> str:
        return "AI"

class RemotePlayerDriver(PlayerDriver):
    # pylint: disable=abstract-method
    "Player driver proxy for a remote player"
    @classproperty
    @classmethod
    def class_description(cls) -> str:
        return "remote"

class IsInteractive(Overlord.PluginPattern):
    def match_distance(self, plugin: Overlord.Plugin) -> Optional[int]:
        if  (plugin.has_datafield("X-Omaha-Interactive") and
             plugin.datafield("X-Omaha-Interactive") == "true"):
            return 0
        return None

class PlayerDriverEvent:
    """A change in the state of a PlayerDriver."""
    def __init__(self, player_driver: PlayerDriver, **kwargs: Any):
        super().__init__(**kwargs)
        self.__player_driver = player_driver
    @property
    def player_driver(self) -> PlayerDriver:
        return self.__player_driver

class PlayerDriverFatalError(PlayerDriverEvent):
    """A error that caused a PlayerDriver to become unusable."""
    def __init__(self, message: str, **kwargs: Any):
        super().__init__(**kwargs)
        self.__message = message
    @property
    def message(self) -> str:
        return self.__message
