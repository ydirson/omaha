# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

import functools
import logging
from typing import Any, Callable, Optional, Union
import os

import Overlord
from Overlord import plugin_pattern

from .UI import App
from .game import Game, Match, UndoableGame, GamePhase, PlayerTurnChange, GameEvent
from .player import Player
from .game_renderer import GameRenderer
from .player_driver import PlayerDriver, LocalPlayerDriver, RemotePlayerDriver, MoveComposer
from .player_driver import PlayerDriverEvent, PlayerDriverFatalError
from .context import Context

__all__ = [ 'GameApp' ]

logger = logging.getLogger(__name__)

# FIXME: isn't there some Play-specific stuff here ...

class GameApp(App):
    def __init__(self, match: Match, **kwargs: Any):
        super().__init__(**kwargs)
        self.canvas: object
        self.game_renderer: GameRenderer
        self.move_composer: MoveComposer
        self.local_playerdrivers: set[LocalPlayerDriver] = set()

        self.match = match
        self.game = match.game

    def run(self) -> None:
        self._create_gui()
        assert self.canvas
        self.check_for_undo()

        ## connect GUI and game engine

        self.connect_players()
        self.game.register_listener(self.__game_listener)

        # first player was set before our listener could get the event
        self.set_player_turn(self.game.scratch_state.whose_turn)

        # launch game
        self.game.ui_resume()

    def release_backrefs(self) -> None:
        super().release_backrefs()
        try:
            # can be called before registration (from _init_default_params)
            self.game.unregister_listener(self.__game_listener)
        except ValueError:
            # FIXME: should use a more specific exception
            pass

    def register_playerdriver(self, player_driver: LocalPlayerDriver) -> None:
        self.local_playerdrivers.add(player_driver)
    def unregister_playerdriver(self, player_driver: LocalPlayerDriver) -> None:
        self.local_playerdrivers.remove(player_driver)

    @Overlord.parameter
    def game_renderer_class(cls, context: Context) -> Overlord.params.Plugin:
        game_plugin = context.game_plugin
        assert isinstance(game_plugin, Overlord.Plugin)
        if game_plugin.has_datafield("X-Omaha-DefaultRenderer"):
            default_renderer = game_plugin.datafield("X-Omaha-DefaultRenderer")
            logger.info("Using %s as requested by %s",
                        default_renderer, game_plugin.name)
        else:
            default_renderer = None
        return Overlord.params.Plugin(
            label = 'Renderer for %s' % game_plugin.name,
            plugintype = 'X-Omaha-GameRenderer',
            context = context,
            # FIXME: should allow X-Omaha-RendererCompat too
            pattern = plugin_pattern.ParentOf(
                'X-Omaha-Game-Categories', game_plugin.Class),
            default_by_name = default_renderer
            )

    @Overlord.parameter
    def move_composer_class(cls, context: Context) -> Overlord.params.Plugin:
        game_plugin = context.game_plugin
        game_class = game_plugin.Class
        assert issubclass(game_class, Game)

        return Overlord.params.Plugin(
            label = "Move composer",
            plugintype = 'X-Omaha-MoveComposer',
            context = context,
            pattern = plugin_pattern.ParentOf(
                'X-Omaha-Move-Categories', game_class.Move),
            default_by_name = cls.__default_movecomposer(context),
        )

    def set_params(self, params: dict[str,Union[Overlord.Param[Any],Any]]) -> None:
        parentparams = dict(params)
        replacing_movecomposer = False
        game_renderer_param: Optional[Overlord.Param[Overlord.Plugin]] = None

        for paramid, param in params.items():
            if paramid == "game_renderer_class":
                # HACK: keep to apply subparams
                game_renderer_param = param

            elif paramid == "move_composer_class":
                composer_class = param.value.Class
                assert issubclass(composer_class, MoveComposer), \
                    f"{composer_class} is not a MoveComposer"

                # replace the existing move composer, but wait till the
                # new one started before removing the old one
                new_composer = composer_class(gui=self, parentcontext=self.context)
                if hasattr(self, "move_composer"):
                    replacing_movecomposer = True
                    self.move_composer.disconnect_from_gui()
                self.move_composer = new_composer

                # apply any MoveComposer parameters
                choice_text = param.value.name
                if choice_text in param.subparams:
                    self.move_composer.set_params(param.subparams[choice_text])
                else:
                    logger.debug("no subparams for %r in %r", choice_text, param.subparams)

                # let upcall deal with underlying ParameterAttribute

        super().set_params(parentparams)

        # propagate game_renderer_class change
        if game_renderer_param:
            self._apply_pluginparams("game_renderer",
                                     game_renderer_param,
                                     game=self.game, gui=self)
            assert isinstance(self.game_renderer, GameRenderer)

        if replacing_movecomposer:
            # FIXME: on first setting MoveComposer, we must wait for
            # the GUI to be there before we can connect.
            self.move_composer.connect_to_gui()

    def playerdriver_listener(self, event: PlayerDriverEvent) -> None:
        "React to PlayerDriverEvent"
        assert isinstance(event, PlayerDriverEvent)
        if isinstance(event, PlayerDriverFatalError):
            event.player_driver.unregister_listener(self.playerdriver_listener)
            event.player_driver.disconnect()
            del self.match._player_drivers[event.player_driver.player]
            self.tk.notify("player driver fatal error",
                           "player driver '%s' for player %s:\n%s" %
                           (event.player_driver.name, event.player_driver.player.name,
                            event.message))
            # FIXME should keep track of the issue in the Game

    def fill_from_playerdriver(self, player: Player, drv: PlayerDriver) -> None:
        "Fill UI with details of player driver"
        assert isinstance(player, Player), "%r is not a Player" % (player,)
        assert isinstance(drv, PlayerDriver)
        name_param = Overlord.getparam(drv, "name")
        driver_name = name_param.value
        driver_text = "({})".format(drv.class_description)
        # FIXME never unregistered
        name_param.register_changed(
            lambda param, oldvalue, drv: self.set_playerdriver_name(
                player, param.value if param else "-"),
            drv=drv)

        self.set_playerdriver_type(player, driver_text)
        self.set_playerdriver_name(player, driver_name)

    def set_player_turn(self, player: Player) -> None:
        raise NotImplementedError()
    def set_playerdriver_type(self, player: Player, driver_text: str) -> None:
        raise NotImplementedError()
    def set_playerdriver_name(self, player: Player, name: str) -> None:
        raise NotImplementedError()

    def connect_players(self) -> None:
        self.move_composer.connect_to_gui()
        for player_drv in self.match._player_drivers.values():
            player_drv.connect()
    def disconnect_players(self) -> None:
        for key, player_drv in self.match._player_drivers.items():
            player_drv.unregister_listener(self.playerdriver_listener)
            player_drv.disconnect()
        self.match._player_drivers.clear()
        self.move_composer.disconnect_from_gui()

    @classmethod
    def __default_movecomposer(cls, context: Context) -> Optional[str]:
        # try to locate a default movecomposer decl
        default_drv: Optional[str] = None
        game_plugin = context.game_plugin
        assert isinstance(game_plugin, Overlord.Plugin)
        game_class = game_plugin.Class
        assert issubclass(game_class, Game)
        for ancestor in game_class.mro():
            if hasattr(ancestor, 'x-plugin-desc'):
                ancestor_plugin = getattr(ancestor, 'x-plugin-desc')
                if ancestor_plugin.has_datafield("X-Omaha-DefaultMoveComposer"):
                    default_drv = ancestor_plugin.datafield(
                        "X-Omaha-DefaultMoveComposer")
                    break
        else:
            logger.info("no default MoveComposer for %s", game_class)
            # select a player driver for the most specific move type
            default_drv = context.plugin_manager.find_plugin(
                'X-Omaha-MoveComposer',
                pattern = plugin_pattern.ParentOf('X-Omaha-Move-Categories',
                                                  game_class.Move),
            ).ID
        return default_drv

    def local_players(self) -> dict[Player,PlayerDriver]:
        return {player: driver for player, driver in self.match._player_drivers.items()
                if isinstance(driver, LocalPlayerDriver)}
    def nonremote_players(self) -> dict[Player,PlayerDriver]:
        return {player: driver for player, driver in self.match._player_drivers.items()
                if not isinstance(driver, RemotePlayerDriver)}

    def save_game(self, filename: str) -> None:
        """Write game to `filename`, safely.

        Write to a temporary `.part` file first, and rename it only
        when write is succesful, so as to avoid clobbering a valid
        pre-existing file with a buggy one.

        The temporary file is kept on error, to help in debugging
        problems.
        """
        # FIXME: catch errors and notify user
        partialfilename = filename + ".part"
        WriterClass = self.game.get_writer_class(filename)
        writer = WriterClass(self.match)
        with open(partialfilename, "w+") as f:
            writer.write_to(f)
        os.rename(partialfilename, filename)

    ## the UI itself

    def __game_listener(self, event: GameEvent) -> None:
        if isinstance(event, Game.PhaseChangeEvent):
            if event.newphase is GamePhase.Over:
                winners = [ player for player, rank
                            in self.game.outcome.items()
                            if rank is self.game.Outcomes.Winner ]
                resigns = [ player for player, rank
                            in self.game.outcome.items()
                            if rank is self.game.Outcomes.Resign ]
                draws = [ player for player, rank
                          in self.game.outcome.items()
                          if rank is self.game.Outcomes.IsDraw ]
                if len(winners) == 1:
                    phrase = "%s won!" % (winners[0].name,)
                    if resigns:
                        phrase = "{0} resigned, {1}".format(
                            ', '.join(p.name for p in resigns), phrase)
                elif len(draws) == len(self.game.players):
                    phrase = 'Draw game'
                else:
                    phrase = "cannot interpret outcome {0}".format(
                        self.game.outcome)
                self.tk.notify("Game over",
                               "Game over, %s" % (phrase,) )

        elif isinstance(event, PlayerTurnChange):
            self.set_player_turn(event.newplayer)

    @property
    def main_window(self) -> object:
        raise NotImplementedError()

    def _create_gui(self) -> None:
        raise NotImplementedError()

    @property
    def canvas_size(self) -> tuple[int,int]:
        raise NotImplementedError()

    def _add_game_action(self, label: str, callback: Callable[[],None]) -> object:
        raise NotImplementedError()
    def _remove_game_action(self, game_action: object) -> None:
        raise NotImplementedError()

    def check_for_undo(self) -> None:
        supports_undo = functools.reduce(
            lambda x, y: x and y,
            (drv.supports_undo() for drv in self.match._player_drivers.values()))
        if supports_undo and isinstance(self.game, UndoableGame):
            self._enable_undo()
        else:
            self._disable_undo()
    def _enable_undo(self) -> None:
        pass
    def _disable_undo(self) -> None:
        pass
