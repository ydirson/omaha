# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

__all__ = [ 'Rectangle' ]

class Rectangle:
    """
    An object describing a rectangular area on the screen.
    Will also include some rotation support.
    """
    def __init__(self, x: float, y: float, w: float, h: float):
        self.x = x
        self.y = y
        self.width = w
        self.height = h
        #FIXME: orientation

    def contains(self, x: float, y: float) -> bool:
        return (x >= self.x and y >= self.y and
                x <= self.x_max and y <= self.y_max)

    @property
    def center(self) -> tuple[float,float]:
        return (self.x + self.width / 2, self.y + self.height / 2)

    @property
    def x_max(self) -> float:
        return self.x + self.width

    @property
    def y_max(self) -> float:
        return self.y + self.height

    def __str__(self) -> str:
        return "{0.width}x{0.height}+{0.x}+{0.y}".format(self)
