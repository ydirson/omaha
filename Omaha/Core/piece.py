# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from typing import TYPE_CHECKING, Any, Generic, Optional, TypeVar

from .player import Player
if TYPE_CHECKING:
    from .piece_holder import PieceHolder, Location
    from .game import GameState

__all__ = [ 'Piece', 'PieceState', 'FacetedPieceState' ]

TPiece = TypeVar("TPiece", bound="Piece")
PieceType = TypeVar("PieceType")

class PieceState(Generic[TPiece]):
    def __init__(self, piece: TPiece):
        self.__piece = piece
        self.__owner: Optional[Player] = None
        self.__holder: Optional["PieceHolder"] = None
    @property
    def piece(self) -> TPiece:
        return self.__piece
    @property
    def owner(self) -> Optional[Player]:
        return self.__owner
    @property
    def holder(self) -> Optional["PieceHolder"]:
        return self.__holder

    def set_owner(self, owner: Optional[Player]) -> None: # pylint: disable=method-hidden
        self.__owner = owner
    def set_holder(self, holder: Optional["PieceHolder"]) -> None: # pylint: disable=method-hidden
        self.__holder = holder

    def clone(self: "PieceState[TPiece]") -> "PieceState[TPiece]":
        c = type(self)(self.__piece)
        c.__owner = self.__owner
        c.__holder = self.__holder
        return c
    def frozen(self: "PieceState[TPiece]") -> "PieceState[TPiece]":
        c = self.clone()
        c.freeze()
        return c
    def freeze(self) -> None:
        self.set_owner = NotImplemented # type: ignore[assignment]
        self.set_holder = NotImplemented # type: ignore[assignment]

class Piece(Generic[PieceType]):
    """Class for the generic concept of a piece.

    Subclassed or referred-to by Game subclasses.

    Used as hierarchy root, but does not provide proper __init__
    chaining for multiple inheritance (some subclasses need move args
    to __init__).
    """

    def __init__(self, piecetype: PieceType):
        self.__type = piecetype

    @property
    def type(self) -> PieceType:
        return self.__type

    def location(self, gamestate: "GameState") -> Optional["Location"]:
        return gamestate.piece_location(self)

    def clone(self: TPiece) -> TPiece:
        raise SyntaxError("obsolete interface moved to PieceState")

    def __str__(self) -> str:
        raise DeprecationWarning("Piece.__str__: use GameState.piece_description")
        return ("Piece %x type %s" % (id(self), self.type,))
    def __repr__(self) -> str:
        return ("<%s %x (%s)>" %
                (type(self).__name__, id(self), self.type))

    # catch use of old API
    @property
    def player(self) -> Player: raise SyntaxError("obsolete interface moved to PieceState")
    @property
    def holder(self) -> "PieceHolder": raise SyntaxError("obsolete interface moved to PieceState")

class FacetedPieceState(PieceState):
    """A PieceState for pieces featuring several possible faces.

    Some pieces can have a single face, but this class suits best
    games in which at least one piece can change its face value.
    """
    @property
    def face(self) -> Any:
        raise NotImplementedError()
