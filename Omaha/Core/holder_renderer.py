# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

from typing import TYPE_CHECKING, Any

import Overlord
from Overlord.misc import classproperty

if TYPE_CHECKING:
    from .game import GameState
    from .game_app import GameApp
    from .piece_holder import PieceHolder, Location
    from .rectangle import Rectangle
    from .UI import DrawingContext

__all__ = [ 'HolderRenderer', 'LocHighlights' ]

class HolderRenderer(Overlord.Parametrizable):
    """
    Base class for holder renderers.
    Used as hierarchy root for proper __init__ chaining, and
    for checking by introspection.
    """
    def __init__(self, holder: PieceHolder, gui: GameApp, **kwargs: Any):
        super().__init__(**kwargs)
        self.holder = holder
        self.gui = gui

    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return HolderRenderer

    def tile_center(self, location: Location) ->tuple[int,int]:
        raise NotImplementedError()

    def draw(self, drawing_context: DrawingContext,
             rectangle: Rectangle, gamestate: GameState) -> None:
        pass

    def draw_pieces(self, drawing_context: DrawingContext, gamestate: GameState) -> None:
        "Default drawing of pieces: all in (unspecified) order"
        for piece in gamestate.holder_state(self.holder).pieces:
            self.gui.game_renderer._draw_piece(drawing_context, gamestate, piece)

    def location_tooltip_text(self, gamestate: GameState, loc: Location) -> str:
        return ""

    def point_to_location(self, x: int, y: int) -> Location:
        """
        Map coordinates in canvas to a holder location.
        """
        raise NotImplementedError()

    def clone(self) -> HolderRenderer:
        # prevent calling of Parametrizable.clone()
        raise SyntaxError("clone() is not supported in HolderRenderer")

class LocHighlights:
    "Namespacing-only class for type of location highlights"

    # locations that are part of the move being composed interactively
    COMPOSING = "composing"
    # locations that may become part of the move being composed
    MAYBE = "maybe"
    # locations that are available as continuations of the move being composed
    REACHABLE = "reachable"
