# This file is part of the Omaha Board-Game GUI.
# Copyright (C) 2009-2023  Yann Dirson
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation,
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

from __future__ import annotations

from enum import Enum
import functools
import logging
import sys
from typing import (TYPE_CHECKING, cast, Any, Callable, ClassVar,
                    Iterable, Mapping, Optional, Sequence, TypeVar, Union)

from frozendict import frozendict

import Overlord
from Overlord import plugin_pattern
from Overlord.misc import classproperty, class_fullname

from .player import Player
from .player_driver import PlayerDriver
from .piece import Piece as CorePiece, PieceState as CorePieceState
from .piece_holder import (Location, LocationContents, PieceHolder,
                           HolderState, OnePieceTypePerLocHolderState)
from .moves import Move as CoreMove
from .notations import MoveNotation
from .exceptions import InvalidMove, GameSuspended
from .context import Context

if sys.version_info >= (3, 11):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

if TYPE_CHECKING:
    from .game_io import GameWriter
    from .game_app import GameApp

__all__ = [ 'Match', 'Game', 'AlternatingTurnsGame', 'UndoableGame', 'AutomaticMovesGame',
            'GamePhase', 'GameState', 'OnePieceTypePerLocGameState', 'MoveList' ]

logger = logging.getLogger(__name__)

_T = TypeVar("_T")

class MoveList:
    """
    A list of Move objects to encapsulate game history.
    """
    def __init__(self, game: Game):
        self.__game = game
        self.__moves: list[CoreMove] = []
        self.__current = 0
        self.__listeners: list[Callable[[MoveList, int,int],None]] = []

        # Each move and their resulting GameState share the same
        # index, and initial gamestate for move is looked up with
        # [index-1].  __initial_state stores `__state[-1]`, the game
        # starting position, which cannot be undone.
        self.__initial_state: GameState
        self.__states: list[GameState] = []

    @property
    def nmoves_done(self) -> int:
        """Number of moves done since the game started."""
        return self.__current

    def add_move(self, move: CoreMove, state: GameState) -> None:
        """Add given move in the list and increment pointer."""
        self.__moves[self.__current:] = [move]
        self.__states[self.__current:] = [state]
        self.__current += 1
        self.__trigger_listeners(self.__current - 1, self.__current)

    def undo(self) -> None:
        """
        Undo last move.

        We do not forget it yet (think "redo"), only move the pointer.
        """
        if self.__current > 0:
            self.__current -= 1
            self.__trigger_listeners(self.__current + 1, self.__current)

    def redo(self) -> None:
        """Redo undone move."""
        if self.__current < len(self.__moves):
            self.__current += 1
            self.__trigger_listeners(self.__current - 1, self.__current)
        else:
            raise IndexError()

    def for_each_played(self, action: Callable[[int,CoreMove],_T],
                        first: int=0, last: Optional[int]=None) -> list[_T]:
        """Return list of results of `action` calls for each move in range(first, last).

        `action` is called with 2 parameters: the move number and the move itself.
        """
        if last == None:
            last = self.__current
        return [action(first + i, m) for i, m in enumerate(self.__moves[first:last])]

    def register_listener(self, l: Callable[[MoveList, int,int],None]) -> None:
        self.__listeners.append(l)
    def unregister_listener(self, l: Callable[[MoveList, int,int],None]) -> None:
        self.__listeners.remove(l)
    def __trigger_listeners(self, newpos: int, oldpos: int) -> None:
        for l in self.__listeners:
            l(self, newpos, oldpos)

    # TODO: that should indeed be a "view" of __moves
    @property
    def played(self) -> list[CoreMove]:
        """The list of moves effectively played, excluding undone ones."""
        return self.__moves[:self.__current]

    @property
    def last(self) -> CoreMove:
        """Last move recorded."""
        if self.__current == 0:
            raise IndexError("game has not started")
        return self.__moves[self.__current - 1]
    @property
    def last_state(self) -> GameState:
        """Last move recorded."""
        if self.__current == 0:
            assert self.__initial_state
            return self.__initial_state
        return self.__states[self.__current - 1]

    def set_initial_state(self, state: GameState) -> None:
        assert not hasattr(self, "__initial_state")
        self.__initial_state = state

    @property
    def pretty_printed(self) -> str:
        result = "[ "
        for i, move in enumerate(self.__moves):
            if self.__current == i:
                result += "<> "
            result += self.__game.default_notation.move_serialization(move) + " "
        if self.__current == len(self.__moves):
            result += "<> "
        result += "]"
        return result


class GameEvent:
    """A change in the state of a game."""
    def __init__(self, game: Game, **kwargs: Any):
        super().__init__(**kwargs)
        self.__game = game
    @property
    def game(self) -> Game:
        return self.__game

class MoveEvent(GameEvent):
    def __init__(self, move: CoreMove, **kwargs: Any):
        super().__init__(**kwargs)
        self.__move = move
    @property
    def move(self) -> CoreMove:
        return self.__move

class PlayerTurnChange(GameEvent):
    def __init__(self, oldplayer: Optional[Player], newplayer: Player, **kwargs: Any):
        super().__init__(**kwargs)
        self.__oldplayer, self.__newplayer = oldplayer, newplayer
    @property
    def oldplayer(self) -> Optional[Player]:
        return self.__oldplayer
    @property
    def newplayer(self) -> Player:
        return self.__newplayer

class UndoEvent(GameEvent):
    pass

class ResignEvent(GameEvent):
    def __init__(self, player: Player, **kwargs: Any):
        super().__init__(**kwargs)
        self.__player = player
    @property
    def player(self) -> Player:
        return self.__player

class PhaseChangeEvent(GameEvent):
    def __init__(self, oldphase: GamePhase, newphase: GamePhase, **kwargs: Any):
        super().__init__(**kwargs)
        self.__oldphase, self.__newphase = oldphase, newphase
    @property
    def oldphase(self) -> GamePhase:
        return self.__oldphase
    @property
    def newphase(self) -> GamePhase:
        return self.__newphase

class GameOutcomes(Enum):
    Winner = "winner"
    IsDraw = "draw"
    Resign = "resign"

class GamePhase:
    """One of several phases in a game.

    Includes instances `SettingUp`, `Playing`, `Suspended`, `Over` as
    standard for all games, but individual games may create more to
    suit they needs.

    This class is not unlike an Enum, except it is designed to get
    extended by subclasses, and this cannot be done with an Enum.
    """
    def __init__(self, name: str):
        self.__name = name
    def __str__(self) -> str:
        return "GamePhase(%s)" % self.__name

    # tell static checkers those are coming...
    SettingUp: "GamePhase"
    Playing: "GamePhase"
    Suspended: "GamePhase"
    Over: "GamePhase"

# ... and create them as soon as we can
GamePhase.SettingUp = GamePhase("SettingUp")
GamePhase.Playing = GamePhase("Playing")
GamePhase.Suspended = GamePhase("Suspended")
GamePhase.Over = GamePhase("Over")

_TGameState = TypeVar("_TGameState", bound="GameState")

class GameState(metaclass=Overlord.OuterClass):
    __innerclasses__ = {'PieceState'}
    PieceState: TypeAlias = CorePieceState

    "Complete state of a given game at a given point in time"
    def __init__(self, game: Game):
        self.__game = game
        self.__phase: GamePhase
        self.__whose_turn: Player
        self.__pieces: Mapping[CorePiece, CorePieceState] = {}
        self.__holders: Mapping[PieceHolder, HolderState] = {holder: holder.State(holder)
                                                             for holder in game.holders}
        self.__is_frozen = False
    @property
    def game(self) -> Game:
        return self.__game
    @property
    def is_frozen(self) -> bool:
        return self.__is_frozen

    @property
    def phase(self) -> GamePhase:
        return self.__phase
    def set_phase(self, phase: GamePhase) -> None: # pylint: disable=method-hidden
        self.__phase = phase

    @property
    def whose_turn(self) -> Player:
        return self.__whose_turn
    def set_whose_turn(self, player: Player) -> None: # pylint: disable=method-hidden
        old = getattr(self, "__whose_turn", None)
        self.__whose_turn = player
        self.game.listeners_notify(
            PlayerTurnChange(game=self.game, oldplayer=old, newplayer=player))
        logger.debug("whose_turn now %s", player.name)

    def add_piece(self, piece: CorePiece) -> None: # pylint: disable=method-hidden
        assert piece not in self.__pieces
        assert isinstance(self.__pieces, dict)
        self.__pieces[piece] = self.PieceState(piece)
    def piece_state(self, piece: CorePiece) -> CorePieceState:
        return self.__pieces[piece]
    def forget_piece(self, piece: CorePiece) -> None:
        assert self.__pieces[piece].holder is None
        assert isinstance(self.__pieces, dict)
        del self.__pieces[piece]
    @property
    def pieces(self) -> Iterable[CorePiece]:
        return self.__pieces.keys()

    def holder_state(self, holder: PieceHolder) -> HolderState:
        return self.__holders[holder]

    def piece_owner(self, piece: CorePiece) -> Optional[Player]:
        return self.__pieces[piece].owner
    def set_piece_owner(self, piece: CorePiece,
                        player: Optional[Player]) -> None: # pylint: disable=method-hidden
        self.__pieces[piece].set_owner(player)
    def piece_holder(self, piece: CorePiece) -> Optional[PieceHolder]:
        return self.__pieces[piece].holder
    def set_piece_holder(self, piece: CorePiece, holder: Optional[PieceHolder]
                         ) -> None: # pylint: disable=method-hidden
        self.__pieces[piece].set_holder(holder)

    def take_piece(self, piece: CorePiece) -> None: # pylint: disable=method-hidden
        holder = self.piece_holder(piece)
        assert holder
        self.__holders[holder].take_piece(piece)
        self.set_piece_holder(piece, None)
    def contents_at(self, location: Location) -> LocationContents:
        return self.__holders[location.holder].contents_at(location)
    def valid_location(self, location: Location) -> bool:
        return self.__holders[location.holder].valid_location(location)
    def put_piece(self, location: Location, piece: CorePiece) -> None: # pylint: disable=method-hidden
        assert isinstance(location, Location)
        assert isinstance(piece, self.game.Piece)
        assert self.piece_holder(piece) is None, "%s has holder!" % self.piece_description(piece)
        self.__holders[location.holder].put_piece(location, piece)
        self.set_piece_holder(piece, location.holder)
    def all_locations(self, holder: PieceHolder) -> Iterable[Location]:
        return self.__holders[holder].all_locations
    def piece_location_in(self, piece: CorePiece, holder: PieceHolder) -> Optional[Location]:
        return self.__holders[holder].piece_location(piece)
    def piece_location(self, piece: CorePiece) -> Optional[Location]:
        holder = self.piece_holder(piece)
        if holder is None:
            return None
        return self.piece_location_in(piece, holder)
    def next_player(self) -> None: # pylint: disable=method-hidden
        assert self.__whose_turn
        self.set_whose_turn(self.__whose_turn.next)

    def new_piece_at(self, location: Location, player: Optional[Player],
                     *piece_args: Any) -> CorePiece: # pylint: disable=method-hidden
        """
        Create a new piece, put it on given location, and return it.

        The piece is created as instance of the Piece subclass
        specified by the Game subclass being used, and that piece
        constructor is passed the requested arguments.
        """
        assert isinstance(location, Location)
        assert player is None or isinstance(player, Player)
        piece = self.game.Piece(*piece_args)
        self.add_piece(piece)
        self.set_piece_owner(piece, player)
        self.put_piece(location, piece)
        return piece

    def piece_description(self, piece: CorePiece) -> str:
        if piece is None:
            return "None"
        return ("Piece type %s of player %s at %s" %
                (piece.type, self.piece_owner(piece), self.piece_location(piece)))
    def __repr__(self) -> str:
        return("%s(%s)" % (class_fullname(type(self)), self.game))

    def clone(self: _TGameState) -> _TGameState:
        # game_clone is not complete enough for this...
        c = type(self)(self.game)
        c.__phase = self.__phase
        c.__whose_turn = self.__whose_turn
        assert isinstance(c.__pieces, dict)
        assert isinstance(c.__holders, dict)

        # PieceStates
        for piece, piecestate in self.__pieces.items():
            c.__pieces[piece] = piecestate.clone()

        # HolderStates
        for holder, holder_state in self.__holders.items():
            # replace with a clone the default HolderState populated by __init__
            c.__holders[holder] = holder_state.clone()
        return c
    def frozen(self: _TGameState) -> _TGameState:
        c = self.clone()
        c.freeze()
        return c
    def freeze(self) -> None:
        self.__pieces = frozendict({piece: state.frozen()
                                    for piece, state in self.__pieces.items()})
        self.__holders = frozendict({holders: state.frozen()
                                     for holders, state in self.__holders.items()})
        self.set_phase = NotImplemented # type: ignore[assignment]
        self.set_whose_turn = NotImplemented # type: ignore[assignment]
        self.add_piece = NotImplemented # type: ignore[assignment]
        self.set_piece_owner = NotImplemented # type: ignore[assignment]
        self.set_piece_holder = NotImplemented # type: ignore[assignment]
        self.take_piece = NotImplemented # type: ignore[assignment]
        self.put_piece = NotImplemented # type: ignore[assignment]
        self.next_player = NotImplemented # type: ignore[assignment]
        self.new_piece_at = NotImplemented # type: ignore[assignment]
        self.__is_frozen = True

class OnePieceTypePerLocGameState(GameState):
    def piece_at(self, location: Location) -> Optional[CorePiece]:
        holder_state = self.holder_state(location.holder)
        assert isinstance(holder_state, OnePieceTypePerLocHolderState), \
            f"{holder_state!r} is not a OnePieceTypePerLocHolderState"
        return holder_state.piece_at(location)


class Game(Overlord.Parametrizable,
           metaclass=Overlord.ParametrizableOuterclass):
    """Abstract base class."""

    __innerclasses__ = {'Piece', 'Move', 'State',
                        'MoveEvent', 'UndoEvent', 'PhaseChangeEvent',
                        }

    Piece: TypeAlias = CorePiece
    Move: TypeAlias = CoreMove
    State: TypeAlias = GameState

    Outcomes: type[GameOutcomes] = GameOutcomes

    MoveEvent: TypeAlias = MoveEvent
    UndoEvent: TypeAlias = UndoEvent
    PhaseChangeEvent: TypeAlias = PhaseChangeEvent

    DefaultMoveNotation: type[MoveNotation] = MoveNotation

    player_names: ClassVar[tuple[str, ...]] = NotImplemented

    def __init__(self, import_mode: bool=False, **kwargs: Any):
        super().__init__(**kwargs)
        self.__importmode = import_mode
        self.is_set_up = False
        self._holders: dict[str,PieceHolder] = {}
        self.__moves = MoveList(self)
        self.scratch_state: GameState
        self.players: tuple[Player, ...]
        self.__listeners: list[Callable[[GameEvent],None]] = []
        self.context: Context
        self.context.holder_types = set(self.holder_classes)
        self.default_notation: MoveNotation = self.DefaultMoveNotation(self)

        self.__ui_ready = False
        self.__players_ready: list[bool]
        # endgame outcome
        self.outcome: dict[Player,GameOutcomes] = {}
        # game outcome proposed by each player
        self.proposed_outcomes: dict[Player,dict[Player,GameOutcomes]] = {}

        self.create_players()

    # guard from obsolete members usage
    @property
    def pieces(self) -> Iterable[CorePiece]:
        raise SyntaxError("obsolete interface moved to GameState")
    @property
    def whose_turn(self) -> Optional[Player]:
        raise SyntaxError("obsolete interface moved to GameState")

    def clone(self) -> Game:
        # prevent calling of Parametrizable.clone()
        raise SyntaxError("clone() is not supported in Game")

    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return Game

    @property
    def last_state(self) -> GameState:
        return self.__moves.last_state

    @property
    def is_importing(self) -> bool:
        return self.__importmode
    def done_importing(self) -> None:
        self.__importmode = False

    def setup(self) -> None:
        assert not self.is_set_up
        self.setup_players()
        self.set_start_position()
        # record initial state
        self.__moves.set_initial_state(self.scratch_state.frozen())
        self.is_set_up = True

    def setup_players(self) -> None:
        self.scratch_state.set_whose_turn(self.players[0])
        self.__players_ready = [ False for p in self.players ]

    @property
    def phase(self) -> GamePhase:
        # FIXME or should it be .last_state ?
        return self.scratch_state.phase

    def change_phase(self, newphase: GamePhase) -> None:
        oldphase = self.phase
        self.scratch_state.set_phase(newphase)
        logger.info("(%x) phase now %s.", id(self), newphase)
        self.listeners_notify(
            self.PhaseChangeEvent(game=self, oldphase=oldphase, newphase=newphase))

    def ui_suspend(self) -> None:
        "Suspend game on behalf of UI"
        self.__ui_ready = False
        logger.debug("ui_suspend() => %s, UI=%s", self.__players_ready, self.__ui_ready)
        if self.phase is GamePhase.Playing:
            self.change_phase(GamePhase.Suspended)
    def ui_resume(self) -> None:
        """Start/resume game on behalf of UI.

        The game will really (re)start only when players are ready.
        """
        self.__ui_ready = True
        logger.debug("ui_resume() => %s, UI=%s", self.__players_ready, self.__ui_ready)
        if ( self.phase is not GamePhase.Playing
             and False not in self.__players_ready ):
            self.change_phase(GamePhase.Playing)

    def signal_ready(self, player: Player, ready: bool) -> None:
        assert player in self.players
        assert ready in [ True, False ]
        self.__players_ready[self.players.index(player)] = ready
        logger.debug("signal_ready(%s, %s) => %s, UI=%s",
                     player, ready, self.__players_ready, self.__ui_ready)
        if False in self.__players_ready:
            if self.phase is GamePhase.Playing:
                self.change_phase(GamePhase.Suspended)
        elif self.__ui_ready:
            # everyone is ready: launch game
            if self.phase is not GamePhase.Playing:
                self.change_phase(GamePhase.Playing)

    def declare_resignation(self, player: Player) -> None:
        # FIXME assumes players can resign individually
        self.listeners_notify(ResignEvent(game=self, player=player))
        self.outcome[player] = self.Outcomes.Resign
        player.prev.next = player.next
        player.next.prev = player.prev
        if player.next.next == player.next:
            self.outcome[player.next] = self.Outcomes.Winner
            self.change_phase(GamePhase.Over)
        elif self.scratch_state.whose_turn == player:
            self.scratch_state.next_player()

    def propose_outcome(self, player: Player, outcome: dict[Player,GameOutcomes]) -> None:
        "Let player propose an outcome, and settle the game when everybody OKs"
        self.proposed_outcomes[player] = outcome
        # don't settle if one player opinion differs
        if set(self.proposed_outcomes.keys()) == set(self.players):
            if not functools.reduce(lambda x, y: x if x == y else {},
                                    self.proposed_outcomes.values()):
                return

        # when only 2 players, settle when one claims the other wins
        elif len(self.players) == 2:
            if  (outcome != {player.next: self.Outcomes.Winner} and
                 outcome != {player: self.Outcomes.Resign}):
                return

        # no idea!
        else:
            return

        # we have settled !
        self.outcome = outcome
        self.change_phase(GamePhase.Over)

    def create_move(self, *, gamestate: GameState, player: Optional[Player],
                    **kwargs: Any) -> CoreMove:
        """Factory method to create a Game.Move.

        Default implementation calls `self.Move(**kwargs)`, but more
        sophisticated games can eg. create instances of Game.Move
        subclasses depending on `kwargs`.
        """
        return self.Move(gamestate=gamestate, player=player, **kwargs)

    def _make_move(self, gamestate: GameState, player: Optional[Player], move: CoreMove,
                   anticipate: bool=True) -> None:
        "Apply move to gamestate"
        assert gamestate.game is self
        logger.debug("(%x) _make_move %s", id(self),
                     self.default_notation.move_serialization(move))
        frozen_gamestate = gamestate.frozen()
        # FIXME maybe make sure frozen_gamestate matches move.gamestate
        self.premove_check(frozen_gamestate, player, move)
        self.supplement_and_check_move(frozen_gamestate, player, move, anticipate=anticipate)
        # now really apply the move
        self.do_move(gamestate, player, move)
        self.postmove_state_update(gamestate, move)

    def make_move(self, player: Optional[Player], move: CoreMove, anticipate: bool=True) -> None:
        "Apply move to game's scratch_state"
        self._make_move(self.scratch_state, player, move, anticipate)
        # record move
        self.__moves.add_move(move, self.scratch_state.frozen())
        logger.debug("(%x) Move history now: %s", id(self), self.__moves.pretty_printed)
        # publish move
        self.listeners_notify(self.MoveEvent(game=self, move=move))

    def premove_check(self, gamestate: GameState, player: Optional[Player], move: CoreMove) -> None:
        """Check the move wrt game state (as opposed to game position).

        Default implementation refuses the move when:
         * we are in a known non-playing phase of the game;
         * it is not the player's turn."""

        if not isinstance(move, self.Move):
            raise InvalidMove("Move %r has Wrong type %r, expected %r" %
                              (move, type(move), self.Move))

        if self.is_importing:
            if self.phase is not GamePhase.SettingUp:
                raise InvalidMove("game is not being set up, phase is %s" %
                                  (self.phase,))
        else:
            self.check_playing_phase()

        # wheck whose turn it is
        if player not in (None, gamestate.whose_turn):
            raise InvalidMove("not player's turn.")

    def supplement_and_check_move(self, gamestate: GameState, player: Optional[Player],
                                  move: CoreMove, anticipate: bool) -> None:
        """Check the move wrt game position, augment it with forced and optional changes.

        Also anticipate or not resulting position for more checks
        (eg. forbidding to move one's king into check).
        """
        assert isinstance(gamestate, self.State)
        assert player is None or player in self.players
        move.assert_locations(self.holders)

    def check_has_game_started(self) -> None:
        if self.phase is GamePhase.SettingUp:
            raise InvalidMove("game has not started yet")

    def check_playing_phase(self) -> None:
        self.check_has_game_started()
        if self.phase is not GamePhase.Playing:
            exception = {GamePhase.Suspended: GameSuspended(),
                         GamePhase.Over: InvalidMove("game is over"),
                         }[self.phase]
            raise exception

    def is_playing_phase(self) -> bool:
        try:
            self.check_playing_phase()
            return True
        except InvalidMove as ex:
            return False

    def postmove_state_update(self, gamestate: GameState, move: CoreMove) -> None:
        """Last updates to game state after applying move."""
        # base implementation does not do anything

    def history_formatmove(self, movelist: MoveList, movenum: int,
                           move: CoreMove, notation: MoveNotation) -> str:
        #assert move is movelist.played[movenum]
        movestr = "%d. " % (1 + movenum)
        movestr += notation.move_serialization(move)
        return movestr

    def do_move(self, gamestate: GameState, player: Optional[Player], move: CoreMove) -> None:
        raise NotImplementedError()

    def register_listener(self, l: Callable[[GameEvent],None]) -> None:
        "Register function of one argument to be called for game event."
        self.__listeners.append(l)
    def unregister_listener(self, l: Callable[[GameEvent],None]) -> None:
        self.__listeners.remove(l)
    def listeners_notify(self, event: GameEvent) -> None:
        for l in self.__listeners:
            l(event)

    def create_players(self) -> None:
        player = None
        players = []
        for name in self.player_names:
            player = Player(name, prev=player)
            players.append(player)
        self.players = tuple(players)

    def create_holders(self) -> None:
        "To be overriden by subclasses."
        # FIXME: we should ensure this is consistent with self.holder_classes
        pass

    def post_init(self, **kwargs: Any) -> None: # type: ignore[override]
        """Operations to run after a Game object was constructed and its params set.

        Those operations would belong to the end of __init__(), if
        only params were created before __init__ code is run (known
        Overlord issue).
        """
        super().post_init(**kwargs)
        self.create_holders()
        self.scratch_state = self.State(self)
        self.scratch_state.set_phase(GamePhase.SettingUp)

    def set_start_position(self) -> None:
        raise NotImplementedError()

    @property
    def holders(self) -> Iterable[PieceHolder]:
        return self._holders.values()
    @classproperty
    @classmethod
    def holder_classes(cls) -> tuple[type[PieceHolder], ...]:
        return ()

    @property
    def moves(self) -> MoveList:
        return self.__moves

    ## I/O

    def get_writer_class(self, filename: str) -> type[GameWriter]:
        "Raw dump of game moves in default notation."
        from Omaha.GameIO.RawWriter import RawGameWriter
        return RawGameWriter

    ## undo-related

    def undo_move(self) -> None:
        if not isinstance(self, UndoableGame):
            raise InvalidMove("Game %r does not support undo" % type(self))
        self.check_has_game_started()

        try:
            move = self.moves.last
        except IndexError:
            logger.warning("nothing to undo")
            return

        # back to the turn of the player we're undoing the move
        self.scratch_state.set_whose_turn(self.scratch_state.whose_turn.prev)
        # unrecord
        self.moves.undo()

        self._undo_move(move)

        # publish undo
        self.listeners_notify(self.UndoEvent(game=self))

        logger.debug("(%x) Move history now: %s", id(self), self.moves.pretty_printed)

    def _undo_move(self, move: CoreMove) -> None:
        raise NotImplementedError()

    def redo_move(self) -> None:
        if not isinstance(self, UndoableGame):
            raise InvalidMove("Game %r does not support undo" % type(self))
        self.check_has_game_started()

        self.moves.redo()
        self.listeners_notify(self.MoveEvent(game=self, move=self.moves.last))
        self.do_move(self.scratch_state, self.scratch_state.whose_turn, self.moves.last) # FIXME
        logger.debug("(%x) Move history now: %s", id(self), self.moves.pretty_printed)
        self.scratch_state.next_player()

    def refuse_move(self, move: CoreMove, player: Player) -> None:
        "Let `player` declare he refuses `move` in the movelist."

        if not isinstance(self, UndoableGame):
            # FIXME: there should not be any such dependency on undo here
            raise InvalidMove("Game %r does not support undo" % type(self))
        self.check_playing_phase()

        # Only allow refusing last move
        assert move is self.moves.last
        # We have no adjudication support yet, let anyone refuse :)
        self.undo_move()

    ## tooltip support

    has_tooltip = False

    def location_tooltip_text(self, gamestate: GameState, loc: Location) -> str:
        raise NotImplementedError()

class UndoableGame(Game,
                   metaclass=Overlord.ParametrizableOuterclassABCMeta):
    # pylint: disable=abstract-method
    pass

class AlternatingTurnsGame(Game):
    "A game where players alternate making exactly one move each."

    def _make_move(self, gamestate: GameState, player: Optional[Player], move: CoreMove,
                   anticipate: bool=True) -> None:
        super()._make_move(gamestate, player, move, anticipate)
        assert gamestate.whose_turn is not player

    def postmove_state_update(self, gamestate: GameState, move: CoreMove) -> None:
        super().postmove_state_update(gamestate, move)
        gamestate.next_player()

    def history_formatmove(self, movelist: MoveList, movenum: int,
                           move: CoreMove, notation: MoveNotation) -> str:
        #assert move is movelist.played[movenum]
        nplayers = len(self.players)
        turn, player_rank = divmod(movenum, nplayers)
        movestr = "%d. " % (1 + turn)
        movestr += "… " * player_rank
        movestr += notation.move_serialization(move)
        movestr += " …" * (nplayers - player_rank - 1)
        return movestr

class AutomaticMovesGame(Game):
    "A game where some moves are generated in reaction to player moves."
    class State(Game.State):
        _TState = TypeVar("_TState", bound="AutomaticMovesGame.State") # poor man's Self
        def __init__(self, game: AutomaticMovesGame):
            super().__init__(game)
            self.__pending_moves: Sequence[dict[str,Any]] = []

        @property
        def has_pending_moves(self) -> bool:
            return len(self.__pending_moves) > 0
        def add_pending_move(self, **kwargs: Any) -> None:
            assert isinstance(self.__pending_moves, list)
            self.__pending_moves.append(frozendict(kwargs))
        def trigger_pending_move(self) -> None:
            move_args = self.__pending_moves[0]
            move = self.game.create_move(gamestate=self, player=None, **move_args)
            assert isinstance(move, self.game.Move), "%s is not a game.Move" % (move,)
            try:
                self.game.make_move(None, move)
            except InvalidMove as ex:
                # internal error, don't confuse upper layers into thinking
                # the user move was invalid
                assert False, "induced move %s refused: %s" % (move, ex)
        def consume_pending_move(self, move: CoreMove) -> None:
            assert isinstance(self.__pending_moves, list)
            movespec = self.__pending_moves.pop(0)
            logger.debug("consume_pending_move ... %s", movespec)

        def clone(self: _TState) -> _TState:
            c = super().clone()
            c.__pending_moves = list(self.__pending_moves)
            return c
        def freeze(self) -> None:
            super().freeze()
            self.__pending_moves = tuple(self.__pending_moves)
            self.add_pending_move = NotImplemented # type: ignore[assignment]
            self.consume_pending_move = NotImplemented # type: ignore[assignment]

    scratch_state: State
    last_state: State

    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
        self.register_listener(self.gameevent_listener)

    def gameevent_listener(self, event: GameEvent) -> None:
        if (isinstance(event, self.MoveEvent) or (isinstance(event, self.PhaseChangeEvent) and
                                                  event.newphase == GamePhase.Playing)):
            if self.last_state.has_pending_moves:
                self.last_state.trigger_pending_move()

    def postmove_state_update(self, # type: ignore[override]
                              gamestate: AutomaticMovesGame.State,
                              move: AutomaticMovesGame.Move) -> None:
        super().postmove_state_update(gamestate, move)
        if cast(AutomaticMovesGame.State, move.gamestate).has_pending_moves:
            # we just executing a pending_move
            gamestate.consume_pending_move(move)


class Match(Overlord.Parametrizable):
    def __init__(self, game: Game, **kwargs: Any):
        super().__init__(**kwargs)
        self._playerdrivers_classes: dict[Player,Overlord.Param[Overlord.Plugin]] = {} # the param
        self._player_drivers: dict[Player,PlayerDriver] = {}

        self.game = game

    @classproperty
    @classmethod
    def context_key(cls) -> Overlord.ContextKey:
        return Match

    @classmethod
    def parameters_dcl(cls, context: Context) -> dict[str,Overlord.ParamDecl[Any]]:
        p = dict(super().parameters_dcl(context))
        game_plugin = context.game_plugin
        assert isinstance(game_plugin, Overlord.Plugin)
        game_class = game_plugin.Class
        assert issubclass(game_class, Game)
        for player_name in game_class.player_names:
            paramkey = "playerdriverclass_" + player_name
            assert paramkey not in p
            #if (game_class, holder_class) not in cls.__default_renderers:
            #    cls.__init_default_renderer(game_class, holder_class)
            p[paramkey] = Overlord.params.Plugin(
                label = player_name,
                plugintype = 'X-Omaha-PlayerDriver',
                context = context,
                pattern = plugin_pattern.Or(
                    # some AI engine protocols are generic enough, and
                    # interactive move composition must know game's moves
                    plugin_pattern.ParentOf(
                        'X-Omaha-Move-Categories', game_class.Move),
                    # most AI engines must know the game
                    plugin_pattern.ParentOf(
                        'X-Omaha-Game-Categories', game_class)),
                group_by = "class_description",
                default_by_name = 'LocalPlayerDriver',
                #default = str(getattr(type(self._player_drivers[player]),
                #                      'x-plugin-desc').ID),
                #default = cls.__default_renderers[(game_class, holder_class)]
            )
        return p

    def set_params(self, params: dict[str,Union[Overlord.Param[Any],Any]]) -> None:
        parentparams = dict(params)

        for paramid, param in params.items():
            if paramid.startswith("playerdriverclass_"):
                logger.debug("%s (%s): %s", paramid, param.label, param.value)
                for player in self.game.players:
                    if paramid == "playerdriverclass_" + player.name:
                        break
                else:
                    continue

                Driver = param.value.Class
                assert issubclass(Driver, PlayerDriver), \
                    "%s is not a PlayerDriver" % (Driver,)

                # memorize the class param, to be instanciated by GameApp
                setattr(self, paramid, param)
                self._playerdrivers_classes[player] = param

                del parentparams[paramid]

        super().set_params(parentparams)

    def set_params_using_app(self, params: dict[str,Union[Overlord.Param[Any],Any]],
                             app: GameApp) -> None:
        """Set Match params (essentially PlayerDriver) with knowledge of a Core.GameApp.

        FIXME This is a hack needed for as long as PlayerDriver's have
        need of a Core.GameApp.
        """
        self.set_params(params)

        assert len(self._playerdrivers_classes) == len(self.game.players)
        for player in self.game.players:
            param = self._playerdrivers_classes[player]
            Driver = param.value.Class

            newdrv = Driver(gui=app, player=player, parentcontext=self.context)
            newdrv.register_listener(app.playerdriver_listener)
            self._player_drivers[player] = newdrv

            # apply any PlayerDriver parameters
            choice_text = param.value.name
            if choice_text in param.subparams:
                self._player_drivers[player].set_params(
                    param.subparams[choice_text])
            else:
                logger.debug("no subparams for %r in %r",
                             choice_text, param.subparams)

    def clone(self) -> Match:
        # prevent calling of Parametrizable.clone()
        raise SyntaxError("clone() is not supported in Match")
